try:
	from awsglue.context import GlueContext
	from awsglue.job import Job
	from awsglue.transforms import *
	from awsglue.utils import getResolvedOptions
	envAwsglue = True
except Exception as e:
	print('Not running on AWS Glue')
	envAwsglue = False

from dateutil import parser

from pyspark.conf import SparkConf
from pyspark.context import SparkContext
from pyspark.sql import SparkSession
from pyspark.sql.types import *

try:
	from AvernusGlue import AvernusUtilETL as au
	from AvernusGlue import t2n_Functions as t2n
	from AvernusGlue import DataDictionaries as dd
	print('AVERNUS COMMENT - Used the "from xxx_folder import xXx as x" format')

except:
	import DataDictionaries as dd
	import t2n_Functions as t2n
	import AvernusUtilETL as au
	print('AVERNUS COMMENT - Used the "import xXx as x" format')

import ast
import boto3
import csv
import datetime
import dateutil.parser
import json
import pyspark
import pyspark.sql.functions as f
import sys
import traceback
#######################################################

#Base Variables
#Dictionary for logs
output = {'Status': 'Started' , 'FileReceived': [], 'Data_Processed': [], 'stdoutLog' : []}

#s3 
s3 = boto3.client('s3')

###################################################################
# Processes a folder [Full Dataset]
#  
# Input:
# DataTypeDict -> Data Dictionary
# logDataType -> Log Dictionary for the data type 
# output -> General Log Dictionary
# Returns:
# logDataType-> Log Dicionary for the data type 
#  
def ProcessingTables(DataTypeDict, logDataType, output):
	
	try:
		detailsDict = DataTypeDict['Dict']
		############################################

		initialDf = detailsDict['startingTable']

		df = au.OpenParquetFolder(detailsDict['spark'], detailsDict['currentBucket'], initialDf)

		# If data type is a master table, make all the initial joins
		if 'masterInitJoin' in detailsDict:
			
			for initialJoin in detailsDict['masterInitJoin']:
				StartJoin = au.JoinDf(df, detailsDict['spark'], initialJoin, output)

				df = StartJoin['DF']
				output = StartJoin['Log']
				
		#Result for transformationProcess = {'log': output, 'FinalDfCount': rowCount}
		transformationProcess = t2n.BasicEnhancement(df, detailsDict, output)

		output = transformationProcess['Log']
		transformedDfSize = transformationProcess['FinalDfCount']

		logDataType['RowsCreated'] += transformedDfSize

		if transformedDfSize == 0:
			output['stdoutLog'].append('All the content in the files {} was already in the transformed table'.format(DataTypeDict['Dict']['tableName']))

		logDataType['FilesProcessed']['Succesful'].append(DataTypeDict['Files'])

	except Exception as e:
		errorReason = 'The following files {} failed because of {}\n MainReason was {}'.format(DataTypeDict['Dict']['tableName'], e, str(traceback.format_exc()))
		print('Avernus Commens: {}'.format(errorReason))
		logDataType['FilesProcessed']['Failed'].append(DataTypeDict['Files'])
		output['stdoutLog'].append(errorReason)

		############################################

		print('AVERNUS COMMENT: {0} was fully processed.\n'.format(detailsDict['tableName']))


	except Exception as e:
		#Show errors if it fails at any point, and all those issues to the log dictionary
		print('AVERNUS COMMENT: Error occured and it was', e)
		print(str(traceback.format_exc()))
		print('AVERNUS COMMENT: Data type {0} failed to execute corrently. \nAVERNUS COMMENT:It continued to next data type'.format(DataTypeDict['Dict']['tableName']))
		logDataType['Error'] = str(e)

	return logDataType


#######################################################
# Processes a list of files
# 
# Input:
# DataTypeDict -> Data Dictionary
# logDataType -> Log Dictionary for the data type 
# output -> General Log Dictionary
# Returns:
# logDataType-> Log Dicionary for the data type 
#  
def ProcessingFiles(DataTypeDict, logDataType, output):
	
	try:

		detailsDict = DataTypeDict['Dict']

		fileList = DataTypeDict['Files']

		#Format the file list to make it PySpark Readable and maintain integrity throughout logs
		formattedFileList = ['s3a://{}/{}'.format(detailsDict['currentBucket'],file) for file in fileList]

		############################################
		#Open all files, turn them into DataFrames, and save the DF, Filename and Schema in the dictionary.

		dataframesDict = {'Files': []}

		for file in formattedFileList:

			try:

				fileDF = au.DataframeOpener(file, spark, output)
				output = fileDF['Log']
				fileDF = fileDF['Dataframe']

				if fileDF is None:
					raise Exception('File was not transformed into DF and the DataframeOpener did not catch the error')

				fileSchema = fileDF.dtypes
				dataframesDict['Files'].append({'Filename': file,'fileDataframe': fileDF , 'FileSchema': str(fileSchema)})

			except:
				output['stdoutLog'].append('AVERNUS COMMENT: The file {} failed to be made into a dataframe by PySpark and will not be processed'.format(file))
				logDataType['FilesProcessed']['Failed'].append(file)
				continue

		############################################
		#Group all the DFs and Files per schema type
		#Iterate through the different schemas and combine them into a single DF
		#Process these DFs individually

		#Makes changes to it such as --
		#   Deletes duplicates, maitaining the smallest Timestamp (Oldest File with those specific details)
		#   Transforms all the date strings into timestamps
		#   Rechecks the data types
		#	Joins tables to this dataframe
		#	Drops columns
		#	Rounds/ hashes columns
		#	Saves the Dataframe as parquet files in the defined directory
		#	Returns the log dictionary

		uniqueSchemas = {}

		for fileInfo in dataframesDict['Files']:

			if fileInfo['FileSchema'] not in uniqueSchemas:
				uniqueSchemas[fileInfo['FileSchema']] = {'Filenames': [], 'Dataframes':[]}

			uniqueSchemas[fileInfo['FileSchema']]['Filenames'].append(fileInfo['Filename'])
			uniqueSchemas[fileInfo['FileSchema']]['Dataframes'].append(fileInfo['fileDataframe'])

		for files in uniqueSchemas.values():

			try:
				thisDF = au.unionAll(*files['Dataframes'])

				#Result for transformationProcess = {'log': output, 'FinalDfCount': rowCount}
				transformationProcess = t2n.BasicEnhancement(thisDF, detailsDict, output)

				output = transformationProcess['Log']
				transformedDfSize = transformationProcess['FinalDfCount']

				logDataType['RowsCreated'] += transformedDfSize

				if transformedDfSize == 0:
					output['stdoutLog'].append('All the content in the files {} was already in the enhanced table'.format(files['Filenames']))

				logDataType['FilesProcessed']['Succesful'].append(files['Filenames'])

			except Exception as e:
				errorReason = 'The following files {} failed because of {}\n MainReason was {}'.format(files['Filenames'], e, str(traceback.format_exc()))
				print('Avernus Commens: {}'.format(errorReason))
				logDataType['FilesProcessed']['Failed'].append(files['Filenames'])
				output['stdoutLog'].append(errorReason)
				continue

		############################################

		print('AVERNUS COMMENT: {0} was fully processed.\n'.format(detailsDict['tableName']))


	except Exception as e:
		#Show errors if it fails at any point, and all those issues to the log dictionary
		print('AVERNUS COMMENT: Error occured and it was', e)
		print(str(traceback.format_exc()))
		print('AVERNUS COMMENT: Data type {0} failed to execute corrently. \nAVERNUS COMMENT:It continued to next data type'.format(detailsDict.keys()))
		logDataType['Error'] = str(e)

	return logDataType
################################################################### 

#AWS initiating the job
## @params: [JOB_NAME]
args = getResolvedOptions(sys.argv, ['JOB_NAME'])

#Starting the Spark Context
sc = SparkContext()

#Starting the glue contex -> it is just like a spark context but with some extra functions and classes --> None are currently used
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)

#Get the arguments given with the start of the job
args = getResolvedOptions(sys.argv, ['JOB_NAME','PRODATAFILES','LOGBUCKET','LOGPREFIX','ORIGINBUCKET','CURRENTDATABASENAME','RESULTBUCKETNAME', 'TRANSFORMEDBUCKET', 'RAWBUCKET', 'ENRICHEDBUCKET'])
job.init(args['JOB_NAME'], args)

#Separate some of the arguments into their own variables
requestFile = ast.literal_eval(args['PRODATAFILES'].strip())
requestFileContents = au.openS3Files(s3, requestFile['Bucket'], requestFile['FileKey'], output)
fileValues = json.loads(requestFileContents)

#General Job Variables
currentBucket = args['ORIGINBUCKET']
resultBucket = args['RESULTBUCKETNAME']

#Log Variables
logFolderName = args['LOGPREFIX']
logBucketName = args['LOGBUCKET']
logDetails = {'LogBucket': logBucketName, 'LogFolder':logFolderName}

#Add those data types into the log dict
output['FileReceived'] = requestFile

# Start DataDictionary Class
DataDictionary = dd.DataDictionary(raw = args['RAWBUCKET'], transformed = args['TRANSFORMEDBUCKET'] , enriched = args['ENRICHEDBUCKET'] )


# If any of the arguments are missing, this will stop the process before it goes any further 
if len(requestFile) == 0:
	output['stdoutLog'].append('AVERNUS COMMENT: No Data Types were found, please make sure the argument was added to the command')
	output['Status'] = 'Failed'
	au.SaveLog(output,s3,logDetails)
	exit()
	
elif logFolderName == '' or logBucketName == '' or args['ORIGINBUCKET'] == '' or args['CURRENTDATABASENAME'] == '' or args['RESULTBUCKETNAME'] == '':
	output['stdoutLog'].append('AVERNUS COMMENT: One or more required arguments is empty, please make sure the arguments were added to the command')
	output['Status'] = 'Failed'
	au.SaveLog(output,s3, logDetails)
	exit()

else:

	# The order which files are processed
	# This is done by iterating through the data types in the input file
	# If none was explicitly stated - then it assumes it to be irrelevant [Therefore, makes it a `Reference` table]
	tableType_Order = ['reference', 'masterReferece','event','masterEvent']

	newFileDict = {}
	foundMasters = []

	for filetype in fileValues.keys():

		#get result/ source table name
		tableName = filetype.lower()

		#Find data type in the DataDictionary file, and gets it 
		#If not, it will log this use the default dictionary.
		detailsDict = DataDictionary.getDictionary(tableName)

		#Find data type in dictionary, and get default processes
		detailsDict['spark'] = spark
		detailsDict['currentBucket'] = currentBucket
		detailsDict['resultBucket'] = resultBucket

		if 'tableType' not in detailsDict:
			detailsDict['tableType'] = 'reference'

		# Check if any of the updated files are a part of the 'MasterTable'
		# If it is, adds the data type
		foundMasters += DataDictionary.getMasterDicts(tableName)
			
		#Creates new dictionary with the data dictionaries and their relevant files 
		newFileDict[filetype] = {'Files':fileValues[filetype], 'Dict': detailsDict}


	foundMasters = list(set(foundMasters))

	for masterTableInfo in foundMasters:

		masterDict = DataDictionary.getDictionary(masterTableInfo)

		#Find data type in dictionary, and get default processes
		masterDict['spark'] = spark
		masterDict['currentBucket'] = currentBucket
		masterDict['resultBucket'] = resultBucket

		# If tableType wasn't set up, it ignores the Master as this could interfere with other tables.
		if 'tableType' not in masterDict:
			output['stdoutLog'].append('AVERNUS COMMENT: MasterDictionary {} has not bee set up correctly, please add the tableType argument!'.format(masterTableInfo['dictName']))
			continue

		newFileDict[masterTableInfo] = {'Files': masterDict['startingTable'], 'Dict':masterDict}

	# Iterate following the tableType_Order 
	for tt in tableType_Order:

		for DataType in newFileDict.keys():

			if newFileDict[DataType]['Dict']['tableType'] != tt:
				continue

			#Data type log
			logDataType = {'DataType': DataType, 'RowsCreated': 0, 'FilesProcessed': {'Succesful':[], 'Failed':[]}}

			try:

				#Start the datatypes dict
				detailsDict = newFileDict[DataType]['Dict']

				#Get TableName
				tableName = DataType.lower()

				#File types
				areTheyFiles = ['.' in x for x in  newFileDict[DataType]['Files']][0]

				#Process regular Files
				if areTheyFiles == True:
					output['Data_Processed'].append(ProcessingFiles(newFileDict[DataType], logDataType, output))
				else:
				#Processes folder [Full Dataset]
					output['Data_Processed'].append(ProcessingTables(newFileDict[DataType], logDataType, output))
				continue
			
			except Exception as e:
				#Show errors if it fails at any point, and all those issues to the log dictionary
				print('AVERNUS COMMENT: Error occured and it was', e)
				print(str(traceback.format_exc()))
				print('AVERNUS COMMENT: Data type {0} failed to execute corrently. \nAVERNUS COMMENT:It continued to next data type'.format(detailsDict['tableName']))
				logDataType['Error'] = str(e)

#Save the log dictionary as a JSON file on S3 
#then
#Commit and finish
output['Status'] = 'Completed'
au.SaveLog(output,s3, logDetails)
job.commit()