from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions

from dateutil import parser

from pyspark.conf import SparkConf
from pyspark.context import SparkContext
from pyspark.sql import SparkSession
from pyspark.sql.types import *

try:
	from AvernusGlue import AvernusUtilETL as au
	from AvernusGlue import r2t_Functions as r2t
	from AvernusGlue import DataDictionaries as dd
	print('AVERNUS COMMENT - Used the "from xxx_folder import xXx as x" format')

except:
	import r2t_Functions as r2t
	import AvernusUtilETL as au
	import DataDictionaries as dd
	print('AVERNUS COMMENT - Used the "import xXx as x" format')

import ast
import boto3
import csv
import datetime
import dateutil.parser
import json
import pyspark
import pyspark.sql.functions as f
import sys
import traceback

###################################################################
#Base Variables

#Dictionary for logs
output = {'Status': 'Started' , 'Received': [], 'Data_Processed': [], 'stdoutLog' : []}

#s3 for logs + CSV in the future
s3 = boto3.client('s3')
################################################################### 

#AWS initiating the job
## @params: [JOB_NAME]
args = getResolvedOptions(sys.argv, ['JOB_NAME'])

#Starting the Spark Context
sc = SparkContext()

#Starting the glue contex -> it is just like a spark context but with some extra functions and classes, specifically the Dynamic Frame class.
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)

#Get the extra arguments given with the start of the job
args = getResolvedOptions(sys.argv, ['JOB_NAME','PRODATAFILES','LOGBUCKET','LOGPREFIX','ORIGINBUCKET','CURRENTDATABASENAME','RESULTBUCKETNAME', 'RAWBUCKET', 'TRANSFORMEDBUCKET', 'ENRICHEDBUCKET'])
job.init(args['JOB_NAME'], args)

output['stdoutLog'].append('AVERNUS COMMENT: Arguments Received were {}'.format(args))

#Separate some of the arguments into their own variables

#Get the PROCESSING DATA FILES [PRODATAFILES] file, which has a list of dictionaries, where the key is the data type and the value is a list of files.
#For example:
#
# [
# 	{ DataType: [folder/file1.csv, folder/file2.csv]}
# ]
#
requestFile = ast.literal_eval(args['PRODATAFILES'].strip())
requestFileContents = au.openS3Files(s3, requestFile['Bucket'], requestFile['FileKey'], output)
fileValues = json.loads(requestFileContents)

#General Job Variables
currentBucket = args['ORIGINBUCKET']
resultBucket = args['RESULTBUCKETNAME']

#Log Variables
logFolderName = args['LOGPREFIX']
logBucketName = args['LOGBUCKET']

logDetails = {'LogBucket': logBucketName, 'LogFolder':logFolderName}

#Add those data types into the log dict
output['FileReceived'] = requestFile

# Start DataDictionary Class
DataDictionary = dd.DataDictionary(raw = args['RAWBUCKET'], transformed = args['TRANSFORMEDBUCKET'] , enriched = args['ENRICHEDBUCKET'] )

# If any of the arguments are missing, this will stop the process before it goes any further
#
if len(requestFile) == 0:
	output['stdoutLog'].append('AVERNUS COMMENT: No Data Types were found, please make sure the argument was added to the command')
	output['Status'] = 'Failed'
	au.SaveLog(output,s3,logDetails)
	exit()
	
elif logFolderName == '' or logBucketName == '' or args['ORIGINBUCKET'] == '' or args['CURRENTDATABASENAME'] == '' or args['RESULTBUCKETNAME'] == '':
	output['stdoutLog'].append('AVERNUS COMMENT: One or more required arguments is empty, please make sure the arguments were added to the command')
	output['Status'] = 'Failed'
	au.SaveLog(output,s3, logDetails)
	exit()

else:
	#If all the arguments were input, the main process starts.
	#The process goes as follows:
	#
	#	1. It will iterate through all the data types/files
	#	2. Create dynamic frames from their files
	#	3. Send that information to the r2t_Functions' BasicTransformation function.
	#
	#The function will return the log dictionary, which will be appended to the current one and move into the next data type
	for DataType, fileList in fileValues.items():

		logDataType = {'DataType': DataType, 'RowsCreated': 0, 'FilesProcessed': {'Succesful':[], 'Failed':[]}}

		try:
			#get result/ source table name
			tableName = DataType.lower()

			#Find data type in the DataDictionary file, and gets it 
			#If not, it will log this use the default dictionary.
			detailsDict = DataDictionary.getDictionary(tableName)
		
			#Add the job based variables into the dictionary
			detailsDict['spark'] = spark
			detailsDict['currentBucket'] = currentBucket
			detailsDict['resultBucket'] = resultBucket

		
			############################################
			#Format the file list to make it PySpark Readable 
			formattedFileList = []

			for file in fileList:
				fullname = 's3a://{}/{}'.format(detailsDict['currentBucket'], file)
				formattedFileList.append(fullname)

			############################################
			#Open all files, turn them into DataFrames, and save the DF, Filename and Schema in the dictionary.
			#If file cannot be open, fileDF will be None and it will move to the next file
			dataframesDict = {'Files': []}

			for file in formattedFileList:
				
				try:
					# Open file
					fileDF = au.DataframeOpener(file, spark, output)
					output = fileDF['Log']
					fileDF = fileDF['Dataframe']
					
					if fileDF is None:
						continue
					
					#Get schema in a list of tuples format --> [(columnName, dataType())]
					fileSchema = fileDF.dtypes
					dataframesDict['Files'].append({'Filename': file,'fileDataframe': fileDF , 'FileSchema': str(fileSchema)})
				
				except:
					output['stdoutLog'].append('AVERNUS COMMENT: The file {} failed to be made into a dataframe by PySpark and will not be processed'.format(file))
					logDataType['FilesProcessed']['Failed'].append(file)
					continue

			############################################			
			#Group all the DFs and Files per schema type
			#Iterate through the different schemas and combine them into a single DF
			#Process these DFs individually

			#Makes changes to it such as:
			#   Deletes duplicates, maitaining the smallest Timestamp (Oldest File with those specific details)
			#   Transforms all the date strings into timestamps
			#   Rechecks the data types
			#	Saves the Dataframe as parquet files in the defined directory
			#	Returns the log dictionary

			uniqueSchemas = {}

			for fileInfo in dataframesDict['Files']:
				
				if fileInfo['FileSchema'] not in uniqueSchemas:
					uniqueSchemas[fileInfo['FileSchema']] = {'Filenames': [], 'Dataframes':[]}
				
				uniqueSchemas[fileInfo['FileSchema']]['Filenames'].append(fileInfo['Filename'])
				uniqueSchemas[fileInfo['FileSchema']]['Dataframes'].append(fileInfo['fileDataframe'])


			# Iterate through different schemas
			for schema, files in uniqueSchemas.items():

				try:
					#Join all DFs with the same schema
					thisDF = au.unionAll(*files['Dataframes'])
					
					#Result for transformationProcess = {'log': output, 'FinalDfCount': rowCount}
					transformationProcess = r2t.BasicTransformation(thisDF, detailsDict, output)
					
					# Result from BasicTransformation
					output = transformationProcess['Log']
					transformedDfSize = transformationProcess['FinalDfCount']

					# Breakdown the results into the log
					logDataType['RowsCreated'] += transformedDfSize

					# If no new rows, adds that comment to log and moves on
					if transformedDfSize == 0:
						output['stdoutLog'].append('All the content in the files {} was already in the transformed table'.format(files['Filenames']))

					logDataType['FilesProcessed']['Succesful'].append(files['Filenames'])
					#Move into the next file
				
				# If any error occurs, adds the files to the 'failed' list and adds the error to the log
				except Exception as e:
					errorReason = 'The following files {} failed because of {}\n MainReason was {}'.format(files['Filenames'], e, str(traceback.format_exc()))
					print('Avernus Commens: {}'.format(errorReason))
					logDataType['FilesProcessed']['Failed'].append(files['Filenames'])
					output['stdoutLog'].append(errorReason)
					continue
			
			############################################

			print('AVERNUS COMMENT: {0} was fully processed.\n'.format(DataType))

		# If any error occurs, adds all the files to the 'failed' list and saves the data type as an 'error' + adds the error to the log
		except Exception as e:
			#Show errors if it fails at any point, and all those issues to the log dictionary
			output['stdoutLog'].append('AVERNUS COMMENT: Error occured and it was {}'.format(e))
			output['stdoutLog'].append(str(traceback.format_exc()))
			output['stdoutLog'].append('AVERNUS COMMENT: Data type {0} failed to execute corrently. \nAVERNUS COMMENT:It continued to next data type'.format(DataType))
			logDataType['Error'] = str(e)
			continue

		# Cleans the log of unused variables
		output['Data_Processed'].append(au.clean_empty(logDataType))

#Save the log dictionary as a JSON file on S3 
#then
#Commit and finish
output['Status'] = 'Completed'
au.SaveLog(output,s3, logDetails)
job.commit()