# Glue ETL Job Scripts
##### Ventures | Pedro Mroninski | 18/04/2019

###### Last Updated: 07/08/2019

#

#### Description
Templates for performing the Enhance/Transform/Load (ETL) jobs on AWS Glue using PySpark and the awsglue packages. The templates are for Raw-to-Transform (R2T) and Transform-to-Enhance (T2N) basic processes. Written for the Python 3.6 + PySpark 2.4 runtime and to be run in the AWS Glue environment.
#

#### Pre-Run Setup + First Run
These functions require the user to have:

1. Already downloaded the files and added them into S3

2. Crawled the data with an AWS Glue Crawler.

Once these have been done and checked that AWS Athena is correctly reading the files and finding the correct schemas - then you can run the R2T job. 

Once that is run, then you are to crawl the destination folder for that job, check the schema and only then run the T2N. This is to be done only on the first run through, to ensure that Athena/Glue are correctly seeing your initial data.

#
#### Before Calling a Job
As mentioned in the State Machine, the glue job requires a file with the data types [For grouping and Data Dictionaries] and the files which are to be processed. The file would look similar to the following:

```json
    "nmds": [
        "NMDS/NMDS_685859_20190730txt_1564458039.parquet",
        "NMDS/NMDS_685860_20190730txt_1564445110.parquet",
        "NMDS/NMDS_685861_20190730txt_1564445114.parquet",
        "NMDS/NMDS_685862_20190730txt_1564458073.parquet"
    ]
```


#### Arguments Input
Arguments have been very useful and enables the Glue processes to run jobs for multiple data types without having the server having to stop-start multiple times and taking a large amount of time + increase in costs. These arguments can be set up to have defaults in AWS glue and so, if one is to _always_ run the same code, the could just make that the argument and never change it.



**The following values are expected to be input before running any of the jobs:**

| Argument           | Meaning                                                      | Example (For R2T)                                        |
| ------------------ | :----------------------------------------------------------- | :------------------------------------------------------- |
| --PRODATAFILES     | Bucket and Key of file that contains the JSON dictionary of data types + files you want to process | {'Bucket':'datalake-logs-s3','FileKey':'Request/01.txt'} |
| --DATATYPES        | All the data types which you want to process, in a list with **single** quotes around their names | ['Patient', 'Drugs']                                     |
| --ORIGINBUCKET     | Bucket where files that are to be processed are currently located | datalake-raw-s3                                          |
| --RESULTBUCKETNAME | Bucket where you want the post-processed data to go to.      | datalake-transformed-s3                                  |
| --LOGPREFIX        | Prefix for the Log so that it can go into the relevant folder | R2T                                                      |
| --LOGBUCKET        | Bucket where the logs will be kept                           | datalake-logs-s3                                         |




Please note: The examples apply to R2T but the arguments are the same for all templates.

#### Template Structures
The templates follow similar structures, with the least amount of variables requested in order to simplify the process. In all, it is necessary to have the Origin Bucket name, Result bucket name and where the Logs are to be saved. .

All templates follow the following steps:
1. Open the file input with the data types and list of files in each.
2. Open the files, transform them into Dataframes and get their schemas.
3. Groups the Dataframes that have the same schema and are the same data type, join them together
4. Pass the joined Dataframes into the function.
5. Process the Dataframe with the information stated in the data dictionary
6. Save the changed Dataframe in the input "Result" bucket as a Parquet.
7. Move to the next schema or data type.
8. Once all files have been processed, save the logs in the input log location.

#

#### Data Dictionaries
Both the R2T function and the T2N will have basic processes where it cleans the column headers, save as parquet, deletes duplicates, move the data into the next step. These templated funcions don't require any input.

However, if you are to take advantage of the other functions available within the Avernus DataLake, these details need to added into the data's "Data Dictionary".

Data Dictionaries are YAML files that you save within S3. When you run a job, it compares the data types in it's initial request with what is in the directory. If it finds the data dictionary, it opens it and creates a python dictionary with the information. The dictionary then describes what needs to be done to the data type, and gives the variables for the functions to run.

If the Job doesn't find a file, it creates one and fills it with it's default values. Those can then be picked up and changed however you feel like the data should be processed.

Up to date example with commenting and description of the function can always be found within the [DataDictionaries folder](DataDictionaries/) but a simple example is below:
```yaml
changeColType:
- columnName: date_of_birth
  columnType: timestamp
- columnName: age
  columnType: int
deletedColumns:
- organisation_id
- event_id
- unique_identifier
enhanceDrops:
- ROWACTIVE
hashedColumns:
- nhi_number
joinTables:
- leftOn: ethnicity
  rightDataType:
    joinType: singlefile
    path: s3a://infoFiles/enrolmentMapping/EthinicGroup.csv
  rightOn:
  - EthinicGroup
  suffix: ethnicity_description
- joinType: left
  leftOn:
  - resultcode
  - subjectcode
  - extern_facility
  - source
  rightDataType:
    bucket: !Bucket transformed
    dataType: clinic/labs
    joinType: datatype
  rightKeep:
  - facility
  - subject
  - result
  - source
  rightOn:
  - resultcode
  - subjectcode
  - facility
  - source
  saveAs: null
  suffix: labs
keepOnlyColumns:
- id
- description
- source
masterInitJoin:
- joinType: union
  rightDataType:
    bucket: !Bucket transformed
    dataType: eightwire/interactiondruglink
    joinType: datatype
roundTime:
- colName: datereceived
  roundSecs: 300
roundedColumns:
- - x-coordinate
- - y-coordinate
  - 2
saveFormat: append
specialTreatment:
- file_timestamp
- columnName: clinic_name
  columnType: string
  typeAgg: !!python/name:pyspark.sql.functions.collect_list ''
startingTable: eightwire/genericdruglink
tableType: reference
```

And this turns into the following python dictionary that is then used to process the data type:

```python
{'changeColType': [{'columnName': 'date_of_birth', 'columnType': 'timestamp'}, {'columnName': 'age', 'columnType': 'int'}], 'deletedColumns': ['organisation_id', 'event_id', 'unique_identifier'], 'enhanceDrops': ['ROWACTIVE'], 'hashedColumns': ['nhi_number'], 'joinTables': [{'leftOn': 'ethnicity', 'rightDataType': {'joinType': 'singlefile', 'path': 's3a://infoFiles/enrolmentMapping/EthinicGroup.csv'}, 'rightOn': ['EthinicGroup'], 'suffix': 'ethnicity_description'}, {'joinType': 'left', 'leftOn': ['resultcode', 'subjectcode', 'extern_facility', 'source'], 'rightDataType': {'bucket': tranformed_bucket, 'dataType': 'clinic/labs', 'joinType': 'datatype'}, 'rightKeep': ['facility', 'subject', 'result', 'source'], 'rightOn': ['resultcode', 'subjectcode', 'facility', 'source'], 'saveAs': None, 'suffix': 'labs'}], 'keepOnlyColumns': ['id', 'description', 'source'], 'masterInitJoin': [{'joinType': 'union', 'rightDataType': {'bucket': tranformed_bucket, 'dataType': 'eightwire/interactiondruglink', 'joinType': 'datatype'}}], 'roundTime': [{'colName': 'datereceived', 'roundSecs': 300}], 'roundedColumns': [['x-coordinate'], ['y-coordinate', 2]], 'saveFormat': 'append', 'specialTreatment': ['file_timestamp', {'columnName': 'clinic_name', 'columnType': 'string', 'typeAgg': <function _create_function.<locals>._ at 0x0000016762A5C1E0>}], 'startingTable': 'eightwire/genericdruglink', 'tableType': 'reference'}

```


#

#### Template Functions
The functions are a way to minimise the number of sources to change, and because they were made to easily adapt to the source both [Raw-to-Transform (R2T)](AvernusGlue/r2t_Functions.py) and [Transform-To-Enhanced (T2N)](AvernusGlue/t2n_Functions.py) functions are very similar, as they both are trying to keep the minimum number of rows while maintaining the relevant information.

Both functions do the following:
- Removes duplicate rows, maintaining the first instance of each row only.

However, there are some differences between the jobs:

##### [Raw-to-Transform (R2T)](AvernusGlue/r2t_Functions.py)
- Fixes the column headers to follow the PySpark format
- Force the data types into the ones in the Data Dictionary - If this isn't described in the data dictionary, then keep the original/ transform the unknown into strings.

##### [Transform-To-Enhanced (T2N)](AvernusGlue/t2n_Functions.py)
- Deletes all columns that were mentioned in the T2N Template
- Hashes all the columns that were mentioned in the T2N Template
- Joins other tables to it.
- Can save multiple version of the data with different columns/ naming conventions.
- Can append to Enhanced data or Overwrite it.

#

#### Logs
The ETL Jobs also produce their own logs that function as a file with some metadata as to what has been performed, if any issues occured and which data types have been processed. The log file currently looks similar to the below:

```json
{
    "Data_Processed": {
        "Failed": {}, 
        "Sucessful": {
            "nnpac": {
                "NumOfRows": 0
            }, 
            "pho_enrolment_weekly": {
                "NumOfRows": 437531
            }
        }
    }, 
    "Received": [], 
    "Status": "Successful", 
    "stdoutLog": []
}
```
#

#### Use Case
1. Avernus Ferryman uploads files into an S3 bucket, in the folder X
2. Avernus Ferryman then triggers an AWS Glue trigger based on the folder name
3. The trigger starts the ETL R2T Template
-  - The template sends the Dynamic Frame to the R2T Function
-  - The function processes the data and sends it back into the template
4. Template saves new post-transformed data in a different bucket
5. Automatically trigger T2N job post completion
6. T2N repeats the same process as R2T
7. Final data is saved in a different bucket