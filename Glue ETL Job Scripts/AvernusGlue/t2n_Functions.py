from dateutil import parser
from functools import reduce

from pyspark.conf import SparkConf
from pyspark.context import SparkContext
from pyspark.sql import SparkSession
from pyspark.sql import DataFrame
from pyspark.sql.types import *

import ast
import boto3
import datetime
import dateutil.parser
import json

try:
	from AvernusGlue import AvernusUtilETL as au
	print('AVERNUS COMMENT - Used the "from xxx_folder import xXx as x" format')

except:
	import AvernusUtilETL as au
	print('AVERNUS COMMENT - Used the "import xXx as x" format')

import pyspark
import pyspark.sql.functions as f

import sys
import traceback

####################


#Options are:
# deletedColumns -> Delete the columns in the datatype dictionary
# joinColumns -> Join a column to another file (left outter join)
# roundedColumns -> round the values in the column to N (Default is 3)
# hashedColumns -> Hashes the column
# changeColType -> Changes column type to the specified, otherwise leave as string
#

uniqFunctions = ['joinTables','roundedColumns', 'deletedColumns', 'keepOnlyColumns', 'enhanceDrops', 'hashedColumns']

####################

#Function with the base transformation from Transformed to Enhance
#Iterates through the functions list and applies those to the DF
#Deletes all duplicates
#Saves the DF
#If current table exists, compare against it and only keep the first appearence
#Convert Spark Dataframe into AWS Glue Dynamic Frame

#Input:
#	datasource0 -> AWS Glue Dynamic Frame that is to be cleaned
#	detailsDict -> Dictionary with the Variables at the beggining of the file, naming should be as follows: 
#		detailsDict = {'RawDatabaseName':RawDatabaseName, 'CurrentDatabaseName':CurrentDatabaseName,'resultBucket':resultBucket, 'tableName':tableName }
#Returns:
#	output -> Dictionary with the processes's logs.

def BasicEnhancement(datasource0, detailsDict, output):
	
	#Checks if input value is a dataframe
	isDF = isinstance(datasource0,pyspark.sql.dataframe.DataFrame)
	
	if isDF == True:
		df = datasource0
	else:
		#Transform Dynamic Frame to Spark DataFrame
		df = datasource0.toDF()

	#Cleans the DF column headers, if necessary
	try:
		df = au.cleanDfColumns(df)
	except Exception as e:
		errorMessage = 'Could not clean the columns names because of {} and the full error is {}'.format(e,traceback.format_exc())
		raise Exception(errorMessage)
	
	#Iterates through the functions 
	#Only applies the ones detailed in the Data Dictionary
	for functionType in uniqFunctions:
		
		try:
			
			if functionType in detailsDict:

				dfFunctSettings = detailsDict[functionType]

				if functionType == 'joinTables':
					
					for joinStatement in dfFunctSettings:
						
						joinAttempt = au.JoinDf(df, detailsDict['spark'], joinStatement, output)

						df = joinAttempt['DF']
						output = joinAttempt['Log']


				if functionType == 'roundedColumns':
					df = au.RoundCols(df,dfFunctSettings)

				if functionType == 'roundTime':
					df = au.RoundTime(df,dfFunctSettings)

				if functionType == 'changeColType':
					updateTypes = au.changeColType(df,dfFunctSettings,output)
					df = updateTypes['DataFrame']
					output = updateTypes['Log']
				
				if functionType == 'deletedColumns':
					df = au.DelColumns(df,dfFunctSettings)
				
				if functionType == 'keepOnlyColumns':
					df = au.KeepCols(df,dfFunctSettings)

				if functionType == 'enhanceDrops':
					enhanceDrop = au.EnhanceDrops(df,detailsDict,output)

					df = enhanceDrop['DF']
					output = enhanceDrop['Log']
				
				if functionType == 'hashedColumns':
					df = au.HashCols(df, dfFunctSettings)

		except Exception as e:
			output['stdoutLog'].append('AVERNUS COMMENT: Function {} failed to run on {} because of {}'.format(functionType,detailsDict['tableName'],e))
			output['stdoutLog'].append('Full Error is: {}'.format(traceback.format_exc()))
			continue


	#Attempts to save the df directly into S3 as a parquet through spark
	#Calls an error if it fails
	try:
		saved_df_info = au.SavingProcess(df,detailsDict,output)
	
	except Exception as e:
		raise Exception('File failed to be created within Pyspark and threw the following error {}'.format(e))
		

	####################
	#Return log
	return saved_df_info
