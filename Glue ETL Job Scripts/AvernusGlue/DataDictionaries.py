import io
import os
import sys

import boto3
import yaml

from AvernusGlue import AvernusUtilETL as au

# Class contains the constructor that registers the Bucket tag in YAML files that tell which bucket to account for
class BucketTag:
    yaml_tag = u'!Bucket'

    def __init__(self, env_var):
        self.env_var = env_var

    def __repr__(self):
        return os.environ.get(self.env_var, '')

    @staticmethod
    def yaml_constructor(loader, node):
        return BucketTag(loader.construct_scalar(node))
    

# Data dictionary class -- FINALLY!
# Gets the data type from S3 bucket if it exists
# Else, fills with defaults and saves the new data type file on S3 to later be changed
class DataDictionary:
    
    # Default value
    rawBucket = ""
    transformedBucket = ""
    enrichdBucket = ""
    
    # Default Location
    dataDictBucket = ''
    s3Prefix = ''
    
    # To initiate Class
    # Input:
    # raw -> Raw bucket name [String] 
    # transformed -> Transform bucket name [String]
    # enriched -> Enrich bucket name [String]
    # dataDictBucket -> Bucket where the data dictionaries are located [String]
    # s3Prefix -> Path to the Data Dictionaries within the bucket [String]
    def __init__(self, raw = rawBucket, transformed = transformedBucket, enriched = enrichdBucket, dataDictBucket = dataDictBucket,  s3Prefix = s3Prefix):
        #Default buckets
        self.buckets = {'raw':raw, 'transformed':transformed, 'enriched':enriched}
        self.dataTypes = self.AvailableDatatypes()
        
        #Set Up buckets for Dictionaries
        self.SetUpBuckets()
        
        # Set up s3 Connection 
        self.s3_conn = boto3.client('s3')  #Start S3 client
        self.dataDictBucket = dataDictBucket
        self.s3Prefix = s3Prefix
        
        #Get Master list
        self.masters = self.MasterList()

    ############################################################
    # Function uses the Bucket dictionary and uses the keys
    # as environmental variables and the key's value as the  
    # environmental variable's value.
    # 
    # This is for the YAML constructor so that it can translate 
    # the internal variables.
    def SetUpBuckets(self):
        for bucketName in self.buckets:
            os.environ[bucketName] = self.buckets[bucketName] 
        
    ############################################################
    # Function looks for the data dictionary in the S3 directory
    # If no dictionary found, creates one with basic default value and saves
    # Input:
    # requestedData -> Datatype name, as string
    # Returns:
    # dataDictionary -> Dictionary with the processes for the data type 
    def getDictionary(self, requestedData):

        searchingDict = requestedData.lower()
        dataDictionary = {}
        
        createDict = True
        
        if searchingDict in self.dataTypes:
            createDict = False
            dataDictionary = self.getFile(searchingDict)
        
        dataDictionary['tableName'] = requestedData
        
        if  createDict == True:
            dataDictionary['tableType'] = 'reference'
            self.saveDict(dataDictionary)

        return dataDictionary

    ############################################################
    # Function Sets up YAML constructor and opens Data Dictionary YAML file from S3
    # 
    # Input:
    # dt -> Datatype, as string
    # Return:
    # dataLoaded -> Data Dictionary as Dictionary 
    #  
    def getFile(self, dt):
        
        # Format Filename
        filename = "{}/{}.yaml".format(self.s3Prefix, dt)
        
        # Get text content of file
        file_content = au.openS3Files(self.s3_conn, self.dataDictBucket,filename, {})
    
        # Read YAML file
        # Set up Bucket Tag reading
        yaml.Loader.add_constructor(BucketTag.yaml_tag, BucketTag.yaml_constructor)
        dataLoaded = yaml.load(file_content)
        return dataLoaded
    
    
    ############################################################
    # Function finds all DataType files on the S3 bucket/prefix
    #
    def AvailableDatatypes(self):
        dtFiles = au.BucketList(self.dataDictBucket, prefix = self.s3Prefix)
        
        dataTypes = []
        
        for f in dtFiles:
            newF = '/'.join(f.split('/')[2:])
            dataTypes.append(newF.split('.')[0])
        
        return dataTypes
        
        
    ############################################################
    # Function gets a data type and looks in the data dictionary
    # for the data type within the 'mainTables' lists and returns
    # a list of MasterDictionaries that contain the data type
    #
    # Input:
    # dt -> Data Type [String]
    # Returns:
    # foundDicts -> List of Master Dictionaries that contain dt [List]
    #
    def getMasterDicts(self, dt):
        foundDicts = []
        for masterList in self.masters:
            masterListClean = [x.lower() for x in masterList['mainTables']]
            if dt.lower() in masterListClean:
                foundDicts.append(masterList['dictName'])
            
        return foundDicts
                
    ############################################################
    # Function open the Master dictionary from S3
    #
    def MasterList(self):
        
        # Gets specific config file that has all the master tables and their dependencies
        # mainTable = whenever this table updates also update the master tables
        # nonUpdateTable = the master table does not need to be updated if this table is
        masterDict = self.getFile('MasterTables')
        masterList = masterDict['MasterTables']
    
        return masterList
    
    ############################################################
    # Function turns temp dictionary into YAML and uploads to s3 
    # If you would like to specify the name outside of the data 
    # dictionary [For example, updating Master Table] -
    # use the dataType variable as that will be the file's name
    #
    # Input:
    # dtDict -> Data Dictionary [Dict]
    # dataType -> Name to save the file, expected to be in dtDict but
    #can be explicitly defined here [String]
    # 
    # Returns:  
    # None
    #
    def saveDict(self, dtDict, dataType = ''):
        
        mainName = dataType
        if 'tableName' in dtDict: 
            mainName = dtDict['tableName']

        # Format new names
        tempFileName = '{}.yaml'.format(mainName.replace('/','_'))
        newFileName = '{}.yaml'.format(mainName)
        
        # Temporary saving file
        with io.open(tempFileName, 'w+', encoding='utf8') as outfile:
            yaml.dump(dtDict, outfile, default_flow_style=False, allow_unicode=True)

        #Add file to bucket
        s3 = self.s3_conn
        s3.upload_file(tempFileName, self.dataDictBucket, '{0}/{1}'.format(self.s3Prefix,newFileName))
        
        return