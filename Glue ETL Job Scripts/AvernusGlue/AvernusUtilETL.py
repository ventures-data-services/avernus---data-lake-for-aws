import ast
import copy
import csv
import datetime
import io
import json
import re
import sys
import traceback

import boto3
import dateutil.parser
import pyspark
import pyspark.sql.functions as f
import yaml

from dateutil import parser
from functools import reduce
from pyspark.conf import SparkConf
from pyspark.context import SparkContext
from pyspark.sql import DataFrame, SparkSession
from pyspark.sql.types import *

# Blank output variable, in case you want it to be ignored
output = {'Status': 'Started' , 'Received': [], 'Data_Processed': [], 'stdoutLog' : []}



###################################################################
# Removes any characters not allowed in column headers from a string
# Removes spaces, lowers name and changes any bad characters for an underline '_' 
#
# Input: 
# colName -> Column Header, as a string
# Returns:
# newColName -> Clean Column header
def cleanColumn(colName):

    #Invalid values that have to be removed from column names
    unsafeCharacters = [' ',',',';','{','}','(',')','\n','\t','=']

    newColName = colName.lower()

    newColName = ''.join(['_' if charac in unsafeCharacters else charac for charac in newColName ])
    
    return newColName

###################################################################

#Function receives two DataFrames
#Finds the schemas of both
#Compares the schemas, and if it finds any missing columns on either DataFrame, it adds those columns to the DF and fills it with None Values
#
#Input:
#df_x, df_y -> The two dataframes that are to be compared.
#Retuns:
#[df_x, df_y] -> The two dataframes with the missing columns added as a List
def columnCompletion(df_x, df_y):

    df_x = cleanDfColumns(df_x)
    df_y = cleanDfColumns(df_y)
    
    try:

        df_x_schema = [[f.name, f.dataType] for f in df_x.schema.fields]
        df_y_schema = [[f.name, f.dataType] for f in df_y.schema.fields]

        allMissing = []

        missingCols_x = [field for field in df_y_schema if field not in df_x_schema]
        missingCols_y = [field for field in df_x_schema if field not in df_y_schema]

        allMissing = missingCols_x + missingCols_y

        print('allMissing columns are {}'.format(allMissing))

        if len(allMissing) > 0:
            for missingCol in allMissing:
                try:
                    colname = '{0}_{1}'.format(missingCol[0], missingCol[1])

                    ogColName = str(missingCol[0])

                    if isinstance(missingCol[1],pyspark.sql.types.StructType):
                        for _ in missingCol[1]:
                            subName = _.name
                            subType = _.dataType

                            df_x = df_x.withColumn('{}_{}'.format(missingCol[0], subName), f.col('{}.{}'.format(missingCol[0],subName)).cast(subType))
                            df_y = df_y.withColumn('{}_{}'.format(missingCol[0], subName), f.col('{}.{}'.format(missingCol[0],subName)).cast(subType))

                        continue
                    
                    if missingCol in df_x_schema:
                        df_x = df_x.withColumn(colname, f.col(ogColName).cast(missingCol[1]))

                    if missingCol in df_y_schema:
                        df_y = df_y.withColumn(colname, f.col(ogColName).cast(missingCol[1]))
                    
                    if missingCol not in df_x_schema:
                        df_x = df_x.withColumn(colname, f.lit(None).cast(missingCol[1]))
                    
                    if missingCol not in df_y_schema:
                        df_y = df_y.withColumn(colname, f.lit(None).cast(missingCol[1]))

                except Exception as e:

                    print('AVERNUS COMMENT:')
                    print(missingCol)
                    print(e)
            
            dropColumns = [field[0] for field in allMissing]
            
            df_x = DelColumns(df_x, dropColumns)
            df_y = DelColumns(df_y, dropColumns)

        #Make sure the columns are in the right order -> Necessary for Union
        dfs_schema = [f.name for f in df_y.schema.fields]
        df_x = df_x.select(dfs_schema)
        df_y = df_y.select(dfs_schema)
                
    except Exception as e:
        x = 'AVERNUS COMMENT: Failed on columnCompletion because of {} full error is {}'.format(e,traceback.format_exc())
        print(x)
        raise Exception(x)
            
    return [df_x, df_y]

###################################################################

# This function opens files located in S3 Buckets and returns their content as UTF-8
#
#Input:
#	BucketName -> S3 Bucket name as string 
#	FileKey -> File's path ('NMDS/file01.parquet') as path
#
#Returns:
#	s3FileContent -> The file's content as a single UTF-8 string
#
def openS3Files(s3, BucketName, FileKey, output = output):
    try:
        s3File = s3.get_object(Bucket = BucketName, Key = FileKey)
        s3FileContent = s3File['Body'].read().decode('utf-8')
    except Exception as e:
        fullError = str(traceback.format_exc())
        errorName = str(e)
        output['stdoutLog'].append('AVERNUS COMMENT: File {0} failed to open. It threw the error {1}.'.format(FileKey, errorName))
        output['stdoutLog'].append('AVERNUS COMMENT: Full error for file {0} was {1}'.format(FileKey, fullError))
        return None
    
    return s3FileContent

###################################################################

#Opens Parquet file and transforms into PySpark DataFrame
#
# Input:
#	filename -> file's name as a string
#
# Return:	
#	File as a PySpark DataFrame
def OpenParquet(filename, spark):
    return spark.read.parquet(filename)

###################################################################

#Opens CSV file and transforms into PySpark DataFrame
#
# Input:
#	filename -> file's name as a string
#
# Return:	
#	File as a PySpark DataFrame
def OpenCSV(filename, spark):

    return spark.read\
    .option("header", "true")\
    .option("inferSchema", "true")\
    .option("multiLine", "true")\
    .option("header", True)\
    .option("delimiter", '|' )\
    .option("quote", '"')\
    .option('columnNameOfCorruptRecord','Broken_Row')\
    .option("escape", '\"')\
    .csv(filename)

###################################################################

# Function receives the filename to be opened
# Checks the extension and sends it to the appropiate function 
# Receives back a PySpark DataFrame and passes it along
# If any functions fail, it passes through None
#
# Input:
#	filename -> file's name as a string
#	output -> log dictionary
#
# Return:	
#	File as a PySpark DataFrame if sucessful
#	None if failed
def DataframeOpener(filename, spark, output):
    try:
        csvTypes = ['csv']
        
        fileType = filename.split('.')[-1].lower()

        if fileType == 'parquet':
            df = OpenParquet(filename,spark)
        
        if fileType in csvTypes:
            df = OpenCSV(filename,spark)
    
    except Exception as e:

        fullError = str(traceback.format_exc())
        errorName = str(e)

        errorString = 'AVERNUS COMMENT: File {0} failed to be transformed into Dataframe and will not be processed. It threw the error {1}.'.format(filename, errorName)

        output['stdoutLog'].append(errorString)
        output['stdoutLog'].append('AVERNUS COMMENT: Full error for file {0} was {1}'.format(filename, fullError))
        df = None

        raise Exception(errorString)
    
    return {'Dataframe': df, 'Log':output}

###################################################################

#Function attempts to save the 'output' dictionary as a json file in the log bucket
#If it fails, it saves a text file of the dictionary

#input:
#	output -> The logs of this file, as a Nested Dictionay
#Returns:
#	None
def SaveLog(output,s3,logDetails):
    logFolderName = logDetails['LogFolder']
    logBucketName = logDetails['LogBucket']
    
    #Naming convention information
    time_identifier = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    filename = '{}_log_{}.txt'.format(logFolderName, time_identifier)
    
    #Turn Nested dictionary into JSON
    try:
        file_content = json.dumps(output, indent=4, sort_keys=True)
    except Exception as e:
        print("AVERNUS COMMENT: didn't save as json, reason was {}".format(e))
        file_content = str(output)
    
    # Temporary saving file
    with open(filename, 'w+') as f:
        f.write(file_content)
    
    #Add file to bucket
    s3.upload_file(filename, logBucketName, '{0}/{1}'.format(logFolderName,filename))

###################################################################

#Clead dictionary from any empty/unused keys
def clean_empty(d):
    if not isinstance(d, (dict, list)):
        return d
    if isinstance(d, list):
        return [v for v in (clean_empty(v) for v in d) if v]
    return {k: v for k, v in ((k, clean_empty(v)) for k, v in d.items()) if v}

###################################################################

#Join multiple DataFrames together
#
#Input:
#dfs -> LIST of dataframes to be added together -- They must have the same schemas for this to work
#Returns:
#The combined PySpark Dataframe
def unionAll(*dfs):
    return reduce(DataFrame.unionAll, dfs)

###################################################################

#Function iterates through all rows in an already defined column
#Checks wether the value could be parsed as a date
#If value could be parsed as a date, returns the correctly formated string with the datetime values
#If not, returns 01/01/1900 value as it's version of blank

#Input:
#	x -> value in cell
#Returns:
#	y.strftime('%Y-%m-%d %H:%M:%S.%f') -> Date found/ blank value as correctly formatted String
def DateFormatting(x):
    try:
        if isinstance(x,datetime.datetime):
            y = x
        else:
            slashLocation = max(x.find('/'),x.find('-'))
            if slashLocation == 4:
                y = parser.parse(x)
            else:
                y = parser.parse(x, dayfirst = True)
        return y.strftime('%Y-%m-%d %H:%M:%S.%f')
    except:
        x = '1900-01-01'
        y = parser.parse(x)
        return y.strftime('%Y-%m-%d %H:%M:%S.%f')

###################################################################



###################################################################

#Removes all the columns in a list
#
# Input:
# df -> Dataframe (PySpark)
# columns -> list of columns to be removed from the dataframe
# Returns:
# df -> Dataframe with removed columns 
#
def DelColumns(df, columns):

    cleanCols = [cleanColumn(col) for col in columns]

    return df.drop(*cleanCols)

###################################################################
#Transforms the column into a Timestamp format
# 
# Input:
# df -> Dataframe
# column -> string, with the column's name
# Returns:
# df -> Dataframe with the column transformed into a timestamp, and a '_timestamp' added to the end of the column's name
#
def colTimeStamp(df,column):

    #Calls DateFormatting function and saves result in "columnName_timestamp"
    f_DateFormatting = f.udf(lambda x: DateFormatting(x), StringType())
    df = df.withColumn('{}_timestamp'.format(column), f_DateFormatting(f.col(column)))
    
    #Transform Values in "columnName_String" into timestamp data format and saves it in "columnName_timestamp"
    df = df.withColumn('{}_timestamp'.format(column), f.col('{}_timestamp'.format(column)).cast('timestamp'))

    return df

###################################################################
#
# Rounds timestamp columns
# The time varibale should be in seconds to round (E.G - 1 min = 60, 10 min = 600) as an int
# 
# Input: 
# df -> Dataframe 
# column -> Name of column [MUST be TimestampType already], string 
# time -> seconds to round to [60 if want to round to nearest minute], int
# Returns: 
# dataframe rounding the column, and naming it `COlUMN_rounded`
#
def RoundTime(df, column, time):
    if df.schema[column].dataType != TimestampType():
        errorMessage = 'Column {} is not the correct type to round - RoundTime Function'.format(column)
        raise Exception(errorMessage)

    return df.withColumn("{}_rounded".format(column), ((f.round(f.unix_timestamp(f.col(column)) / time) * time).cast("timestamp")))

###################################################################
#
# Iterates through a list of column, finds wether they are in the dataframe and changes them to the type explicity defined in the columnsLists
# columnslists MUST look like the following: 
# [{'columnName': 'name_column', 'columnType': 'string'}, {'columnName': 'number_column', 'columnType': 'float'}]
# 
# Input: 
# df -> Dataframe
# columnsLists -> List of dictionaries with the columns and their types
# output -> log dictionary
# Returns: 
# Dictionary with the following values:
#   {'DataFrame': df, 'Log': output}
# DataFrame -> dataframe with all the columns that could be changed, changed
# Log -> dictionary with the logs of the processes
#
def changeColType(df, columnsLists, output):

    ColList = df.schema.names

    for columnInfo in columnsLists:

        try:
            # Lower for better comparison
            colName = columnInfo['columnName'].lower()
            colType = columnInfo['columnType']

            if colName in ColList:

                if 'time' in colType.lower():
                    df = colTimeStamp(df, colName)
                else:
                    df = df.withColumn(colName, f.col(colName).cast(colType))
        
        except Exception as e:
            output['stdoutLog'].append('AVERNUS COMMENT: Column {} failed to be transformed into {} because of {} -- Full error:'.format(columnInfo['columnName'],columnInfo['columnType'],e))
            mainError = str(traceback.format_exc())
            output['stdoutLog'].append(mainError)
            continue
    
    return {'DataFrame': df, 'Log': output}

###################################################################

# Function attempts to remove duplicates based on column name and aggregates that column. 
# orderedAndIgnored MUST be like this:
#        {'columnName':Column header, 'columnType': Column Type(such as 'int' - [keep the aggregation in mind]), 'TypeAgg':f.min (Can be any aggregation function from PySpark)}
#  
# If it fails, it removes any duplicate rows it could find and notes that on the log
#  
#input:
#	orderedAndIGnored -> Column name that will be aggregated to, and then ignored during removal, as list of Dictionaries:
#		Should have the following values:
#		- columnName: String with the column's header
#		- columnType: Type the column is to be, during aggregation (Eg int,string )
#		- typeAgg: aggregation which will be done to the column. Can be any function on PySpark agg (To know all the options, please check the PySpark Aggregation documentation)
#	output -> Dictionary with the information which will be added to the log, as Dictionary
#	df -> Full Dataframe that is to have it's duplicates removed and aggregation done to
# Returns:
#	df -> Dataframe with duplicates removed based on column and aggregation done, and if column or aggregation fails, df with all duplicate rows removed.
#  
def RemoveDuplicatesBasedOnColumn(df, output, orderedAndIgnored):
    
    try:
        #Set up values for aggregation
        processingColumns = [colInfo['columnName'] for colInfo in orderedAndIgnored]
        aggExpressions = [colInfo['typeAgg'](f.col(colInfo['columnName'])) for colInfo in orderedAndIgnored]


        #Fix the columns to be in the correct type
        changedTypes = changeColType(df,orderedAndIgnored,output )

        df = changedTypes['DataFrame']
        output = changedTypes['Log']

        #Remove the Ignored from the list of columns to be grouped by
        columnList =  [col for col in df.schema.names if col not in processingColumns]
        
        #Groupby + Aggregation of the data
        df = df.groupBy(*columnList).agg(*aggExpressions)

        #Get the previous names and the current ones, and fix them by returning the aggregated column names to their previous naming.
        aggNewValues = [{'NewName':colName, 'OldName':originalName} for colName in df.schema.names for originalName in processingColumns if originalName in colName]
        
        for aggNames in aggNewValues:
            df = df.withColumnRenamed(aggNames['NewName'],aggNames['OldName'])
        
    except Exception as e:
        #Adds error to the logs

        ErrorMessage = 'AVERNUS COMMENT: Could not delete all duplicates based on the column named "{0}" as it threw the error {1}. \n AVERNUS COMMENT: FullError {}'.format(orderedAndIgnored, e,traceback.format_exc())
        raise Exception(ErrorMessage)

    return df, output

###################################################################
# Saves the Dataframe as parquet files
# It assumes it to be within the AWS environment, unless otherwise stated in the saveLocation variable.
#  
#
# Input:
# df -> DataFrame to be saved
# output -> Logs dictionary
# detailsDict -> Data Dictionary for the dataframe
# saveLocation -> Default None | Would be a string with the save location, if saving outside the AWS Environment
# Return:
#  The following Dictionary:
#       {'FinalDfCount':rowCount, 'Log':output }
# Where:
#   FinalDfCount -> Number of rows in DF [Size of the DF]
#   Log -> Log Dictionary
#  
def SaveDf(df, output, detailsDict, saveLocation = None):

    

    saveFormat = 'append'
    if 'saveFormat' in detailsDict:
        saveFormat = detailsDict['saveFormat']


    saveLoc = 's3'
    if 'saveLoc' in detailsDict:
        saveLoc = detailsDict['saveLoc']

    rowCount = 0
    try:
        #Cache DF
        df.cache()
        
        print('AVERNUS COMMENT: DF FOR {} SHOWN BELOW:'.format(detailsDict['tableName']))
        df.show(10)
        
        #Get size of new file (rows wise)
        rowCount = df.count()
        
        if saveLocation == None:
            #Write directory of DF
            if saveLoc == 's3':
                saveLocation = "s3a://{0}/{1}/".format(detailsDict['resultBucket'], detailsDict['tableName'])
            
            if saveLoc == 'local':
                saveLocation = "{0}/".format(detailsDict['tableName'])
        
        try:
            if rowCount > 0:
                numOfReparitions = (rowCount//5000000) + 1
                #Try to save it as DF in the saveLocation, with less than 5 million rows per file.
                df.repartition(numOfReparitions).write.parquet(saveLocation,mode=saveFormat)
            else:
                output['stdoutLog'].append('AVERNUS COMMENT: no new values to be added for {}'.format(detailsDict['tableName']))

        except Exception as e:
            #If failed, tries to save as a single file to minimise chance of error
            errorString = ('AVERNUS COMMENT: Failed to save with more than one repartition, reason was {}. Will be trying to save as one partition'.format(e))
            output['stdoutLog'].append(errorString)
            mainError = str(traceback.format_exc())
            print(traceback.format_exc())
            output['stdoutLog'].append(mainError)
            print(errorString)
            df.repartition(1).write.parquet(saveLocation,mode=saveFormat)
    
    except Exception as e:
        #If it fails, add fail reason to log and data type that caused it.
        # failOutputValues = {detailsDict['tableName']:{'Error':str(e)}}
        output['stdoutLog'].append('AVERNUS COMMENT: File failed to be created within PySpark')
        output['stdoutLog'].append('Error was: {}'.format(str(e)))
        mainError = str(traceback.format_exc())
        output['stdoutLog'].append(mainError)
        #raise Exception('File failed to be created within Pyspark and threw the following error {}'.format(mainError))
    
    return {'FinalDfCount':rowCount, 'Log':output }

###################################################################
# Compares two Dataframes and keeps only the values on the first (left) dataframe that are NOT in the right.
# You can tell it which columns to ignore and it creates a subset of the headers and deletes duplicates based on those. -- CURRENTLY ONLY WORKS WITH INT
#  
# Input:
# df -> Left DataFrame [MAIN] 
# comparingDF ->  Right DataFrame [COMPARED]
# output -> Logs dictionary
# colsToIgnore -> List of columns to ignore - CURRENTLY MUST BE INT COLUMNS
# Return:
#  The following Dictionary:
#       {'DF':df_save, 'Log': output}
# Where:
#   DF -> Dataframe with only values that were NOT in the comparingDF
#   Log -> Log Dictionary
#  
def LeftAntiJoinDFs(df, comparingDF, output, colsToIgnore = []):

    try:
        
        #Adds 'ComparisonColumn' column to the transforming df and the transformed df with different values to differenciate later
        df = df.withColumn('comparisoncolumn', f.lit(1).cast('int'))
        comparingDF = comparingDF.withColumn('comparisoncolumn', f.lit(0).cast('int'))
        
        #Checks both schemas and adds any missing columns from either
        #Then joins the DFs and returns a combined DF
        joinedDF = unionAll(*columnCompletion(df,comparingDF))

        #Tell it to ignore the comparioncolumn in the groupby 
        colsToIgnore.append('comparisoncolumn')
        orderedAndIGnoredDict = getSpecialTreatment(colsToIgnore)
        dataclean = RemoveDuplicatesBasedOnColumn(joinedDF, output, orderedAndIGnoredDict)

        #
        df = dataclean[0]
        output = dataclean[1]
        
        #Remove all rows already in the Transformed database
        #Remove comparison Column
        df_save = df.filter(df['comparisoncolumn'] == 1)
        df_save = df_save.drop('comparisoncolumn')
        
    except Exception as e:
        output['stdoutLog'].append('AVERNUS COMMENT: Failed the `LeftAntiJoinDFs` function because - {}'.format(e))
        output['stdoutLog'].append(str(traceback.format_exc()))
        raise Exception(e)

    return {'DF':df_save, 'Log': output}

###################################################################
# Hashes [Secure Hashing Algorith-2 (SHA2)] all columns in hashedList @ 256-bit
#  
# Input:
# df -> DataFrame 
# hashedList ->  list of strings 
# Return:
#  df -> Dataframe with the columns hashed
#  
def HashCols(df, hashedList):

    dfCols = df.schema.names

    #Make sure the input in the data dictionary is clean
    cleanHashedList = [cleanColumn(col) for col in hashedList]

    #Get all columns that need to be hashed
    checkedHashedList = [realCol for realCol in dfCols if realCol in cleanHashedList]

    #Hash columns in list
    for hashingColumn in checkedHashedList:
        df = df.withColumn(hashingColumn, f.sha2(f.col(hashingColumn), 256))
    
    return df

###################################################################
# Round all columns in roundList
# roundList can be like either of the following:
#   [['ColumnA'], ['ColumnB']] OR ['ColumnA', 'ColumnB'] OR -- IF you would like to choose how many to round to -- [['ColumnA', 1],['ColumnA', 2] ]
#  
# Input:
# df -> DataFrame 
# roundList ->  list with the column headers to be rounded and the numbers to round them down to.
# Return:
#  df -> Dataframe with the columns rounded
#  
def RoundCols(df, roundList):

    dfCols = df.schema.names
    fixedRoundList = []

    for roudingValues in roundList:

        if type(roudingValues) is not list:
            roudingValues = [roudingValues]

        clearedName = cleanColumn(roudingValues[0])

        fixedRoundList.append([clearedName, roudingValues[-1]])

    #Get all columns that need to be rounded
    checkedRouList = [roundList for roundList in fixedRoundList if roundList[0] in dfCols]

    #Round Columns
    for roudingValues in checkedRouList:
        roundNum = 3
        
        if type(roudingValues[-1]) is int: 

            roundNum = roudingValues[-1]

        print(roudingValues)
        df = df.withColumn(roudingValues[0], f.round(f.col(roudingValues[0]), roundNum))
    
    return df

###################################################################
# Opens the result folder [Where the current DataType will be saved] 
# and returns the already processed dataframe (if any) + a boolean stating wether this data type has been processed before + logs.
#  
# Input:
# detailsDict -> Data Dictionary for the data being processed
# output -> Log Dictionary
# Return:
# Dictionary with the following information:
#       {'DF':currentDbDf, 'tableExists':tableExists, 'output':output }
# Where:
# DF -> Dataframe, if it exists | None if it does not
# tableExists -> Boolean with TRUE if table exists and FALSE if not
# output -> Log dictionary
#  
def OpenResultFolder(detailsDict,output):
    #Checks if result folder already exists
    #If it does exist, creates a dataframe with the Transformed values
    tableExists = False

    s3 = boto3.client('s3')
    objList = s3.list_objects_v2(Bucket=detailsDict['resultBucket'],Prefix=detailsDict['tableName'])

    if objList['KeyCount'] > 0:

        try:
            currentDbDf_Folder = 's3a://{}/{}'.format(detailsDict['resultBucket'],detailsDict['tableName'])
            currentDbDf = OpenParquet(currentDbDf_Folder,detailsDict['spark'])
            tableExists = True
            
        except Exception as e:
            currentDbDf = None
            output['stdoutLog'].append('AVENUS COMMENT: -- OpenResultFolder error -- Could not open resultBucket as DF because of {}'.format(e))
            output['stdoutLog'].append(str(traceback.format_exc()))

    else:
        currentDbDf = None
        message = 'The result path {} in the bucket {} was not found and will not be joined.'.format(detailsDict['tableName'], detailsDict['resultBucket'])
        output['stdoutLog'].append(message)
    
    return {'DF':currentDbDf, 'tableExists':tableExists, 'Log':output }

###################################################################
# Opens a parquet folder as a single DF 
#  
# Input:
# spark -> spark context
# bucket -> bucket name, as string
# folder -> Folder path, as string 
# Return:
# Dictionary with the following information:
# df -> Dataframe, if it exists | Raises error,if it does not
#  
def OpenParquetFolder(spark, bucket, folder):

    try:
        formattedPath = 's3a://{}/{}'.format(bucket,folder)
        df = OpenParquet(formattedPath,spark)
        
    except Exception as e:
        errors = []
        errors.append('Could not open folder as DF because of{}\n'.format(e))
        errors.append(str(traceback.format_exc()))
        raise Exception(str(errors))
    
    return df

###################################################################
# Deletes all columns not in the KeepCols List 
#  
# Input:
# df -> Dataframe
# KeepCols -> List of columns to keep
# 
# Return:
# Call to delete all columns not in the keeoCols list / which returns the df 
#  
def KeepCols(df, keepCols):
    
    df_cols = df.schema.names
    colsToRemove = [x for x in df_cols if x not in keepCols]
    
    return DelColumns(df, colsToRemove)
    
###################################################################
# Join the current df to another dataframe.
#
# It expects a joinDict, which MUST be similar to the following:
            # {
            # 'joinType': 'inner', --> Can be any type of join accepted by PySpark
            # 'rightDataType': {
            #       'joinType': 'datatype',  --> Can be either datatype or singlefile
            #       'bucket': 'bucket', --> Bucket name, as string
            #       'dataType' : 'data/folder' --> Path to folder, within S3
            #                  },
            # 'leftOn': ['ColumnA', 'ColumnAB'], --> List of columns, must be the same length for leftOn AND rightOn & be in the same order 
            # 'rightOn': ['ColumnA', 'ColumnB'], --> List of columns, must be the same length for leftOn AND rightOn & be in the same order
            # 'suffix': 'y' --> Extension that will be added to any columns that appear in both.
            # } 
# If the 'saveAs' value is used in the dataframe, the join function joins the tables/makes any changes that were requested to either dataframes/ saves it and then RETURNS THE ORIGINAL DF
# 
#  
# Input:
# l_df -> Dataframe
# spark -> spark context
# joinDict -> Dictionary, as decribed above  
# output -> Log Dictionary
# r_df -> IF r_df has already been opened, you can join it by adding it to this arg | default is None 
# 
# Return:
# The following dictionary:
#       {'DF':df, 'Log':output}
# Where:
# DF -> It will be a dataframe, however it could be the JoinedDf || Fuzzy JoinedDF || Original Df , as Joined DF was 'SaveAs'
# Log -> Log Dictionary  
#  
def JoinDf(l_df, spark, joinDict, output, r_df = None):

    if 'joinType' not in joinDict:
         joinDict['joinType'] = 'left'

    if joinDict['joinType'] == 'union':
        df = unionAll(*columnCompletion(l_df,r_df))
        return {'DF':df, 'Log':output}

    #Gets the left dataframe's column names
    l_df_colNames = l_df.schema.names

    # If 'leftOn' in the joinDict is a STRING and not a LIST, it means the word will be used with a fuzzy join
    # A fuzzy join creates a list of all the columns that contain that word, and then runs the JoinDf function on each of those columns in the hopes some/all will join 
    if type(joinDict['leftOn']) is not list:
        return fuzzyJoin(df, spark, joinDict, output,'leftOn')
    
    if type(joinDict['rightOn']) is not list:
        return fuzzyJoin(df, spark, joinDict, output,'rightOn')
    
    # Set up 'saveAs' default value, if none input into dictionary
    if 'saveAs' not in joinDict:
        joinDict['saveAs'] = None

    # If 'saveAs' was used, save a copy of the left dataframe before any changes are made, which will be returned after the changed dataframe has been saved.
    if joinDict['saveAs'] != None:
        original_schema = copy.deepcopy(l_df.schema)
        original_l_df = l_df.rdd.zipWithIndex().toDF(original_schema)
        
    # Initiate right dictionary as an empty variable -- For Safety -- If RightDf was input, this will be ignored
    if r_df == None:
    
        try:

            # Creates the dataframe based on wether it will be joining from a single file or a full dataframe
            if joinDict['rightDataType']['joinType'] == 'singlefile':
                r_details = DataframeOpener(joinDict['rightDataType']['path'], spark, output)
                r_df = r_details['Dataframe']

            if joinDict['rightDataType']['joinType'] == 'datatype':
                r_df = OpenParquetFolder(spark,joinDict['rightDataType']['bucket'],  joinDict['rightDataType']['dataType'])
        
        except Exception as e:
            errorMessage = 'Could not open the file in the JoinDf function with the joinDict input of {}, received the error {} and the full error of {}'.format(joinDict, e,traceback.format_exc())
            raise Exception(errorMessage)
    
    #Clean the r_df and get it's schema
    r_df = cleanDfColumns(r_df)
    r_df_colNames = r_df.schema.names
    
    #Ensure the input columns are in the r_df
    newRightOn = []
    for joinCol in joinDict['rightOn']:
        cleanJoinCol_r = cleanColumn(joinCol)
        if cleanJoinCol_r in r_df_colNames:
            newRightOn.append(cleanJoinCol_r)
        else:
            mismatchColumns_error = 'Could not find the input column {} in the right dataframe. The columns for the R dataframe are {} \n Please check your dictionary before trying again.'.format(cleanJoinCol_l,r_df_colNames)
            raise Exception(mismatchColumns_error)
    
    joinDict['rightOn'] = newRightOn

    #Clean the l_df and get it's schema
    l_df = cleanDfColumns(l_df)
    l_df_colNames = l_df.schema.names

    #Ensure the input columns are in the l_df
    newLeftOn = []
    for joinCol in joinDict['leftOn']:
        cleanJoinCol_l = cleanColumn(joinCol)
        if cleanJoinCol_l in l_df_colNames:
            newLeftOn.append(cleanJoinCol_l)
        else:
            mismatchColumns_error = 'Could not find the input column {} in the left dataframe. The columns for the L dataframe are {} \n Please check your dictionary before trying again.'.format(cleanJoinCol_l,l_df_colNames)
            raise Exception(mismatchColumns_error)
    joinDict['leftOn'] = newLeftOn



    # Right keep columns
    rightKeep = r_df.schema.names
    if 'rightKeep' in joinDict:
        rightKeep = []
        for inputCol in joinDict['rightKeep']:
            rightKeep.append(cleanColumn(inputCol))

    #Find all the column headers that are the same on both l_df and r_df
    #Add suffix if therea are any matches
    #Updates the rightOn dictionary if any were found
    matching = [lCol for lCol  in l_df_colNames if lCol in r_df_colNames]
    
    if len(matching) > 0:
        r_suffix = joinDict['suffix']
        for col in matching:
            r_col_newName = cleanColumn("{}_{}".format(col, r_suffix))
            joinDict['rightOn'] = [r_col_newName if x == col else x for x in joinDict['rightOn']]

            if col in rightKeep:
                rightKeep.append(r_col_newName)

            r_df = r_df.withColumnRenamed(col, r_col_newName)

    r_df.show
    
    #Update l_df schema variabe
    l_df_colNames = l_df.schema.names

    # Keep left columns list 
    leftKeep = l_df.schema.names
    if 'leftKeep' in joinDict:
        leftKeep = []
        for inputCol in joinDict['leftKeep']:
            leftKeep.append(cleanColumn(inputCol))


    #########################
    
    # Check the column headers on the l_df, create new column with a new name and as a string to ensure that they connect correctly without interfering with the real data source
    leftList = []
    for colName in joinDict['leftOn']:
        if colName.lower() in l_df_colNames:
            nameForJoin = '{}_left'.format(colName.lower())
            leftList.append(nameForJoin)
            l_df = l_df.withColumn(nameForJoin, f.trim(f.col(colName).cast(StringType())))
        else:
            errorMessage = 'Could not join because the colName {} could not be found in the l_df. The current df schema is {}'.format(colName,l_df_colNames )
            raise Exception(errorMessage)

    #Update r_df schema variable
    r_df_colNames = r_df.schema.names

    # Check the column headers on the r_df, create new column with a new name and as a string to ensure that they connect correctly without interfering with the real data source
    rightList = []
    for colName in joinDict['rightOn']:
        if colName.lower() in r_df_colNames:
            nameForJoin = '{}_right'.format(colName.lower())
            rightList.append(nameForJoin)
            r_df = r_df.withColumn(nameForJoin, f.trim(f.col(colName).cast(StringType())))
        else:
            errorMessage = 'Could not join because the colName {} could not be found in the r_df. The current df schema is {}'.format(colName,r_df_colNames )
            raise Exception(errorMessage)
    

    # Join the DFs -- Finally!!! --
    df = l_df.join(r_df, [f.col(x) == f.col(y) for (x, y) in zip(leftList, rightList)], how = joinDict['joinType'])
    
    #Delete the join column for clarity
    df = DelColumns(df, joinDict['rightOn']+ leftList+ rightList)

    #########################

    #Get the final DF schema
    finalSchema = df.schema.names

    # Check if new columns will be added
    if len([col for col in rightKeep if col not in finalSchema]) == 0:
        errorMessage = 'No column from the join has been kept for the r_df in the join dictionary {}. This means either the R_DF does not have the columns in the rightKeep or there was an issue within the join process, please check'.format(joinDict)
        print(errorMessage)
        output['stdOut'].append(errorMessage)

    #Only keep the wanted columns
    df = KeepCols(df, leftKeep + rightKeep)
    
    #Return DF if 'saveAs' not defined
    if joinDict['saveAs'] == None:
        return {'DF':df, 'Log':output}
    
    #If 'saveAs' is defined - save it and then return original DF
    else:
        savedFiles = SaveDf(df, output, joinDict['saveAs'])
        return {'DF':original_l_df, 'Log':savedFiles['Log']} 


###################################################################
# Finds the correct saving method based on the data dictionary (or lack of saving method) and sends the df to be saved.
#
# Input:
# df -> Dataframe
# detailsDict -> Data dictionary
# output -> Log Dictionary
# Returns
#  Call to the correct saving function, which will have their own results
#  
def SavingProcess(df,detailsDict,output):

    saveFormat = 'append'

    if 'saveFormat' in detailsDict:
        saveFormat = detailsDict['saveFormat']
        
    
    #Check wether result folder already exists
    #If it does, returns a df with the folders information and True for the tableExists boolean
    #If not, returns None and False for the tableExists
    checkResultFolder = OpenResultFolder(detailsDict,output)
    output = checkResultFolder['Log']
    currentDbDf = checkResultFolder['DF']
    tableExists = checkResultFolder['tableExists']


    if saveFormat == 'append':
    #If table exists, only adds values that were not in the table
    #Else, created a new folder with the new information
        if tableExists == True:

            try:
                colsToIgnore = []

                if 'specialTreatment' in detailsDict:
                    colsToIgnore = detailsDict['specialTreatment']
                
                comparingDFS = LeftAntiJoinDFs(df, currentDbDf, output, colsToIgnore)
                
                print('AVERNUS COMMENT: GOT PAST LEFTANTIJOINDFs ')
                df2save = comparingDFS['DF']
                output = comparingDFS['Log']
                
            except Exception as e:
                exceptionTitle = 'Could not join the tables, because of {}'.format(e)
                raise Exception(exceptionTitle)
                
        else:
            # If no current table found, no need to compare
            df2save = df
    
    else:
        output['stdoutLog'].append('AVERNUS COMMENT: Saving format was not "append"')
        df2save = df
    
    return SaveDf(df2save, output, detailsDict)



###################################################################
# Iterates through all the DF column headers and ensures they are safe for the DF
# 
# Input:
# df -> Dataframe
# Returns
# df -> Dataframe with cleaned headers
#  
def cleanDfColumns(df):

    #Create list with all DataFrame Columnss
    ColList = df.schema.names
    
    #Iterate through list of columns
    for ColName in ColList:
        newName = cleanColumn(ColName)
        df = df.withColumnRenamed(ColName, newName)
    
    return df

###################################################################
# If the dataframe is to be saved on the current bucket BEFORE moving into the enhance functions, this function saves it and continues with the enhance process
# 
# Input:
# df -> dataframe
# detailsDict -> Data Dictionary
# output -> Log Dictionary
# Returns:
# Dictionary with the following values:
#       {'DF':DelColumns(df,dropCols),'Log':output}
# Where:
# DF -> dataframe with the deleted columns for enhanced
# Log -> Log Dictionary  
#  
#
def EnhanceDrops(df,detailsDict,output):

    dropCols = detailsDict['enhanceDrops']

    try:
        saveLocation = "s3a://{0}/{1}/".format(detailsDict['currentBucket'], detailsDict['tableName'])
        SaveDf(df, output, detailsDict, saveLocation)
    except Exception as e:
        errorMessage = 'Failed to save the dataframe in the current bucket in the function EnhanceDrops. Tried to save with the following path: {} \n It threw the error {}'.format(saveLocation, e)
        raise Exception(errorMessage)

    return {'DF':DelColumns(df,dropCols),'Log':output}


###################################################################
# Aggregates based on colsToIgnore
# colsToIgnore is to look like the following:
# [
# {'stringColumn':f.aggStringFunction}, -> This is because it expects the column to be an int column. If it isn't, you need to define which aggregation function you would like to use.
#  'intColumn', -> With an Int Column, it will aggregate to the minimum by default, since this is the most common use case for our data set
#  {'intColumn2': f.max} -> If you have an int Column that you want to aggregate but don't want to aggregate to min, explicitly define it in a dictionary
# ]
# 
# Input:
# colsToIgnore -> list, as explained above.
# Returns:
# orderedAndIGnoredDict -> List of Dictionaries correctly formatted for aggregation function.
#   following the above example, it will look similar to this:
#       [
#       {'columnName':'stringColumn', 'columnType':'string', typeAgg':f.aggStringFunction}, 
#       {'columnName':'intColumn', 'columnType':'int', 'typeAgg':f.min},
#       {'columnName':'intColumn2', 'columnType':'int', 'typeAgg':f.max}
#       ]
# 
def getSpecialTreatment(colsToIgnore):

    defaultDictionary = {'columnName':None, 'columnType':'int', 'typeAgg':f.min}

    orderedAndIGnoredDict = []
    
    for ignoredCol in colsToIgnore:
        
        if type(ignoredCol) == dict:

            for requirement in list(defaultDictionary):
                
                if requirement not in list(ignoredCol):
                    errorMessage = 'AVERNUS COMMENT: The specialTreatment dictionary was not set up correctly, missing {} on the dictionary {} and cannot continue the process. The full job has been cancelled.'.format(requirement,ignoredCol)
                    raise Exception(errorMessage)
                
                defaultDictionary[requirement] = ignoredCol[requirement]

            orderedAndIGnoredDict.append(defaultDictionary)

            
        if type(ignoredCol) == str:
            defaultDictionary['columnName'] = ignoredCol
            orderedAndIGnoredDict.append(defaultDictionary)
    

    if len(orderedAndIGnoredDict) == 0:
        errorMessage = 'Failed to process the specialTreatment correctly, the input {} was not read at all and needs to be checked for this data type. It should either be a list of strings or a Dicionary with the keys {}'.format(colsToIgnore, list(defaultDictionary))
        raise Exception(errorMessage)

    return orderedAndIGnoredDict


###################################################################
# Fuzzy join looks for all columns in the DFs schema that match the input value
# Iterates through the list of matches and attempts to join them all with the other DF
#
# Input:
# df -> Dataframe
# spark -> Spark Instance
# joinDict -> Dictionary with the join details, from the data dictionary
# output -> log dictionary
# fuzzySide -> String, which tells which side of the join is to be iterated through
# Return:
# Dictionary that looks like the below:
#   {'DF':df, 'Log':output}
# where:
#  DF -> joined DF
#  Log -> log dictionary
def fuzzyJoin(df, spark, joinDict, output,fuzzySide):

    fuzzyColumnMatching = [[col] for col in df if cleanColumn(joinDict[fuzzySide]) in col]

    for value in fuzzyColumnMatching:

        try:
            tempJoinDict = joinDict
            tempJoinDict[fuzzySide] = value
            joinOperation = JoinDf(df, spark, tempJoinDict, output)
            df = joinOperation[0]
            output = joinOperation[1]

        except Exception as e:
            fuzzyMatchingError = 'AVERNUS COMMENT: Could not work on the fuzzy column matching, failed on column {} when attempting {}. The join threw the error {}'.format(value,joinDict['leftJoin'], e )
            output['stdOut'].append(fuzzyMatchingError)
            print(fuzzyMatchingError)
        
        return {'DF':df, 'Log':output}

###################################################################

# Cleans the RTF from a string column.
# 
# Input:
# df -> Dataframe
# column -> string, with the column's name
# Returns:
# df -> Dataframe with the column cleaned from rtf 
#
def RTF_Extract(df,column):

    #Calls striprtf function and saves result in "columnName_extracted"
    f_textExtracting = f.udf(lambda x: striprtf(str(x)), StringType())
    df = df.withColumn('{}_extracted'.format(column), f_textExtracting(f.col(column)).cast('string'))

    return df

###################################################################
# Function clears any RTF from a string
# 
# Input:
# text -> string variable
# Returns:
# Clened string, with RTF removed and encoded/decoded from utf-16 to account for special characters.
def striprtf(text):
   pattern = re.compile(r"\\([a-z]{1,32})(-?\d{1,10})?[ ]?|\\'([0-9a-f]{2})|\\([^a-z])|([{}])|[\r\n]+|(.)", re.I)
   # control words which specify a "destionation".
   destinations = frozenset((
      'aftncn','aftnsep','aftnsepc','annotation','atnauthor','atndate','atnicn','atnid',
      'atnparent','atnref','atntime','atrfend','atrfstart','author','background',
      'bkmkend','bkmkstart','blipuid','buptim','category','colorschememapping',
      'colortbl','comment','company','creatim','datafield','datastore','defchp','defpap',
      'do','doccomm','docvar','dptxbxtext','ebcend','ebcstart','factoidname','falt',
      'fchars','ffdeftext','ffentrymcr','ffexitmcr','ffformat','ffhelptext','ffl',
      'ffname','ffstattext','field','file','filetbl','fldinst','fldrslt','fldtype',
      'fname','fontemb','fontfile','fonttbl','footer','footerf','footerl','footerr',
      'footnote','formfield','ftncn','ftnsep','ftnsepc','g','generator','gridtbl',
      'header','headerf','headerl','headerr','hl','hlfr','hlinkbase','hlloc','hlsrc',
      'hsv','htmltag','info','keycode','keywords','latentstyles','lchars','levelnumbers',
      'leveltext','lfolevel','linkval','list','listlevel','listname','listoverride',
      'listoverridetable','listpicture','liststylename','listtable','listtext',
      'lsdlockedexcept','macc','maccPr','mailmerge','maln','malnScr','manager','margPr',
      'mbar','mbarPr','mbaseJc','mbegChr','mborderBox','mborderBoxPr','mbox','mboxPr',
      'mchr','mcount','mctrlPr','md','mdeg','mdegHide','mden','mdiff','mdPr','me',
      'mendChr','meqArr','meqArrPr','mf','mfName','mfPr','mfunc','mfuncPr','mgroupChr',
      'mgroupChrPr','mgrow','mhideBot','mhideLeft','mhideRight','mhideTop','mhtmltag',
      'mlim','mlimloc','mlimlow','mlimlowPr','mlimupp','mlimuppPr','mm','mmaddfieldname',
      'mmath','mmathPict','mmathPr','mmaxdist','mmc','mmcJc','mmconnectstr',
      'mmconnectstrdata','mmcPr','mmcs','mmdatasource','mmheadersource','mmmailsubject',
      'mmodso','mmodsofilter','mmodsofldmpdata','mmodsomappedname','mmodsoname',
      'mmodsorecipdata','mmodsosort','mmodsosrc','mmodsotable','mmodsoudl',
      'mmodsoudldata','mmodsouniquetag','mmPr','mmquery','mmr','mnary','mnaryPr',
      'mnoBreak','mnum','mobjDist','moMath','moMathPara','moMathParaPr','mopEmu',
      'mphant','mphantPr','mplcHide','mpos','mr','mrad','mradPr','mrPr','msepChr',
      'mshow','mshp','msPre','msPrePr','msSub','msSubPr','msSubSup','msSubSupPr','msSup',
      'msSupPr','mstrikeBLTR','mstrikeH','mstrikeTLBR','mstrikeV','msub','msubHide',
      'msup','msupHide','mtransp','mtype','mvertJc','mvfmf','mvfml','mvtof','mvtol',
      'mzeroAsc','mzeroDesc','mzeroWid','nesttableprops','nextfile','nonesttables',
      'objalias','objclass','objdata','object','objname','objsect','objtime','oldcprops',
      'oldpprops','oldsprops','oldtprops','oleclsid','operator','panose','password',
      'passwordhash','pgp','pgptbl','picprop','pict','pn','pnseclvl','pntext','pntxta',
      'pntxtb','printim','private','propname','protend','protstart','protusertbl','pxe',
      'result','revtbl','revtim','rsidtbl','rxe','shp','shpgrp','shpinst',
      'shppict','shprslt','shptxt','sn','sp','staticval','stylesheet','subject','sv',
      'svb','tc','template','themedata','title','txe','ud','upr','userprops',
      'wgrffmtfilter','windowcaption','writereservation','writereservhash','xe','xform',
      'xmlattrname','xmlattrvalue','xmlclose','xmlname','xmlnstbl',
      'xmlopen',
   ))
   # Translation of some special characters.
   specialchars = {
      'par': '\n',
      'sect': '\n\n',
      'page': '\n\n',
      'line': '\n',
      'tab': '\t',
      'emdash': '\u2014',
      'endash': '\u2013',
      'emspace': '\u2003',
      'enspace': '\u2002',
      'qmspace': '\u2005',
      'bullet': '\u2022',
      'lquote': '\u2018',
      'rquote': '\u2019',
      'ldblquote': '\201C',
      'rdblquote': '\u201D',
   }
   stack = []
   ignorable = False       # Whether this group (and all inside it) are "ignorable".
   ucskip = 1              # Number of ASCII characters to skip after a unicode character.
   curskip = 0             # Number of ASCII characters left to skip
   out = []                # Output buffer.
   for match in pattern.finditer(text):
      word,arg,hex,char,brace,tchar = match.groups()
      if brace:
         curskip = 0
         if brace == '{':
            # Push state
            stack.append((ucskip,ignorable))
         elif brace == '}':
            # Pop state
            ucskip,ignorable = stack.pop()
      elif char: # \x (not a letter)
         curskip = 0
         if char == '~':
            if not ignorable:
                out.append('\xA0')
         elif char in '{}\\':
            if not ignorable:
               out.append(char)
         elif char == '*':
            ignorable = True
      elif word: # \foo
         curskip = 0
         if word in destinations:
            ignorable = True
         elif ignorable:
            pass
         elif word in specialchars:
            out.append(specialchars[word])
         elif word == 'uc':
            ucskip = int(arg)
         elif word == 'u':
            c = int(arg)
            if c < 0: c += 0x10000
            if c > 127: out.append(chr(c)) #NOQA
            else: out.append(chr(c))
            curskip = ucskip
      elif hex: # \'xx
         if curskip > 0:
            curskip -= 1
         elif not ignorable:
            c = int(hex,16)
            if c > 127: out.append(chr(c)) #NOQA
            else: out.append(chr(c))
      elif tchar:
         if curskip > 0:
            curskip -= 1
         elif not ignorable:
            out.append(tchar)

   return ''.join(out).encode('utf-16','surrogatepass').decode('utf-16')


###################################################################
# Get a list of all the values in a s3 bucket
# Can be filtered by prefix
# 
# Input:
# bucketName -> name of bucket to be searched
# prefix -> prefix to be filtered by, default is '' [No filter]
# delimiter -> What is the prefix separator
# fileList -> List of files that it will be appended to, this is for internal recursion
# Return:
# fileList -> List of all files in bucket
def BucketList(bucketName, prefix='', delimiter='/', fileList = []):
	
	#Start connection
	s3_conn   = boto3.client('s3')  # type: BaseClient  ## again assumes boto.cfg setup, assume AWS S3
	s3_result =  s3_conn.list_objects_v2(Bucket=bucketName, Prefix=prefix, Delimiter = "/")
	
	#If no files in bucket, return empty list
	if s3_result['KeyCount'] == 0:
		return fileList
	
	#For each folder found in top of bucket, run this function on it on it.
	if 'CommonPrefixes' in list(s3_result):
		for key in s3_result['CommonPrefixes']:
			#Get a list of all files within folder.
			prefixFileList = BucketList(bucketName, prefix=key['Prefix'], delimiter='/')
			
			#Only append files not already in fileList variable
			for prefixFile in prefixFileList:
				if prefixFile not in fileList:
					fileList.append(prefixFile)

	
	#Check if files [not folders] are inside of the folder.
	if 'Contents' in list(s3_result):
		
		#Append all filenames to list of files.
		for x in s3_result['Contents']:
			fileList.append(x['Key'])		
		
		#If folder has >1000 files, it will be truncated. this requests the other files in that list and adds them to the fileList. 
		if s3_result['IsTruncated'] == True:
			while s3_result['IsTruncated']:
				continuation_key = s3_result['NextContinuationToken']
				s3_result = s3_conn.list_objects_v2(Bucket=bucketName, Prefix=prefix, Delimiter="/", ContinuationToken=continuation_key)
				for key in s3_result['Contents']:
					fileList.append(key['Key'])
	
	return fileList