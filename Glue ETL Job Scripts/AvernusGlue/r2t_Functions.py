from dateutil import parser
from functools import reduce

from pyspark.conf import SparkConf
from pyspark.context import SparkContext
from pyspark.sql import SparkSession
from pyspark.sql import DataFrame
from pyspark.sql.types import *

import ast
import boto3
import datetime
import dateutil.parser
import json

import pyspark
import pyspark.sql.functions as f

import sys
import traceback

# Open package
try:
	from AvernusGlue import AvernusUtilETL as au
	print('AVERNUS COMMENT - Used the "from xxx_folder import xXx as x" format')
	
except:
	try:
		import AvernusUtilETL as au
		print('AVERNUS COMMENT - Used the "import xXx as x" format')
	except:
		print('AVERNUS COMMENT - Imports failed')
####################
#Initiate variable
rowCount = 0

#Unique functions
uniqFunctions = ['changeColType', 'RTF_Extract']


#Function with the base transformation from Raw to Transformed
#Iterates through the column names and checks the columns with the word "Date" or "time" in it. 
#Deletes all duplicates only maitaining the first appearence of all rows -> This is based on the File_Timestamp column
#If current table exists, compare against it and only keep the first appearence
#Convert Spark Dataframe into AWS Glue Dynamic Frame

#Input:
#	datasource0 -> AWS Glue Dynamic Frame that is to be cleaned
#	detailsDict -> Dictionary with the Variables at the beggining of the file, naming should be as follows {'RawDatabaseName':RawDatabaseName, 'CurrentDatabaseName':CurrentDatabaseName,'resultBucket':resultBucket, 'tableName':tableName }
#	output -> Log as Dictionary
#Returns:
#	output -> Log of the processes, as Dictionary

def BasicTransformation(datasource0, detailsDict, output):
	
	#Checks if input value is a dataframe
	isDF = isinstance(datasource0,pyspark.sql.dataframe.DataFrame)
	
	if isDF == True:
		df = datasource0
	else:
		#Transform Dynamic Frame to Spark DataFrame
		df = datasource0.toDF()

	# specialTreatment means that a column is to be ignored within the dataframe when removing duplicates
	#for example, if you have a column that is always an unique value (like time created), but you want to delete all the duplicates, this would prevent that.
	#to ignore those, make sure they are state in the file's data dictionary
	#If it can't find the 'specialTreatment' in the data dictionary, it just deletes all rows that are exact duplicates
	if 'specialTreatment' in detailsDict:
		orderedAndIGnoredDict = au.getSpecialTreatment(detailsDict['specialTreatment'])
		dataclean = au.RemoveDuplicatesBasedOnColumn(df, output, orderedAndIgnored = orderedAndIGnoredDict)
		df = dataclean[0]
		output = dataclean[1]
	
	else:
		df = df.dropDuplicates()
	

	# Cleans the DF columns headers
	# Remove any special characters/ uppercase / spaces 
	try:
		df = au.cleanDfColumns(df)
	except Exception as e:
		errorMessage = 'Could not clean the columns names because of {} and the full error is {}'.format(e,traceback.format_exc())
		raise Exception(errorMessage)
		

	
	# Iterates through the functions in the uniqFunctions list
	# If any of them are in the data dictionary, it passes through the information in the data dictionary to the function
	# If not, it moves on
	for functionType in uniqFunctions:
		try:
			if functionType in detailsDict:

				if functionType == 'changeColType':
					updateTypes = au.changeColType(df,detailsDict[functionType],output)
					df = updateTypes['DataFrame']
					output = updateTypes['Log']

				if functionType == 'RTF_Extract':
					updateTypes = au.changeColType(df,detailsDict[functionType],output)
					df = updateTypes['DataFrame']
					output = updateTypes['Log']

		except Exception as e:
			output['stdoutLog'].append('AVERNUS COMMENT: Function {} failed to run on {} because of {}'.format(functionType,detailsDict['tableName'],e))
			output['stdoutLog'].append('Full Error is: {}'.format(traceback.format_exc()))
	
	#Attempts to save the df directly into S3 as a parquet through spark
	#Calls an error if it fails
	try:
		saved_df_info = au.SavingProcess(df,detailsDict,output)
	except Exception as e:
		raise Exception('File failed to be created within Pyspark and threw the following error {}'.format(e))
		
	####################
	#Return log
	return saved_df_info