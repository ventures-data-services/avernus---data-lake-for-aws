# Avernus Data Lake Structure for AWS CloudFormation 
**Ventures, Dylan Byrne, 25/06/2019**

<img src="/Images/Ventures_logo_stack_col.jpg" alt="Ventures Logo" width="200" height="200" />

## Contents 
- [Overview](#overview)
  - [Description](#description)
  - [Your Scope](#your-scope)
- [Steps](#steps)
  - [Setup](#setup)
  - [Create the Avernus Data Lake Stack](#create-the-avernus-data-lake-stack)
  - [Usage](#usage)
- [Step Notes](#step-notes)
  - [Avernus Data Lake Parameters](#avernus-data-lake-parameters)
  - [Creating Glue ETL Job Scripts](#creating-glue-etl-job-scripts)
  - [Glue ETL Script Accessible Locations](#glue-etl-script-accessible-locations)
  - [Stack Name Prefix](#stack-name-prefix)
  - [Data Prefix](#data-prefix)
  - [Data Forms](#data-forms)
  - [SNS Notifications](#sns-notifications)
  - [Avernus Step Function Code Package](#avernus-step-function-code-package)
  - [Avernus Ferryman Checker Code Package](#avernus-ferryman-checker-code-package)
- [Structure](#structure)
  - [Resource List and Resource Naming Conventions](#resource-list-and-resource-naming-conventions)
  - [Stack Outputs](#stack-outputs)
  - [Stack Deletion](#stack-deletion)
- [Modifying the Avernus Data Lake](#modifying-the-avernus-data-lake)
  - [Adding More Resources](#adding-more-resources)
  - [Making more Glue Jobs](#making-more-glue-jobs)
    - [Add the job to the Stack](#add-the-job-to-the-stack)
    - [Add the job to the Data Processing State Machine](#add-the-job-to-the-data-processing-state-machine)
  - [Removing Avernus Ferryman](#removing-avernus-ferryman)

## Overview
### Description
This CloudFormation structure creates a data lake, using three S3 Buckets for the three stages of data, an S3 bucket for storing logs, and AWS Glue as the workhorse for discovering schemas, converting, and transforming data, among other things. For automation it uses Step Functions with lambda functions for sorting, Glue Job orchestration, notification sending, and error handling. It also has SNS functionality for updates on Glue job and State Machine state changes.

This data lake was also built around the New Zealand public sector health market, and as such some functionality and processes are tied to this context. However, efforts have been made to make the Avernus Data Lake solution generic, so it should be able to meet a wide variety of use cases with minimal modification.

The AWS services this project uses are as follows:

- Athena
- CloudFormation
- CloudWatch
- Glue
- IAM
- Lambda
- S3
- SNS
- Step Functions

This README and project assume you have some understanding of the AWS ecosystem, especially some of the more common terms and services (such as IAM).

### Your Scope
Parts of functionality of this project might not be useful for your scope.

The Avernus Ferryman code provides a solution to downloading files over a SFTP connection and then uploading the files to a S3 bucket. There is also Avernus Ferryman Checker code for lambda functions, used for checking and acting (SNS notifications, running the State Machine) based on the contents of the logs created by the Avernus Ferryman. If your scope does not need this and you wish to remove it refer to the [Removing Avernus Ferryman](#removing-avernus-ferryman) section in this README.

## Steps
### Setup
Before creating the stack in CloudFormation, you will need to complete the following steps:
1. Use the scripts in Glue ETL Job Scripts directory or [Create all Glue ETL Job scripts](#creating-glue-etl-job-scripts)
2. [Place these scripts in a Glue Service accessible location](#glue-etl-script-accessible-locations)
3. Set up the [Avernus Step Function Code Package](#avernus-step-function-code-package) for the lambda function code
4. Set up the [Avernus Ferryman Checker Code Package](#avernus-ferryman-checker-code-package) for the lambda function code

### Create the Avernus Data Lake Stack
Now you can create the stack:
1. Specify the AvernusDataLakeStructure.yaml file for the template when you create a new CloudFormation stack
2. Enter a unique stack name for your account
3. [Enter the parameters](#avernus-data-lake-parameters) for your data lake
4. Acknowledge CloudFormation might create IAM resources (it will)
5. Wait for the stack to be created
6. Upload your data to the Raw data bucket with a unique [prefix](#data-prefix) per [data type](#data-forms) AND/OR place them in the Landing prefix and have them sorted by the Avernus State Machine (if your datatypes are configured in the avernus-landing-judge)
7. Run the Avernus State Machine with your data types set up as an input, or discoverable through the avernus-landing-judge
8. Crawl each bucket with the appropriate Glue Crawler created by the stack to make your data quarriable
9.  At this stage your data should occupy all three data buckets in various stages of transformation, with Glue Databases that contain tables for your data, for each data stage
10. Subscribe to the appropriate [SNS topics](#sns-notifications) for you, Glue SNS topic for glue job state change notifications, the Step Function topic for if unknown files are encountered, state machine execution changes, and for state machine output, and the Ferryman Topic for notifications if the Ferryman has errors
11. Complete the extra setup for the Avernus Ferryman Log Checker described in the [README](/Avernus&#32;Ferryman&#32;Checkers/README.md) of the Ferryman Checker directory for full automation

### Usage
From now on you can use the Avernus State Machine to process the data automatically (from Raw stage to Enhanced stage). The state machine should be started automatically when the *FerrymanLogCheckerFunction* finds a log that indicates that new files have been uploaded. The state machine does not run the Glue Crawlers, but you should only ever need to run the crawlers whenever there is a schema change, or a new data type is added. More detailed information about the Avernus State Machine can be found in it's [README](/Avernus&#32;Step&#32;Function/README.md).

## Step Notes
### Avernus Data Lake Parameters

| **Parameter Name** | **Description** | **Example** |
| -------- | -------- | -------- |
| **BucketKeyARN** | KMS key ARN for decrypting buckets | arn:aws:kms:region:account:key/keynum |
| **FerrymanCheckerCodeKey** |  The file name key of your Ferryman Checker code package | Prefix/FerrymanChecker.zip |
| **FerrymanRunningCheckerRate** | The hourly schedule for the Ferryman running checker (eg 7 would mean the function runs every 7th hour), this is also the threshold value the function uses (see Ferryman Checker README for more info). This value has to be be at least 2 | 7 |
| **LambdaCodeBucket** | The S3 bucket where all the lambda code packages are kept | lambda-code-s3 |
| **PrefixName** | [Enter a prefix](#stack-name-prefix) that will be used on all resources that can use hyphens, in the recommended form {environment}-{team}- | dev-company- |
| **PrefixUnderscoreName** | [Enter a prefix](#stack-name-prefix) that will be used on resources that can't use hyphens, in the recommended form {environment}\_{team}\_ | dev\_company\_ |
| **RawDataJobExtraPyFirst** | Path for the extra python files for the first raw data job | s3://bucket-name/prefix/r2textra.py |
| **RawDataJobPathFirst** | Path for the Glue ETL Job script for the first raw data job | s3://bucket-name/prefix/r2tscript.py |
| **StepFunctionCodeKey** | The file name key of your Ferryman Checker code package | Prefix/StepFunctionCode.zip |
| **TransformedDataJobExtraPyFirst** | Path for the extra python files for the first transformed data job | s3://bucket-name/prefix/t2nextra.py |
| **TransformedDataJobPathFirst** | Path for the Glue ETL Job script for the first transformed data job | s3://bucket-name/prefix/t2nscript.py |

### Creating Glue ETL Job Scripts
Reading through the AWS documentations or editing a pre-generated ETL script is probably a good way to start making Glue Job ETL Scripts from scratch.

For the targets and source for making your ETL scripts the names of the buckets and databases follow predictable naming conventions you can use below:

- buckets: {your prefix}datalake-{data stage}-s3
- databases: {your prefix}datalakedatabase-{data stage}-glu
- database tables: {your data prefix lowercase}
- data stage being one of raw, transformed, or enriched
- more info on naming conventions can be found [here](#resource-list-and-resource-naming-conventions)
Or you can use inputs from the stack as arguments to your job when you create the resource in the template for this kind of information.

Current Raw and Transformed job IAM roles give the following permissions:
- Raw data jobs: access to the raw, transformed, and log bucket
- Transformed data jobs: access to the transformed, enriched, and log bucket
- Both kinds of jobs have access to the *AWSGlueServiceRole* AWS managed role.

You may have a reason for not being able to create ETL scripts at runtime of the stack (such as needing the crawled schema of a data source). If this is the case it is recommended that you create place holder script[s] for the jobs, so the jobs created are built with the correct IAM roles and naming conventions, and then replace them later when you can create them.

More information about adding more jobs to the stack, is provided in the [Making more Glue Jobs](#making-more-glue-jobs) section. More detailed info about the scripts provided is in the [README](/Glue&#32;ETLn&#32;Job&#32;Scripts/README.md) within the Glue ETL Job Scripts directory in this project.

### Glue ETL Script Accessible Locations
In the IAM roles for the ETL jobs it takes on the AWS managed AWSGlueServiceRole which gives it access to buckets with the 'aws-glue-scripts prefix' So, it is recommended you place the ETL scripts in an 'aws-glue-scripts' prefixed bucket you have in your account. If you want to place it in a custom location you will have to edit the *AvernusDataLakeStructure.yaml* file to include the permissions the script location, in the IAM data job roles. (*RawDataJobRole*, *TransformedDataJobRole*)

### Stack Name Prefix
The recommended prefix is {environment}-{team}-, but if you wanted multiple data lakes for the same environment and team you will need to add an extra part to avoid name conflicts, such as {environment}-{team}-{project}-. The names must comply with the bucket naming convention, so they must not contain uppercase characters or underscores, must start with a lowercase letter or number, and can contain lowercase letters, numbers, and hyphens.

Some resources cannot have hyphens in their names, so you must input a parameter for a prefix with underscores instead of hyphens. It is recommended you use the same names as you did for the normal prefix, in the same form, such as {environment}\_{team}\_ or {environment}\_{team}\_{project}\_.

### Data Prefix
The prefix is added in the form 'your-prefix/'. So, a data file with the name 'diceData.csv' with the prefix 'dice/' would be uploaded with the name 'dice/diceData.csv'. This is the same system AWS uses to make folders in buckets.

### Data Forms
A separate data form is data in a different format or significantly different schema.

### SNS Notifications
| **Topic Name** | **Description** |
| -------- | -------- |
| **GlueJobTopic** | Notifications about Glue Job state changes (eg SUCCEEDED, FAILED, TIMEOUT), in the form of a json |
| **StepFunctionTopic** | Notifications from the Avernus State Machine, if the state machine fails or succeeds in an expected manner it will send the current json output of the machine, if it does not finish in an expected manner the CloudWatch event rule will still send a notification about it. Notifications from Lambda functions inside the state machine will also be sent to this topic. |
| **FerrymanTopic** | Notifications if the Ferryman is uploading logs in the specified threshold time, and if the logs indicate the Ferryman has not got the status COMPLETE |

### Avernus Step Function Code Package
To create the Step Function in the data lake stack you will have to provide the code for lambda functions in way accessible to the stack. All the code required is in the Avernus Step Function directory of this project, and the required files are *avernus-bookmark-find-unpro.py*, *avernus-glue-log-checker.py*, *avernus-job-checker.py*, *avernus-landing-checker.py*, *avernus-landing-judge.py*, *avernus-last-judged-checker.py*, *avernus-state-log-sns.py*, *avernus-sort-only-checker.py*, and *bucketLister.py*.

An archive of all the files is provided in the directory also.

1. Archive all required files (recommended format is zip)
2. Place the archive into a S3 bucket
3. input this S3 bucket into the parameters of the CloudFormation stack ([*StepFunctionCodeBucket*](#avernus-data-lake-parameters))
4. input the key (file name with folder extension) of the archive into the parameters of the CloudFormation stack ([*LambdaCodeBucket*](#avernus-data-lake-parameters))

More info about this code is provided in the Avernus Step Function directory in the [README](/Avernus&#32;Step&#32;Function/README.md).

### Avernus Ferryman Checker Code Package
To create the Step Function in the data lake stack you will have to provide the code for lambda functions in way accessible to the stack. All the code required is in the Avernus Ferryman Checkers directory of this project, and the required files are *avernus-ferryman-log-checker.py*, *avernus-ferryman-running-checker.py*, and *bucketListerMod.py*.

An archive of all the files is provided in the directory also.

1. Archive all required files (recommended format is zip)
2. Place the archive into a S3 bucket
3. input this S3 bucket into the parameters of the CloudFormation stack ([*LambdaCodeBucket*](#avernus-data-lake-parameters))
4. input the key (file name with folder extension) of the archive into the parameters of the CloudFormation stack ([*FerrymanCheckerCodeKey*](#avernus-data-lake-parameters))

More info about this code is provided in the Avernus Ferryman Checkers directory in the [README](/Avernus&#32;Ferryman&#32;Checkers/README.md).

## Structure

<img src="/Images/AvernusTemplate20190521.png" alt="Stack Diagram" width="979" height="827" />

### Resource List and Resource Naming Conventions
Where {datastage} is one of raw, transformed, or enhanced, and {IDcreatedByAWS} is a value created by AWS on stack creation.
- 3 S3 Buckets, one for each data stage
  - Raw Data Bucket, Transformed Data Bucket. Enriched Data Bucket
  - {prefix}datalake-{datastage}-s3
- A S3 bucket for logs
  - {prefix}datalake-logs-s3
- A Glue Database for each data stage
  - Raw Database, Transformed Database, Enriched Database
  - {PrefixUnderscore}datalakedatabase_{datastage}_glu
- A Glue Crawler for each data bucket
  - Raw Data Crawler, Transformed Data Crawler, Enriched Data Crawler
  - {prefix}datalakeCrawler-{datastage}-CRL
- Glue ETL Jobs
  - Raw Data Job, Transformed Data Job
  - {prefix}datalakeJob-{datastage}-ETL
- Avernus Data Processing State Machine
  - {PrefixName}AvernusStateMachine-SFN
- Lambda Functions
  - 8 functions for the Data Processing State Machine
  - 2 functions for the Ferryman Checker
  - {PrefixName}-{FunctionName}-{IDcreatedByAWS}
- Lambda Schedule resources
  - Event rule for the running schedule
  - Lambda permission for the schedule
- IAM Roles
  - A role for each data crawler
    - Raw Data Crawler Role, Transformed Data Crawler Role, Enriched Data Crawler Role
  - A role for each set of jobs per bucket
    - Raw Data Jobs Role, Transformed Data Job Role
  - A role for the Avernus State Machine
  - A role for lambda functions
  - {PrefixName}-{RoleName}-{IDcreatedByAWS}
- SNS Resources for glue job state change notifications, Step Function notifications, and Ferryman notifications
  - A SNS Topic for each
    - {PrefixName}DataLakeGlueJobs
    - {PrefixName}DataLakeStepFunction
    - {PrefixName}DataLakeFerryman
  - A single Topic Policy for all topics
  - A Cloud Watch Event Rule for glue state changes, detecting in any glue job within the data lake
    - {PrefixName}datalakeevent-CWT
  - A Cloud Watch Event Rule for Avernus Step Function when its execution state changes to FAILED, TIMED_OUT, or ABORTED. Sends to the State Machine SNS
    - {PrefixName}datalakeevent-stepfunction-CWT

### Stack Outputs
The Avernus Data Lake has the following outputs (it also exports these as the names provided):
- Raw Data Bucket resource name and ARN
  - {PrefixName}datalake-raw-s3-name
  - {PrefixName}datalake-raw-s3-arn
- Transformed Data Bucket name and ARN
  - {PrefixName}datalake-transformed-s3-name
  - {PrefixName}datalake-transformed-s3-arn
- Enriched Data Bucket name and ARN
  - {PrefixName}datalake-enriched-s3-name
  - {PrefixName}datalake-enriched-s3-arn
- Log Bucket name and ARN
  - {PrefixName}datalake-logs-s3-name
  - {PrefixName}datalake-logs-s3-arn
- Raw Data Database name
  - {PrefixName}datalakedatabase-raw-glu-name
- Transformed Data Database name
  - {PrefixName}datalakedatabase-transformed-glu-name
- Enriched Data Database name
  - {PrefixName}datalakedatabase-enriched-glu-name
- Each SNS topic name
  - {PrefixName}DataLakeGlueJobs-name
  - {PrefixName}DataLakeStepFunction-name
  - {PrefixName}DataLakeFerryman
- Glue Job Event Rule name
  - {PrefixName}datalakeevnt-CWT-name
- State Machine Event Rule name
  - {PrefixName}datalakeevent-stepfunction-CWT

### Stack Deletion
When the stack is deleted all resources should be deleted except for S3 buckets that contain files. To fully delete the stack it is recommended that these files are either deleted manually, or moved to a backup location, before the deletion of the stack.

## Modifying the Avernus Data Lake
### Adding More Resources
It is recommended if you want to add more resources to your data lake that you do this through an update to the CloudFormation stack template. This means your new resources are tied to the stack, and can make use of the parameters and resource references in it. This also means if you wish to re-deploy or delete your stack, this new resource is included in this process.

The Avernus Data Lake [exports](#stack-outputs) many of its resource names and ARNs for cross stack reference, as part of its output, if you wish to use these resources in another stack.

### Making more Glue Jobs
#### Add the job to the Stack
This structure, as it is currently, only contains one Raw Data Glue Job, and one Transformed Data Glue Job. However you need more Glue Jobs, so you can follow the bellow steps to add them to the CloudFormation stack. The following steps use adding a second raw data job to the stack as an example, and assumes you have already made the script, and placed it in an [accessible location](#glue-etl-script-accessible-locations).

1. Add a new parameter for the Glue Job's script location
```yaml
RawDataJobPathSecond:
  Type: String
  Default: 's3:///myid/a-job-name'
  Description: Path for the Glue ETL Job script for the new job
```
2. Add a new parameter for an identifying name for the new job, with no spaces or special characters
```yaml
RawDataJobNameSecond:
  Type: String
  Default: 'jobName'
  Description: Identifying name for the new job
```
3. Optionally add a new parameter for an extra python file path
```yaml
RawDataJobExtraPySecond:
  Type: String
  Default: 's3:///myid/extra-functions.py'
  Description: Path for extra python files for the second raw job
```
4. Set up IAM role for your job. If your job is fairly standard it should be able to make use of either the *RawDataJobRole*, or the *TransformedDataJobRole* created by the stack. But if it needs to access different resources you will have to modify a role, or create a new IAM role.
5. Add the Glue Job resource to the stack, using the new parameters you've created. You can also modify the settings of the job based on it's needs such as the `AllocatedCapacity`. All default arguments are optionally but the ones described below are those which are used by the provided job scripts, so are also likely to be used if you use them as the base for your job scripts.

| **Argument** | **Description** | **Example** |
| -------- | -------- | -------- |
| `'--DATATYPES'` | This argument is used for determining which datatypes will be proccessed by the job. It must be inputted as a string representation of a list. | `'[]'` or `'['dataType1', 'dataType2']'` |
| `'--PRODATAFILES'`| A string representation of a dictionary of data type keys, that have a list of files to be processed as the value | `'{}'` or `"{'Bucket' : 'dev-logBucket', 'FileKey' : 'ProDataFile.txt'}"` |
| `'--LOGBUCKET'` | The name of the bucket the job should write logs to. It is recommended this should be a reference to the log bucket created by the stack. | `!Ref LogBucket` or `'dev-logs-s3'` |
| `'--LOGPREFIX'` | The prefix attached to log files written by the job. It is recommended that an unique prefix is used for each job, probably a variation on that job's name. | `'myjob'` |
| `'--ORIGINBUCKET'` | The name of the bucket where the data files to be processed are | `!Ref dataBucket` or `'dev-datastage-bucket'` |
| `'--CURRENTDATABASENAME'` | The name of the target database used by the job, usually connected to the bucket where the result is being stored. | `!Ref my_database` or `'database_name'` |
| `'--RESULTBUCKETNAME'` | The name of the S3 bucket where the job is writing its result, usually the bucket connected to target database | `!Ref TransformedDataBucket` or `'dev-mybucket-s3'`|
| `'--extra-py-files'` | The path of a python file for additional code to be imported into your job script. It is recommended if you need a custom python file for your job you input this new file's path as a parameter (see above step) | `!Ref MyPathParam` or `'s3:///myid/extra-functions.py'` |

```yaml
RawDataJobSecond:
  Type: 'AWS::Glue::Job'
  Properties:
    Role: !GetAtt 
      # put in your IAM policy below
      - RawDataJobRole
      - Arn
    Description: Job for an ETL in the raw data bucket source
    Command:
      Name: glueetl
      # Put your job path parameter below
      ScriptLocation: !Ref RawDataJobPathSecond
    AllocatedCapacity: 10
    ExecutionProperty:
      MaxConcurrentRuns: 1
    Name: !Join 
      - ''
      - - !Ref PrefixName
        - datalakeJob-raw-
        # put your name parameter below
        - !Ref RawDataJobNameSecond
        - -ETL
    DefaultArguments:
      # set up your args as appropriate
      '--DATATYPES': '[]'
      '--PRODATAFILES' : '{}'
      '--LOGBUCKET' : !Ref LogBucket
      '--LOGPREFIX' : 'R2T_Second'
      '--CURRENTDATABASENAME' : !Ref TransformedDatabase
      '--RESULTBUCKETNAME' : !Ref TransformedDataBucket
      '--ORIGINBUCKET' : !Ref RawDataBucket
      '--extra-py-files': !Ref RawDataJobExtraPySecond
```
6. Add the new job to the CloudWatch event rule
```yaml
GlueJobCloudWatchEventRule:
  Type: 'AWS::Events::Rule'
  Properties:
    Name: !Sub '${PrefixName}datalakeevent-CWT'
    EventPattern:
      source:
        - aws.glue
      detail-type:
        - Glue Job State Change
      detail:
        jobName:
          - !Join 
            - ''
            - - !Ref PrefixName
              - datalakeJob-raw-ETL
          - !Join 
            - ''
            - - !Ref PrefixName
              - datalakeJob-transformed-
              - '-ETL'
          # add your new job name below
          - !Join 
            - ''
            - - !Ref PrefixName
              # put your data stage below
              - datalakeJob-raw-
              # put your name parameter below
              - !Ref RawDataJobNameSecond
              - -ETL
    Targets:
      - Arn: !Ref GlueJobTopic
        Id: glueJobTopic
  DependsOn:
    - RawDataJobFirst
    - TransformedDataJobFirst
    # add your job resource here
    - RawDataJobSecond
```
#### Add the job to the Data Processing State Machine
At this stage you should have a fully implemented glue job that can be run, but to improve automation and/or process management it is recommended that you also add this new glue job to the Avernus State Machine. This will also require you to edit some of the Lambda functions used by the state machine. 

You can do this by following the steps below, using the same job above as an example (and using the same resources  we created in adding the job to the stack). This also assumes that your new job follows similar behavior to the job scripts provided (format of logs), so you may need to make some modifications. 

1. Add your new job as an environment variable to the *JobLogCheckerFunction*.

   ```yaml
   JobLogCheckerFunction: 
         Type: "AWS::Lambda::Function"
         Properties: 
           # MORE PROPERTIES ...
           Environment:
             Variables:
               logBucket: !Ref LogBucket
               snsARN: !Ref StepFunctionTopic
               RawJob1: !Ref RawDataJobFirst
               TransformedJob1 : !Ref TransformedDataJobFirst
               # add your job below
               RawJob2: !Ref RawDataJobSecond
           # TAGS ...
   ```

2. Add your new job as an environment variable to the *BookmarkFindUnproFunction*. 

   ```yaml
   BookmarkFindUnproFunction: 
         Type: "AWS::Lambda::Function"
         Properties: 
           # MORE PROPERTIES ...
           Environment:
             Variables:
               logBucket: !Ref LogBucket
               RawJob1: !Ref RawDataJobFirst
               TransformedJob1 : !Ref TransformedDataJobFirst
               RawBucket: !Ref RawDataBucket
               TransformedBucket: !Ref TransformedDataBucket
               # add your job below
               RawJob2: !Ref RawDataJobSecond
           # TAGS ...
   ```

3. In the code for the *JobLogCheckerFunction* edit the following function.
```python
# in_event = the lambda event to get the current job from
# returns the log prefix and bookmark prefix if it is a known job or ERROR_UNKNOWN if it is not
def get_job_args_glog(in_event):
    if in_event['currentJob'] == os.environ["RawJob1"]:
        return {"Prefix" : "R2T", "Bookmark" : "Raw_Bookmark"}
    elif in_event['currentJob'] == os.environ["TransformedJob1"]:
        return {"Prefix" : "T2N", "Bookmark" : "Transformed_Bookmark"}
    # add your new job below
    elif in_event['currentJob'] == os.environ["RawJob1"]:
        # Prefix = what the glue log prefix is (same as the --LOGPREFIX glue resource argument)
        # Bookmark = what the bookmark file prefix is
        return {"Prefix" : "R2TSecond", "Bookmark" : "Raw_Second_Bookmark"}
    else:
        return {"Prefix" : "ERROR_UNKNOWN", "Bookmark" : "ERROR_UNKNOWN"}
```

4. In the code for the *BookmarkFindUnproFunction* edit the following function.

```python
# in_event = the lambda event handler that contains currentJob key
# returns dict {"Prefix : "Jobs_Bookmark_Prefix", "Bucket" : "name-of-source-bucket"}
def get_log_prefix_pro(in_event):
    if in_event['currentJob'] == os.environ["RawJob1"]:
        return {"Prefix" : "Raw_Bookmark", "Bucket" : os.environ['RawBucket']}
    elif in_event['currentJob'] == os.environ["TransformedJob1"]:
        return {"Prefix" : "Transformed_Bookmark", "Bucket" : os.environ['TransformedBucket']}
    # add your new job below
    elif in_event['currentJob'] == os.environ["RawJob1"]:
        # Prefix = what the bookmark file prefix is (same as the Bookmark value in the previous step)
        # Bucket = what source bucket the data is in
        return {"Prefix" : "Raw_Second_Bookmark", "Bucket" : os.environ['RawBucket']}
    else:
        return {"Prefix" : "ERROR_UNKNOWN", "Bucket" : "ERROR_UNKNOWN"}
```

5. Create pass state to replace the current job variable. This is also the entry point to this Glue Job section so you need to set another state's `"Next"` value to reference this state.

```json
"SetR2TJobSecond": {
    "Type": "Pass",
    "Result": "${RawDataJobSecond}",
    "ResultPath": "$.currentJob",
    "Next": "RawSecond2ProFinder"
},
```
6. Create a task state for finding which files in process with the *BookmarkFindUnproFunction*.

``` json
"RawSecond2ProFinder": {
    "Type": "Task",
    "Resource": "${BookmarkFindUnproFunction.Arn}",
    "ResultPath": "$.judgeOut.GlueArgs",
    "TimeoutSeconds": 600,
    "Next": "R2TJobSecond"
},   
```

7. Create a task state for the new glue job. The `"HeartbeatSeconds"` set to `1` causes an automatic timeout, which is caught by the catch statement, so the job can be polled directly by the state machine for more control and reliability.

```json
"R2TJobSecond": {
  "Type": "Task",
  "Resource": "arn:aws:states:::glue:startJobRun.sync",
  "HeartbeatSeconds": 1,
  "Parameters": {
    "JobName": "${RawDataJobSecond}",
    "Arguments.$": "$.judgeOut.GlueArgs"
  },
  "Next": "WaitR2TjobSecond",
  "Catch": [
    {
      "ErrorEquals": [ "States.Timeout" ],
      "Next": "WaitR2TjobSecond"
    }
  ]
},
```
8. Create the wait state for job polling. The current poll time is 300 seconds (5 minutes), but you may want to adjust this based on your Glue Job's expected run time

```json
"WaitR2TjobSecond": {
  "Type": "Wait",
  "Seconds": 300,
  "Next": "R2TJobSecondChecker"
},
```
9. Create the task state for the *JobCheckerFunction* lambda. The lambda function gets the job name to poll from the `"$.currentJob"` and returns its findings to its result path. This lambda function is already created by the stack so no further set up outside of this state needs to be done.

```json
"R2TJobSecondChecker": {
  "Type": "Task",
  "Resource": "${JobCheckerFunction.Arn}",
  "ResultPath": "$.jobCheckerOutR2TSecond",
  "TimeoutSeconds": 30,
  "Next": "R2TJobSecondFinished?"
},
```
10. Create a choice state that checks the result of the *JobCheckerFunction* stored in the result path of the previous state. If the result is `"FAIL"`, then it moves to the **SetEndTypeFail** state, which is the failure notification path of the state machine. If the result is `"SUCCEEDED"` it will move to the next part of the state machine, with the Glue Job having run successfully. If the result is `"WAIT"` then it will return to the wait state set up in step 3.

```json
"R2TJobSecondFinished?": {
  "Type": "Choice",
  "Choices": [
    {
      "Variable": "$.jobCheckerOutR2TSecond",
      "StringEquals": "FAIL",
      "Next": "SetEndTypeFail"
    },
    {
      "Variable": "$.jobCheckerOutR2TSecond",
      "StringEquals": "SUCCEEDED",
      "Next": "RawJobSecondLogChecker"
    },
    {
      "Variable": "$.jobCheckerOutR2TSecond",
      "StringEquals": "WAIT",
      "Next": "WaitR2TjobSecond"
    }
  ],
  "Default": "SetEndTypeFail"
},
```
11. Create a task state for the *JobLogCheckerFunction* to check the output of the job's log file.
``` json
"RawJobSecondLogChecker": {
    "Type": "Task",
    "Resource": "${JobLogCheckerFunction.Arn}",
    "ResultPath": "$.judgeOut",
    "TimeoutSeconds": 30,
    "Next": "RawSecondDataTypes?"
},
```

12. Finally create a choice state to check if there are still data types to process, where **NextStep** is the next state in the machine.

```json
"RawSecondDataTypes?": {
    "Type": "Choice",
    "Choices": [
        {
            "Variable": "$.judgeOut.TypesEmpty",
            "BooleanEquals": true,
            "Next": "SetEndTypeFail"
        }
    ],
    "Default": "NextStep"
},
```
Your Glue Job should now be fully set up in the state machine as well as in the CloudFormation stack. To add even more Glue Jobs just follow the steps through again for each additional job.

### Removing Avernus Ferryman
If your scope does not require the Avernus Ferryman you can follow the below steps to remove it from the Cloudformation stack.

1. Remove the following resources: *FerrymanTopic*, *FerrymanRunningCheckerFunction*, *FerrymanLogCheckerFunction*, *FerrymanRunningSchedule*, *LambdaSchedulePermission*
2. Remove the reference to the *FerrymanTopic* in the *LambdaRole* resource
3. Remove the following Parameters: *FerrymanCheckerCodeBucket*, *FerrymanCheckerCodeKey*, *FerrymanRunningCheckerRate*
4. Remove *OutFerrymanTopicName* in Outputs

At this stage the Avernus Ferryman should be completely deleted from the Cloudformation stack. However, the Avernus Data Lake uses the *FerrymanLogCheckerFunction* to automatically run the State Machine based on the Avernus Ferryman log files, so you may want to find a new method for automatically running the state machine (such as a schedule).