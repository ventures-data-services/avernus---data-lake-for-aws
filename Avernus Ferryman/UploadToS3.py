# Ventures Pinnacle Incorporated
# Pedro Mroninski
# 12/04/2019
##############################
#Packages
import boto3
import time
import glob
import json
import os
import pandas as pd
import re
import traceback

from tqdm import tqdm
from datetime import datetime
import FerrymanUtil as fu
##############################################
#Variables
#Log Info
#Initiates tracking dictionary
FilesDicts = {}
##############################################


#Custom error to help identify files which will not be uploaded
#Exception <- String with the custom exception that will be used as the Exception in the error log
class BrokenFile(Exception):
    pass

				
#This funtion uploads all the data in the tempFiles folder, and possible tags for it
#It creates a connection with the defined AWS S3 Bucket, and a list of all the files in the tempFiles
#Checks the file naming convention against the "prefix_setting.csv" file to get the appropiate tags and command
#Formats the tags to fit the AWS CLI Formatting
#Uploads the file to the pre-defined folder
#Uploads the tags for the file
#Runs command post upload
#Deletes the file from the local directory
#
#Input:
#	None
#Return:
# 	log -> Log dictionary that was initiated at the top of the file. Returns as Dictionary.
def Main(internalLogs):
	##############################
	#File Variables

	# S3 resource and clients
	s3 = boto3.client('s3')

	#S3 Bucket
	bucketName = 'bucket-name'

	#Tags in Dictionary, please fill tags that will always be used to these files:
	tags = {'Project':'Avernus', 'Team':'Venture', 'Role':'Data File', 'Purpose':'', 'Environment':'Dev', 'DataSource':''}

	#File extensions that are to be uploaded
	allowedExtensions = ['csv','psv','txt', 'parquet']

	#File Location
	localFolder=r'tempfiles/'

	
	##############################
	#Program
	
	#Initiate lists that will be later used
	listOfFiles = []
	
	#find all the files in current directory with the 'psv' extension
	listOfFiles = fu.FilesByExtention(allowedExtensions)
	
	#Import CSV File with the mapping for the prefix values
	dfPrefix = pd.read_csv('Prefix_Settings.csv')
	
	#Initiate dictionaries for files
	filesDict = {}
	
	#Find all files that were not processed correctly 
	brokenFiles = []
	for file in internalLogs['Files']:

		if 'UpdateError' in internalLogs['Files'][file]:
			brokenFiles.append(file)

		if 'WasExtracted' in internalLogs['Files'][file]:
			if 'ExtractFailed' in internalLogs['Files'][file]['WasExtracted']:
				brokenFiles.append(file)
				
	#loop through list of files 
	#classify their tags
	#format the tags to fit AWS requirement
	#upload the file to defined bucket and folder with the tags
	for file in tqdm(listOfFiles):
		try:
			
			fileDictKey = fu.RemoveExtension(fu.RemovePath(file))
			
			#Start dictionary for file being uploaded
			fileInfo = {fileDictKey:{}}
			
			
			if fileDictKey in brokenFiles:
				tqdm.write('File {} will not be uploaded as it was not processed correctly'.format(file))
				raise BrokenFile('File cannot be uploaded due to error found in either Extraction or Update, please check.')
				
			
			filename = file[len(localFolder):]
			filename = fu.RemoveExtension(filename)
			
			uploadName = file.split("\\")[-1]
			fileInfo[fileDictKey].update({'UploadName':uploadName})
			
			#
			#Deliminate to determine file type
			#
			filename = filename.split("_")
			
			filetype = []
			
			for section in filename[:2]:
				try:
					int(section[0])
				except:
					section = re.sub('[^A-Za-z0-9]+', '', section)
					filetype.append(section)

			filetype = ' '.join(filetype)
			
			#Creates a DataFrame based on classification found -- This information is gathered from the 'Prefix_Settings.csv'
			currentSettings = dfPrefix.loc[dfPrefix['Prefix'] == filetype]
			currentSettings = currentSettings.reset_index(drop=True)
			
			#If the data type was not found
			if currentSettings.shape[0] == 0:
				raise ValueError('Prefix {0} had not yet been mapped, if you believe it was already mapped please check spaces, capitalisation as these have to be exactly the same.'.format(filetype))
			
			#Remove the information from the DataFrame and populate relevant variables/lists
			fileFolder = currentSettings['Folder'][0]
			tags['DataSource'] = currentSettings['DataSource'][0]
			tags['Purpose'] = currentSettings['Purpose'][0]
			
			
			#Adding to log dictionary
			fileInfo[fileDictKey].update({'UploadFolder':fileFolder})
			fileInfo[fileDictKey].update({'DataType':currentSettings['Purpose'][0]})
			
			
			#Format tag to fit the AWS Requirement by creating a temporary dictionary and separating the key/value with the correct naming convention
			tag_dict = {'TagSet': []}
			for key,value in tags.items():
				temp_dict = {'Key': '', 'Value': ''}
				temp_dict['Key'] = key
				temp_dict['Value'] = value
				tag_dict['TagSet'].append(temp_dict)
				
			#Upload file to S3
			tqdm.write('Uploading file --- {0} ---'.format(file))
			s3.upload_file(file, bucketName, '{0}/{1}'.format(fileFolder,uploadName))
			
			#Upload tags to file
			tqdm.write('Uploading the following tags --- {0} ---\n'.format(tags))
			s3.put_object_tagging( Bucket=bucketName,Key='{0}/{1}'.format(fileFolder,uploadName), Tagging= tag_dict)
			
			
			#Remove file from local directory
			tqdm.write('The file was uploaded and tagged\nRemoving file from local directory\n')
			os.remove(file)
			filesDict.update(fileInfo)
			
		
		except Exception as e:
			#If any files fail, saves error within log dictionary
			tqdm.write("File {} failed to be uploaded to S3".format(file))
			tqdm.write("Raised error: {}".format(e))
			
			errorReason = 'Error was {0}, Details reason is: {1}'.format(e, traceback.format_exc())
			 
			fileInfo[fileDictKey].update({'UploadError':errorReason})
			filesDict.update(fileInfo)
			continue
		
	print('All files have been uploaded')
	
	return filesDict