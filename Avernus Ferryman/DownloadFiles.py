# Ventures Pinnacle Incorporated
# Pedro Mroninski
# 12/04/2019
##############################
import boto3
import os
import pickle
import paramiko
import pandas as pd

import FerrymanUtil as fu

from datetime import datetime
from itertools import chain

##############################################
#Variables

#Base log that will be returned once main() function is completed
log = {'DownloadFiles':{}}
##############################################


#Function finds the latest file from a list of Paramiko File objects
#Iterates through them and returns the file with the largest timestamp

#Input:
#	sftpDirStats -> List of SFTP (Paramiko) Objects located in the Directory
#Returns:
#	latestFileObject -> Single object of the latest file in that directory
def FindLatestFile(sftpDirStats):
	
	latest = 0
	latestFileObject = None
	
	for fileattr in sftpDirStats:
		if fileattr.st_mtime > latest:
			latest = fileattr.st_mtime
			latestFile = fileattr.filename
			latestFileObject = fileattr
	
	return latestFileObject



#Function formats  the name from a Paramiko Object
#Removes the extension, and any directory information and adds in the timestamp to the end of it
#
#Input:
#	fileObject -> Paramiko File object
#Returns:
#	formattedFilename -> Formatted filename as String 
def FileNameFormatting(fileObject):
	
	fileTimestamp = fileObject.st_mtime
	
	fileName = fileObject.filename
	
	fileNameList = fileName.split('.')
	
	fileExtension = fileNameList[-1]
	
	fileNameList[-1] = '_'+str(fileTimestamp)+'.'+fileExtension
	
	formattedFilename = ''.join(fileNameList)
	
	return formattedFilename

	
#Progress bar for downloading files, shows how much of the file has been downloaded as a percentage.
#
#Input:
#	transferred -> Number of bytes already downloaded as int
#	toBeTransferred -> File size in bytes as int
#Return:
#	None
def PrintTotals(transferred, toBeTransferred):
	percentage = 100/toBeTransferred * transferred
	info = "Downloaded  {0:.1f}% of the file".format(round(percentage,1))
	print (info, end="\r")
	

#Saves the name of the files that have been downloaded and/or are in the tempFolder directory
#Input:
#	alreadySaved -> List of files already saved, as list of strings
#Return
#	alreadySaved -> List of files already saved, trimmed and updated with any new files in tempFolder
def SaveLog(alreadySaved, log = log):
	
	cwd = os.getcwd()
	localpath = cwd+r'\tempFiles\\'
	
	#Find local files, remove extensions
	localFiles = []
	for fullFileName in os.listdir(localpath):
		localFiles.append(fu.RemoveExtension(fullFileName))
	
	alreadySaved +=  localFiles
	
	#Remove Duplicates and remove empty values
	alreadySaved = list(filter(None, alreadySaved))
	alreadySaved = list(dict.fromkeys(alreadySaved))
	
	#Save list of already downloaded files into the log
	with open('log', 'wb') as fp:
		pickle.dump(alreadySaved, fp)
	
	return alreadySaved
	
	
#Downloads only the latest file from the path mentioned in the "Sources.csv" 
#Input:
#	Transport with the connection's login details as Paramiko Transport
#	Path - Location where the files that will be downloaded are as a String
#	Localpath - Location where the files are downloaded to as a String
#	AlreadySaved - list of files that are already downloaded as a List of Strings
#	Log - file's log, default uses the one alrady created in the beginning of the file
#Returns:
#	List of file names as List of String
def Download_Latest(transport, path,localpath, alreadySaved, dataType, log = log):
	# Start Connection!
	sftp = paramiko.SFTPClient.from_transport(transport)
	latestFile = FindLatestFile(sftp.listdir_attr(path))
	sftp.close()
	
	if latestFile.filename is not None:
		
		downloadFileName = latestFile.filename
		latestFile = FileNameFormatting(latestFile)
		
		fileComaparison = fu.RemoveExtension(latestFile)
		print('\n')
		
		
		if fileComaparison in alreadySaved:
			print('Latest file {} has already been downloaded and will not be re-downloaded, moving to next connection\n'.format(latestFile))
		else:
			sftp = paramiko.SFTPClient.from_transport(transport)
			print('Downloading {}'.format(latestFile))
			try:
				sftp.get('{0}{1}'.format(path,downloadFileName), localpath+latestFile, callback =PrintTotals)
				sftp.close()
			except Exception as e:
				print('File {0} count not be saved due to {1}'.format(latestFile, e))
				sftp.close()
				os.remove(localpath+latestFile)
				print('file has been removed')
			SaveLog(alreadySaved)
			print('Moving to next connection\n')
			return

#Downloads all the files in the path mentioned in the "Sources.csv" 
#Input:
#	Transport with the connection's login details as Paramiko Transport
#	Path - Location where the files that will be downloaded are as a String
#	Localpath - Location where the files are downloaded to as a String
#	AlreadySaved - list of files that are already downloaded as a List of Strings
#	Specifics - If the file is looking for a key distinction (For example, in the filename there is FINANCIAL in it) you can use that to only download all the files with that specifics. For default uses '' but value needs to be a String
#	Log - file's log, default uses the one alrady created in the beginning of the file
#Returns:
#	List of file names as List of String
def Download_All_SpecificName(transport, path,localpath, alreadySaved, dataType, specifics = '', log = log):
	sftp = paramiko.SFTPClient.from_transport(transport)
	latestFile = FindLatestFile(sftp.listdir_attr(path))
	sftp.close()
		
	if latestFile is not None:
		latestFile = FileNameFormatting(latestFile)
		fileComaparison = fu.RemoveExtension(latestFile)
	
		if fileComaparison in alreadySaved:
			print('No new files in the data type {} \n'.format(dataType))
		
		else:
		
			DownloadedFiles = []
			
			sftp = paramiko.SFTPClient.from_transport(transport)
			files = sftp.listdir_attr(path)
			sftp.close()
			
			files.sort(key = lambda f: f.st_mtime, reverse=False)
			
			numOfFiles = len(files)
			fileCount = 0
			
			print('Found {} to be checked, will be running these against the logs and downloading any files not found.'.format(numOfFiles))
			
			for f in files:
				ComparisonFile = FileNameFormatting(f)
				ComparisonFile = fu.RemoveExtension(ComparisonFile)
				
				if specifics not in ComparisonFile:
					continue
				
				if ComparisonFile not in alreadySaved and specifics in ComparisonFile:
				
					fileCount += 1
				
					newFilename = FileNameFormatting(f)
					sftp = paramiko.SFTPClient.from_transport(transport)
					print('Downloading {}'.format(f.filename))
					try:
						sftp.get('{0}{1}'.format(path,f.filename), localpath+newFilename, callback =PrintTotals)
						sftp.close()
						print('\n')
						DownloadedFiles.append(latestFile)
						
						#Decomment the below if sending the file straight into S3 without any changes
						#s3 = boto3.client('s3')
						#s3.upload_file(localpath+latestFile, 'dev-ventures-datalake-raw-s3', 'Untouched_Files/{}'.format(newFilename))
						
					except Exception as e:
						print('File {0} count not be saved due to {1}'.format(newFilename, e))
						os.remove(localpath+newFilename)
						print('file has been removed')
						
						if 'FailedDownloads' not in log['DownloadFiles']['DataSources'][dataType]:
							log['DownloadFiles']['DataSources'][dataType].update({'FailedDownloads':{latestFile:{'Reason':e}}})
						else:
							log['DownloadFiles']['DataSources'][dataType]['FailedDownloads'].update({latestFile:{'Reason':e}})
					
					SaveLog(alreadySaved)
			
			if len(DownloadedFiles) > 0:
				log['DownloadFiles']['DataSources'][dataType].update({'DownloadedFiles': DownloadedFiles})
				
			if fileCount > 0:
				log['DownloadFiles']['DataSources'][dataType].update({'DownloadsAttempted':fileCount})
			
	if latestFile is None:
		print('No files found in the directory for {0}, the current path used is {1}'.format(dataType, path))
	
	print('All files checked, moving to next connection\n')
	
	return



			
#Function connects to the S3 Raw bucket and gets a list of all files
#Checks the tempFiles folder to see if any files have been downlaoded
#Opens the "Sources.csv" and takes out the login information and download details
#Creates a Paramiko Transport Object with the login details
#Uses the information from "Sources.csv" to decide what function to call
#Compares the latest file there with all the files in the log
#If the latest file is not saved in the Log, it downloads the file
#If the latest file is in the log, it does not download from that connection
#If download fails due to connection/timeout/lack of space, the file is delete post fail and function continues
#
#Input:
#	None
#
#Returns:
# Nothing
#
def Main():
	#Local directories
	cwd = os.getcwd()
	localpath = cwd+r'\tempFiles\\'

	#S3 Directory
	bucketName = 'ventures-datalake-logs-s3'
	
	try:
		with open ('log', 'rb') as fp:
			alreadySaved = pickle.load(fp)
	except:
		alreadySaved = SaveLog([])
	

	#Get full list of files in S3
	s3BucketFiles = fu.BucketList(bucketName)
	
	#Remove directory + extension from filenames and add them to log
	for bucketFile in s3BucketFiles:
		bucketFile = fu.RemovePath(bucketFile)
		bucketFile = fu.RemoveExtension(bucketFile)
		alreadySaved.append(bucketFile)

	alreadySaved = SaveLog(alreadySaved)

	# Read "Sources.csv" File that contains the login details and information in regards to path, and downloading method.
	sourceDf = pd.read_csv('Sources.csv')

	log['DownloadFiles'] = {'DataSources':{}}
	for index, row in sourceDf.iterrows():
		
		dataType = row['data']
		
		log['DownloadFiles']['DataSources'].update({dataType:{}})
		
		print("Getting files from {}".format(row['data']))
		
		# Open a transport
		host = row['host']
		transport = paramiko.Transport((host))

		# Auth
		password = row['password']
		username = row['username']
		transport.connect(username = username, password = password)

		log['DownloadFiles']['DataSources'][dataType].update({'Pickupmethod': row['pickupMethod']})
		
		#Check upload method
		if row['pickupMethod'] == 'all':
			Download_All_SpecificName(transport, row['path'], localpath, alreadySaved, dataType)
		
		
		if row['pickupMethod'] == 'latest':
			Download_Latest(transport, row['path'],localpath, alreadySaved, dataType)
		
		if row['pickupMethod'] == 'name':
			Download_All_SpecificName(transport, row['path'], localpath, alreadySaved, dataType, row['specifics'])

		transport.close()
	
	SaveLog(alreadySaved)
	print('All connections have been checked, and all files have been downloaded. The login information will now be disabled')

	return log