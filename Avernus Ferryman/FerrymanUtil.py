# Ventures Pinnacle Incorporated
# Pedro Mroninski
# 12/04/2019
##############################
#Packages
import glob
import os
import datetime
import csv
import json
import boto3
##############################
#Variables
fileLog = {'stdOut': [], 'FerrymanLogErrors':[]}

##############################
#Functions

#Update internal log
#Checks if file is already in log, if not, it adds that as a key.
#If it is, it updates with the new information
def filesLog(tempLog, Files):	
	if len(Files) > 0:
		for f in Files:
			if f in tempLog:
				tempLog[f].update(Files[f])
			else:
				tempLog.update({f:Files[f]})
	return tempLog


#Saves the fileLog dictionary unto the local machine, transfers it to S3 and then deletes it locally. 
#Input:
#	log -> Log dicionary, uses the fileLog dictionary as default
#Returns:
#	None
def SaveLog(log = fileLog):

	#Remove unused top keys from log dictionary
	#Makes the log file cleaner
	emptyValues = []
	for k in log.keys():
		if len(log[k]) <= 0:
			emptyValues.append(k)
	
	for emptyValue in emptyValues:
		log.pop(emptyValue, None)
    
	# Create an S3 client
	s3 = boto3.client('s3')
	
	#Naming convention information
	time_identifier = datetime.datetime.now().strftime("%Y%m%d%H%M")
	filename = 'Charon_log_{}.txt'.format(time_identifier)
	
	bucketName = 'dev-ventures-datalake-logs-s3'
	
	#Create json out of the dictionary. If that fails, save dictionary as string but add an extra line to let user know of what the error is.
	try:
		file_content = json.dumps(log, indent=4)
	except Exception as e:
		print("didn't save as json, reason was {}".format(e))
		file_content = str(log)
	
	#Save locally
	with open(filename, 'w+') as f:
		f.write(file_content)
	
	#Move from local to S3 log bucket.
	s3.upload_file(filename, bucketName, 'Ferryman/{1}'.format(filename))
	os.remove(filename)


#Updates log dictionary with the newValue
#If newValue is a string, it is saved in the 'stdOut' key in the dictionary.
#If newValue is a dictionary, it updates the current log dictionary to add any new values in the dictionary into the current log dict. 
#If it has any issues adding the values into the dict, it will save a string saying it had an issue
#Input:
#	newValue -> Value that needs to be added into log [Can be any type]
#	Log -> log that is having this infomation added to it, uses fileLog as default
#Returns:
#	None
def LogUpdate(newValue, Log = fileLog):
	
	specialTypes = [dict, list]
	
	if type(newValue) not in specialTypes:
		try:
			stringValue = str(newValue)
			Log['stdOut'].append(stringValue)
		except:
			errorMessage = "PRINT ERROR: Could not turn the object type {} into a string".format(type(newValue))
			print(errorMessage)
			Log['FerrymanLogErrors'].append(errorMessage)
	
	else:
		if type(newValue) == dict:
			Log.update(newValue)
			return

#Read through the first N lines and cound the separator
#Find the highest number of separator, and which line it first occurs [The header]
#Input:
#	Name of file that needs to be checked as String
#Returns:
#	The file's delimiter as String
# 	How many lines to skip as Int 
def CsvConfig(file):
	N = 200
	
	with open(file) as csvfile:
		sample = csvfile.readlines()[:N]
		dialect = csv.Sniffer().sniff(''.join(sample))
		head_count = [x.count(dialect.delimiter) for x in sample]

	first_instance = head_count.index(max(head_count))
	
	return dialect.delimiter, first_instance


#Find all the files in the TempFiles directory with the extensions
#Input:
#	List of extensions as List of Strings
#Returns:
#	List of file names as List of String
def FilesByExtention(extensionList):
	allFiles = []
	for extension in extensionList:
		listOfFiles = glob.glob('tempFiles/*.{0}'.format(extension))
		allFiles += [file for file in listOfFiles]
	return allFiles

#Removes the extension from a filename 
#Input:
#	Filename with extension as String
#	count -> which position to return, default is -2 (second last)
#Returns:
#	Filename without extension as String
def RemoveExtension(filename, count = -2):
	if '.' in filename:
		filename = filename.split('.')[count]
	return filename
	
	
#Removes the path from a filename leaving only the file
#Input:
#	Filename with full path as String
#Returns:
#	Filename without path as String
def RemovePath(filename):
	if '/' in filename:
		filename = filename.split('/')[-1]
	if '\\' in filename:
		filename = filename.split('\\')[-1]
		
	return filename
	

#Finds all the objects in the S3 bucket.
#Looks through all the contents, and recurses through each folder until it reaches the bottom files.
#Moves up gathering all the unique filenames.
#Returns a list with all the filenames found in the bucket.
#Input:
#	bucket_name -> Name of bucket that will have a list of all files as String.
#	prefix -> Folder name within bucket, starts with default of '' as it starts from the top of the bucket. Value should be String.
#	delimiter -> value that separates files in directory, default is '/', Vakye should be String.
#	start_after -> value trims the directories which are checked by giving it a start point. Default is '' but value should be String.
#	file_list -> list of values already found, used to improve recursion. 
#
#Returns:
#	file_list -> List of filenames. The filenames will be as strings. 

def BucketList(bucket_name, prefix='', delimiter='/', start_after='', file_list = []):
	
	#Start connection
	s3_conn   = boto3.client('s3')  # type: BaseClient  ## again assumes boto.cfg setup, assume AWS S3
	s3_result =  s3_conn.list_objects_v2(Bucket=bucket_name, Prefix=prefix, Delimiter = "/")
	
	#If no files in bucket, return empty list
	if s3_result['KeyCount'] == 0:
		return file_list
	
	#For each folder found in top of bucket, run this function on it on it.
	if 'CommonPrefixes' in list(s3_result):
		for key in s3_result['CommonPrefixes']:
			#Get a list of all files within folder.
			prefixFileList = BucketList(bucket_name, prefix=key['Prefix'], delimiter='/', start_after='')
			
			#Only append files not already in file_list variable
			for prefixFile in prefixFileList:
				if prefixFile not in file_list:
					file_list.append(prefixFile)

	
	#Check if files [not folders] are inside of the folder.
	if 'Contents' in list(s3_result):
		
		#Append all filenames to list of files.
		for x in s3_result['Contents']:
			file_list.append(x['Key'])		
		
		#If folder has >1000 files, it will be truncated. this requests the other files in that list and adds them to the file_list. 
		if s3_result['IsTruncated'] == True:
			while s3_result['IsTruncated']:
				continuation_key = s3_result['NextContinuationToken']
				s3_result = s3_conn.list_objects_v2(Bucket=bucket_name, Prefix=prefix, Delimiter="/", ContinuationToken=continuation_key)
				for key in s3_result['Contents']:
					file_list.append(key['Key'])
	
	return file_list	