# Ventures Pinnacle Incorporated
# Pedro Mroninski
# 29/03/2019
##############################################
#Packages
import pandas as pd
import numpy as np
import glob
import csv
import os
import sys
import traceback


from random import randint
from tqdm import tqdm

import FerrymanUtil as fu

##############################################
#Variables

#Log Info
#Initiates tracking dictionary
filesDict = {}
##############################################

#Class
#Saves any content that would've been printed into a console in a file
#This is to track any errors that occur when opening the deliminated files on Pandas
#
# input:
# 		object -> selects where to output those values 
# For example:
# with RedirectStdStreams(stdout = 'filename.txt'): // would be an initiator
#
class RedirectStdStreams(object):
    def __init__(self, stdout=None, stderr=None):
        self._stdout = stdout or sys.stdout
        self._stderr = stderr or sys.stderr

    def __enter__(self):
        self.old_stdout, self.old_stderr = sys.stdout, sys.stderr
        self.old_stdout.flush(); self.old_stderr.flush()
        sys.stdout, sys.stderr = self._stdout, self._stderr

    def __exit__(self, exc_type, exc_value, traceback):
        self._stdout.flush(); self._stderr.flush()
        sys.stdout = self.old_stdout
        sys.stderr = self.old_stderr





#Function
#This function finds all the csv files within the temp folder
#It then iterates through the list of names and removes the timestamp from the file name
#Opens the file and adds a column to the end with the extracted timestamp
#Resaves the file with the same name and separator
#Input:
#	None
#Returns:
#	log of all the processes that occured, saved as Dict
def Main():
	##############################################
	#Variables
	
	#Useable files
	allowedExtensions = ['csv','txt','psv']

	#Empty list that will hold all the files found to have the selected extensions
	allFiles = []

	##############################################
	#Program
	
	#find all the files in the TempFiles directory with the 'psv' extension		
	allFiles = fu.FilesByExtention(allowedExtensions)
	
	#Iterate through list of files
	#Transform files into Pandas DataFrames
	#Create new data frame from random values from original file
	#Save new file as csv
	for file in tqdm(allFiles):
		try:
			
			fileDictKey = fu.RemoveExtension(fu.RemovePath(file))
			
			#CSV Errors tempfile
			csvError = 'tempSettings/{}_CsvError.txt'.format(fileDictKey)
			
			fileInfo = {fileDictKey:{}}
			fileTimestamp = fu.RemoveExtension(file).split('_')[-1]
			
			tqdm.write('Checking whether file {} needs to be updated'.format(file))
			
			#Get configuration details
			csvSettings = fu.CsvConfig(file)
			
			#Add those configs to log.
			fileInfo[fileDictKey].update({'csvSettings': {'Delimiter':csvSettings[0], 'Skiplines':csvSettings[1]}})
			
			#Open temporary file to save any opening file errors into
			file_comments = open(csvError, 'w+')
			
			#Open file and save as Dataframe
			with RedirectStdStreams(stdout=file_comments, stderr=file_comments):
				df = pd.read_csv(file, sep = csvSettings[0], skiprows = csvSettings[1], error_bad_lines=False, warn_bad_lines=True, low_memory = False)
			
			#Save any opening details into log 
			file_comments = open(csvError, 'rb')
			csvReadErrors = file_comments.read()
			
			if len(csvReadErrors) > 3:
				fileInfo[fileDictKey].update({'ReadComment':str(file_comments.read())})
			
			file_comments.close()
			
			#Remove temporary file with csv reading errors
			os.remove(csvError)			
			
			#Get file columns
			fileColumns = [x.lower() for x in list(df)]
			
			
			#Check wether the file already has the timestamp column, if not, add it
			if 'file_timestamp' in fileColumns:
				tqdm.write('\tFile {} already has values, moving to next file'.format(file))
			else:
				tqdm.write('\tFile {} needs values, updating now'.format(file))
				df['File_Timestamp'] = fileTimestamp
				tqdm.write('File {} added timestamp, moving to next file'.format(file))
			
			# convert all empty values to empty strings
			df = df.fillna('')		
			
			# convert all DataFrame columns to a string
			df = df.astype(str)
			
			#Clean the DF Columns Headers
			
			#lower all the characters to minimise the creation of new columns
			df.rename(columns=lambda x: x.lower().replace(' ','_'), inplace=True)
			
			
			#Characters which are not accepted in a header by Spark
			unsafeCharacters = [' ',',',';','{','}','(',')','\n','\t','=']
			
			#Remove those characters from the column headers
			for unsafeChar in unsafeCharacters:
				df.rename(columns=lambda x: x.replace(unsafeChar,'_'), inplace=True)

			#Save file as parquet with snappy compression to ease the reading on AWS
			df.to_parquet(r'{}.parquet'.format(fu.RemoveExtension(file)), compression = 'snappy')
			
			#Remove non parquet file 
			os.remove(file)
			
			#Add processed information to the log
			filesDict.update(fileInfo)
			
		except Exception as e:
			#Show what errors occured if file update fails at any point, and save those values in the logs
			tqdm.write('Could not update file --{}-- please do so manually'.format(file))
			tqdm.write(str(e))
			
			errorReason = '{0}, Details reason is: {1}'.format(e, traceback.format_exc())
			
			fileInfo[fileDictKey].update({'UpdateError':errorReason})
			filesDict.update(fileInfo)
			print('Could not update file --{}-- please do so manually'.format(file))
			
	
	return filesDict