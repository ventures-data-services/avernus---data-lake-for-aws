# Ventures Pinnacle Incorporated
# Pedro Mroninski
# 29/03/2019
##############################################
#Packages
import DownloadFiles as df
import ExtractFiles as ef
import UpdateData as ud
import UploadToS3 as uts
import FerrymanUtil as fu

import boto3
import os
import time
import datetime
import traceback
##############################################

##############################################

#Functions
#Calls the Download and check if any new files were crated. If the are any new files on tempFiles, it will Extract, Modify, Update, and Upload the files in it in that order. 
#Input:
#	numOfHours -> Int with the number of hours the program should wait between checks.
#Returns:
#	None
def main():
	
	#Initiates list for logs
	attemptedFunctions = []
	completedFunctions = []
	
	
	#Initial values for Log dictionary
	charonStatus = {'Status':''}
	
	#Adds start time to log
	timeRunStart = datetime.datetime.now()
	timeLog = {'Run': {'Start': timeRunStart.strftime('%Y-%m-%d %H:%M:%S')}}


	try:
		internalLogs = {'ProcessesRan' : {}, 'Files': {} }

		#Run DownloadFiles
		attemptedFunctions.append('DownloadFiles')
		internalLogs['ProcessesRan'].update(df.Main())
		completedFunctions.append('DownloadFiles')
				
		#Get number of files in tempFolder dir
		filesInDir = fu.FilesByExtention('*')
		internalLogs['ProcessesRan'].update({'CountFilesInTemp':len(filesInDir)})
			
		#Run the next functions if any files are found
		if len(filesInDir) > 0:
		
			print('\n----New files have been found in tempFile folder----')
			print('\n --Extracting all files that were compressed-- ')
			#Run ExtractFiles
			attemptedFunctions.append('ExtractFiles')
			extractedFiles = ef.Main()
			internalLogs['Files'] = fu.filesLog(internalLogs['Files'], extractedFiles)
			completedFunctions.append('ExtractFiles')
			
			#Run UpdateData
			print('\n --Adding column with files timestamp-- ')
			attemptedFunctions.append('UpdateData')
			UpdatedFiles = ud.Main()
			internalLogs['Files'] = fu.filesLog(internalLogs['Files'], UpdatedFiles)
			completedFunctions.append('UpdateData')
			
			#Run UploadtoS3
			print('\n --Files are ready to be uploaded-- ')
			attemptedFunctions.append('UploadToS3')
		
			UploadedFiles = uts.Main(internalLogs)
			internalLogs['Files'] = fu.filesLog(internalLogs['Files'], UploadedFiles)		
			completedFunctions.append('UploadToS3')
			
		else:
			print('No new files in the tempFiles folder, program has stopped running')
		
		filesList = []
		
		for k in internalLogs['Files'].keys():
			fi = {'Filename':k}
			z = {**fi, **internalLogs['Files'][k]}
			filesList.append(z)
			
		internalLogs['Files'] = filesList
		fu.LogUpdate(internalLogs)
			
		
		#Saves all sucessful processes
		internalLogs['ProcessesRan'].update({'Successful': completedFunctions})
		
		#Completed all functions
		charonStatus['Status'] = 'COMPLETED'
		
	except Exception as e:
		#If any functions fail, this will be triggered.
		#Wil save the failed function with cause of error.
		fu.LogUpdate(internalLogs)
		errorReason = '{0}, Details reason is: {1}'.format(e, traceback.format_exc())
		
		failedFunctions = list(set(attemptedFunctions) - set(completedFunctions))
		internalLogs['ProcessesRan'].update({'Failed':{'Functions': failedFunctions, 'FailReason': errorReason}})
		
		print("Error Occured, exception called was:\n{}\n".format(e))
		charonStatus['Status'] = 'FAILED'
	
	#Adds end time to log and calculates process time
	timeRunEnd = datetime.datetime.now()
	timeLog['Run'].update({'End': timeRunEnd.strftime('%Y-%m-%d %H:%M:%S'), 'TotalTime': (timeRunEnd - timeRunStart).total_seconds()})
	fu.LogUpdate(timeLog)
	
	#Update Function List
	fu.LogUpdate(internalLogs)
	
	#Update Final Status
	fu.LogUpdate(charonStatus)	
	
	fu.SaveLog()

while True:
	main()