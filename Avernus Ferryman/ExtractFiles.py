# Ventures Pinnacle Incorporated
# Pedro Mroninski
# 12/04/2019
##############################################
#Packages
import gzip
import shutil
import glob
import os
from pyunpack import Archive


import FerrymanUtil as fu
##############################################
#Variables

#Log Info
#Initiates tracking dictionary
FilesDicts = {}
##############################################	
#Function Opens Compressed files in the temp directory and removes the files inside
#Saves with the same name as compressed file, but as a text file
#Deletes compressed file
#
#Input:
#	None
#Returns:
#	log -> File dictionary that was initiated before the function. Returns as Dictionary.
##############################################
def Main():
	#Variables

	#Compressed Types to be checked
	allowedExtensions = ['gz','rar']
	
	#Empty list that will hold all the files found to have the selected extensions
	allFiles = []
	
	##############################################
	
	#Program
	
	#Find all the files in the TempFiles directory with the mentioned extensions above and add them into list
	allFiles = fu.FilesByExtention(allowedExtensions)
	
	if len(allFiles) == 0:
		print('No files to be extracted')
		return {}
	
	
	#Iterate through list of compressed files
	#Remove files from the compressed folder
	#Save the files from inside the compressed file with the name of the compressed file but with '.txt' extension
	#Delete compressed file
	for file in allFiles:
		
		newFilename = '{0}.txt'.format(fu.RemovePath(fu.RemoveExtension(file, 0)))
		
		fileDictKey = fu.RemoveExtension(newFilename)
		
		#Log for individual file process
		fileInfo = { fileDictKey : {'WasExtracted': {'OriginalName' : fu.RemovePath(file)}}}
		
		
		print('Extracting {}'.format(file))
		
		try:
			
			if file.split('.')[-1] == 'gz':
				with gzip.open(file, 'rb') as f_in:
					with open('tempFiles/{0}'.format(newFilename), 'wb') as f_out:
						shutil.copyfileobj(f_in, f_out)
			
			if file.split('.')[-1] == 'rar':
				Archive(file).extractall('tempFiles/')
			
			os.remove(file)
			
			FilesDicts.update(fileInfo)
			
			
		except Exception as e:
			fileInfo[fileDictKey]['WasExtracted'].update({'ExtractFailed':str(e)})
			FilesDicts.update(fileInfo)
			print('Could not extract files from --{}-- please do so manually'.format(file))
	
	return FilesDicts
		