# Avernus Ferryman
##### Ventures | Pedro Mroninski | 17/04/2019
##### Last Updated 24/06/2019 
# 

#### Description
Automatically downloads files through an SFTP connection, making adjustments to the file before uploading it to AWS S3 with the appropriate object tags. Written for the Python 3.0+ runtime.

#### Pre-defined Values
Prior to running the Ferryman, the user has to set up the connections, and which data formats are expected to arrive. New data types will not be correctly processed if the set up is not done before receiving said data - this program will work as expected only if the user knows and sets up what they will be receiving _before running the program._

The set up requires two CSVs. They are:

- "Sources.csv" contains the Remote Connection login details
- "Prefix_Settings.csv" contains the details for the naming conventions for the different data types, so that the Ferryman can tell which data type it is, and give it appropriate tags.

The set up also needs to be done within the code:

- `DownloadFiles.py` needs the __bucket name__ it will be comparing against to update the logs.
- `UploadToS3.py` needs the __filetypes (csv, txt, parquet)__ which are accepted to be uploaded and the __bucket name__ which you will be uploading the files to.
- `FerrymanUtil.py` needs the bucket which you will be uploading the __logs__ to. 



The file is formatted as so:

| Filename | Column Names |
| ------ | ------ |
| Sources.csv | data, host, username, password, path, pickupMethod, specifics |
| Prefix_Settings.csv | Prefix, DataSource, Purpose, Folder |

The meaning of those columns are explained below:

**_Sources_**

| Column Name | Meaning |
| ------ | ------ |
|data|Name of the connection|
|host|IP Address of the host|
|username|Username for the connection|
|password|Password for the connection|
|path|Path in the remote connection that will have the files to be downloaded|
|pickupMethod | Type of download - Currently the options are ALL [Downloads all files in the path] or NAME [Downloads all files that have the same string as the one passed in the function ] |
|specifics | If the specific method was chosen for the pickupMethod, this column will have what string it should be looking for - For example, if you only wanted files with the word 'financial' in it, you'd put 'NAME' (no quotes) in the pickupMethod and 'financial' (no quotes) in the specific. |

**_Prefix_Settings_**

| Column Name | Meaning |
| ------ | ------ |
|Prefix|Naming Conventions|
|DataSource|Origin of the Data - In this case it is MoH|
|Purpose|Purpose of data as per documentation in the Avernus Datalake|
|Folder|Path in AWS S3 where the file is supposed to be updated to. If you are using the Avernus Ferryman, this should be 'Landing'. Please note - it will add the slash '/' to the end of the name in the program, so if you wanted to add it to a folder within a folder, it would look like "folder/subfolder"|

#

#### FerryMan Process
The ferryman is broken into separate processes, one running after the other has completed. The processes go as follows:

1. [DownloadFiles](DownloadFiles.py)

2. [ExtractFiles](ExtractFiles.py)

3. [UpdateData](UpdateData.py)

4. [UploadToS3](UploadToS3.py)

   

They all use another file, which has some functions that repeat throughout the FerryMan, it is:

- [FerrymanUtil](FerrymanUtil.py)

##### More details on what each of those files does is below.

#

#### DownloadingFiles
The downloading/comparing files is done by the `DownloadFiles.py main()` function. The function opens the `Sources.csv` file and iterates through all the data sources. It uses the __log__ to compare the files, and before downloading it always adds the file's __timestamp__ into the filename to better identify and compare it in the future. Currently, the function downloads one file at a time and leaves them in the tempFiles folder until it has checked all the data sources. It will download only if it cannot find the file within its log.

- ###### Log
This is the files log, the one that the DownloadFiles.py uses to compare which files it should donwload. The log is updated before, during and at the end of the `DownloadFiles.py main()` function. In the beginning, it also adds all the files currently found on the S3 bucket, and as it downloads files it updates based on what is located in the tempFiles folder. The log is not meant to be deleted, but in case it ever was, it will be re-created with everything in the S3 Bucket once the function runs again.


- ###### Timestamp
The file last modified timestamp is used as the file's unique identifier, as files with the same name can be reuploaded and this is the only way to differentiate them. Once the file is downloaded, it will update the file name to have the file timestamp appended to the end in the format  _"filename_timestamp.ext"_ . This isso that it can easily be removed and compared/used.

#

#### ExtractFiles
As sometimes there will be compressed files, it is necessary to extract the file from within and add the correct naming convention prior to the upload, so that the file's timestamp is correctly added to the values within. To do this, we use the `ExtractFiles.py main()` function. This function opens, extracts the file from the inside and adds it back into the tempFiles folder with the same name as the compressed file, but as a `.txt`

Currenlty it only works with gz and rar file types. Any others will be processed through and sent to the S3 bucket as-is.
#

#### UpdateData
The Ferryman always assumes the files will be a type of delimited text files, and the data will be in a tabular format. Once all the files are downloaded, it opens `csv,psv,txt` files and does the following process:
- Find the iterator by reading the first 200 lines.
- Find how many rows need to be skipped before the actual data type starts
- Opens the file with the Pandas package and transforms the data inside of it into a dataframe
- Extracts the timestamp from the filename and adds that as a default column to all rows of the file
- Lowers all column names to minimise issues with previous data
- Cleans the column names from any special characters and replaces those and spaces with '_' 
- Saves the data as a parquet file

#

#### UploadToS3
Once all files have been downloaded, decompressed and updated, they are ready to be uploaded. It compares each file's naming convention with the "Prefix" column in `Prefix_Settings.csv' and if they are the same, it uses the information in the other columns to create **tags** for the file. Once the tags have been set, it sends the file to S3 and runs a **command** to have it run by AWS. 

- ##### Tags
  There are default and file-specific tag. The default are applied to all the files processed through the ferryman and need to be defined within the UploadToS3 code. The tags that are added to all files that are uploaded are:

  | TagName     | Value Definition                                             | Example   |
  | ----------- | ------------------------------------------------------------ | --------- |
  | Project     | Default Value - Should be the project name                   | Avernus   |
  | Team        | Default Value - Should be the team's name                    | Venture   |
  | Role        | Default Value - Should be the ferryman processed files'      | Data File |
  | Purpose     | Purpose of data as per documentation in the Avernus Datalake | Financial |
  | Environment | Default Value - Should be the stage the program is currently in | Dev       |
  | DataSouce   | Origin of the Data, anything that would help identify what the main source is | MoH       |


To change how the Ferryman will fill the tags, it is necessary to change the `Prefix_Settings.csv` file and the UploadToS3 code.

#### FerryMan Logs
Throughout the code, the Ferryman will be creating a log file with the processes that have occurred and Failed or were Successful. This is to easy follow the programs process and be able to diagnose where any issues occurred down the pipeline. It will also be easily quarriable in a document database, if stored in one later on.

The current Log file follows a 'Object Centred' method [where it shows which what occurred to each file and treats them as separate objects] as per below:
```json
{
    "ProcessesRan": {
        "CountFilesInTemp": 1,
        "Successful": [
            "ExtractFiles",
            "UpdateData"
        ]
    },
    "Files": [
        {
            "Filename": "",
            "csvSettings": {
                "Delimiter": "|",
                "Skiplines": 1
            }
		}
    ],
    "Run": {
        "Start": "2019-06-11 16:00:17",
        "End": "2019-06-11 16:00:27",
        "TotalTime": 10.0
    },
    "Status": "COMPLETED"
}
```

### Use
The Ferryman program is meant to automate the pickup/upload process of data files we receive. The Ferryman then triggers AWS to start all ETL processes related to the data that has been uploaded.
