# Ventures Pinnacle Incorporated
# Dylan Byrne
# 30/07/2019
import os
import ast
import boto3
import bucketLister

# Lambda event handler, calls the start point of the function
def lambda_handler(event, context):
    # Set up variables
    bucketName = os.environ['sourceBucket']
    landingPrefix = "Landing"
    # Data Variables
    dataPrefix = ["PHO_Enrolment", "PHOS_PERSON_EXTRACT_PERIOD","PHO_", "NMDS", "NNPAC"]
    dataDictionary = {
        "Unknown" : {"Uploaded" : False, "Path" : "Unknown/", "Data Source" : "Unknown", "Purpose" : "Unknown"},
        "PHO_Enrolment" : {"Uploaded" : False, "Path" : "PHO_Enrolment/", "Data Source" : "MoH", "Purpose" : "PHO Enrolment"},
        "PHO_Enrolment_Weekly" : {"Uploaded" : False, "Path" : "PHO_Enrolment_Weekly/", "Data Source" : "MoH", "Purpose" : "PHO Enrolment"},
        "PHO_Enrolment_Monthly" : {"Uploaded" : False, "Path" : "PHO_Enrolment_Monthly/", "Data Source" : "MoH", "Purpose" : "PHO Enrolment"},
        "PHO_Enrolment_Archive" : {"Uploaded" : False, "Path" : "PHO_Enrolment_Archive/", "Data Source" : "MoH", "Purpose" : "PHO Enrolment"},
        "PHO_" : {"Uploaded" : False, "Path" : "PHO_Collection/", "Data Source" : "MoH", "Purpose" : "PHO Collection"},
        "NNPAC" : {"Uploaded" : False, "Path" : "NNPAC/", "Data Source" : "MoH", "Purpose" : "NNPAC"},
        "NMDS" : {"Uploaded" : False, "Path" : "NMDS/", "Data Source" : "MoH", "Purpose" : "NMDS"},
        "PHOS_PERSON_EXTRACT_PERIOD" : {"Uploaded" : False, "Path" : "PHO_Financial_Extract/", "Data Source" : "MoH", "Purpose" : "PHO Financial Extract"}
    } 
    dataTypes = []

    # Sort and get data types processed
    if check_key(event, "judgeOut"):
        dataTypes = updateDataTypes(event["judgeOut"]["GlueArgs"]["--DATATYPES"], judge_landing_objects(bucketName, landingPrefix, dataPrefix, dataDictionary))
    else:
        dataTypes = judge_landing_objects(bucketName, landingPrefix, dataPrefix, dataDictionary)

    # configure output
    return { "GlueArgs" : {"--DATATYPES" : str(dataTypes)}, "TypesEmpty" : False}

# checks to see if inputted key is contain in dict
# dict = the dictionary to check through
# key = the key to check for in the dictionary
# returns True if the key is contained in the dictionary, or
# returns False if the key is not contained in the dictionary
def check_key(dict, key):  
    if key in dict.keys():
        return True 
    else: 
        return False

# adds data types to a list if they are not already contained in the list
# in_oldDataTypes = the old list of data types from previous runs
# in_newDataTypes = the new list data types from the current run
# returns a list of all unique datatypes from both lists
def updateDataTypes(in_oldDataTypes, in_newDataTypes):
    oldDataTypesCon = ast.literal_eval(in_oldDataTypes)
    for dataType in in_newDataTypes:
        if dataType not in in_oldDataTypes:
            oldDataTypesCon.append(dataType)
    return oldDataTypesCon

# Gets the filename by removing the first folder extension
# in_fileName = the file name to be trimmed
# returns in_fileName without folder prefix, assumes one folder prefix
def get_file_name(in_fileName):
    in_fileName = in_fileName.split('/')
    in_fileName = in_fileName[1]
    return in_fileName

# returns the keyword for the data type, keyword at this stage can only be in dataPrefix or Unknown
# in_fileName = the filename to be checked
# in_dataPrefix = a list of data prefixes to check
# returns prefix = the dataPrefix that matches OR "Unknown"
def get_data_type(in_fileName, in_dataPrefix):
    for prefix in in_dataPrefix:
        if prefix in in_fileName:
            return prefix
    return "Unknown"

# returns a new keyword for a PHO_Enrolment file based on if it's a snapshot or monthly report
# in_fileName = the PHO_Enrolment file name to be checked
# returns 'M' if a monthly report OR 'S' report if a snapshot OR 'A' if an archive 
# OR "Unknown" if it doesn't follow current convention
def check_report_pho_enrol(in_fileName):
    # get the report code
    fileCode = in_fileName.split('_')
    fileCode = fileCode[-4]

    if fileCode == 'M':
        return "PHO_Enrolment_Monthly"
    elif fileCode == 'S':
        return "PHO_Enrolment_Weekly"
    elif fileCode == 'A':
        return "PHO_Enrolment_Archive"
    else:
        return "Unknown"

# Sends an SNS to a topic of all unknown objects found
# in_unknownObjects = the list of all unknownObjects found
# in_bucketName = the name of the bucket in include in the message
def send_unknown_sns(in_unknownObjects, in_bucketName):
    sns = boto3.client('sns')
    snsARN = os.environ['snsTopicARN']
    notification = "Unkown objects found in Landing folder of the " + in_bucketName + " bucket. File[s] are {}".format(",".join(in_unknownObjects))

    sns.publish(
        TopicArn=snsARN,
        Message=notification,
    )

# Main function, sort all objects in the Landing folder of a S3 bucket, defined objects are sorted into appropriate folders,
# all other objects are sorted into a folder called "Unknown", an SNS is sent when this occurs
# in_bucketName = the name of the bucket to judge
# in_landingPrefix = the prefix of the landing folder on the bucket
# in_dataPrefix = the list of viable data prefixes
# in_dataDictionary = the dict with the info about each data type
# returns all data types in a list discovered in the landing prefix
def judge_landing_objects(in_bucketName, in_landingPrefix, in_dataPrefix, in_dataDictionary):
    # Set up variables
    s3Res = boto3.resource('s3')
    unknownObjects = []
    landingFolder = in_landingPrefix + '/'
    landingPath = in_bucketName + '/' + landingFolder
    
    # for each obj in the landing folder
    for obj in bucketLister.get_matching_s3_keys(in_bucketName, in_landingPrefix):
        if obj == "Landing/":
            continue

        obj = get_file_name(obj)
        # obj will now be the file name
        objDataType = get_data_type(obj, in_dataPrefix)

        # check PHO_Enrolment report type
        if objDataType == "PHO_Enrolment":
            objDataType = check_report_pho_enrol(obj)

        # copy/delete object, sometimes throws a key not found error
        try:
            s3Res.Object(in_bucketName, in_dataDictionary[objDataType]["Path"] + obj).copy_from(CopySource= landingPath + obj)
            s3Res.Object(in_bucketName, landingFolder + obj).delete()
        except Exception as e:
            print("AVERNUS COMMENT: " + landingPath + obj)
            print("AVERNUS ERROR " + e)

        # indicate which data types have been uploaded
        if in_dataDictionary[objDataType]["Uploaded"] == False:
            in_dataDictionary[objDataType]["Uploaded"] = True
        
        # if object is unknown add to list of those objects
        if objDataType == "Unknown" and obj != "":
            unknownObjects.append(obj)
            print("AVERNUS COMMENT: " + obj)

    # if unknown objects were found send a notification
    if in_dataDictionary["Unknown"]["Uploaded"] == True:
        send_unknown_sns(unknownObjects, in_bucketName)

    # set uploadTokens to be a list of all item types (by dataDictionary prefixes) that have been uploaded
    uploadedTokens = []
    for dataType in in_dataDictionary.items():
        if dataType[1]["Uploaded"] == True:
            # data type is path without /
            uploadedTokens.append(dataType[1]['Path'][:-1])
    
    return uploadedTokens