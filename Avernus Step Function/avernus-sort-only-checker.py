# Ventures Pinnacle Incorporated
# Dylan Byrne
# 16/05/2019

def lambda_handler(event, context):
    if check_key(event, "sortOnly"):
        return event["sortOnly"]
    else:
        return False

# checks to see if inputted key is contain in dict
# dict = the dictionary to check through
# key = the key to check for in the dictionary
# returns True if the key is contained in the dictionary, or
# returns False if the key is not contained in the dictionary
def check_key(dict, key):  
    if key in dict.keys():
        return True 
    else: 
        return False