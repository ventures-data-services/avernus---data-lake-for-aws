# Ventures Pinnacle Incorporated
# Dylan Byrne
# 09/05/2019

# checks if the landing judge is moving files based to the last judged
# file vs last file seen by the landing checker
def lambda_handler(event, context):
    if event['checkerOut']['lastFile'] == event['checkerOut']['lastJudged']:
        return "Fail"
    else:
        return event['checkerOut']['lastFile']