# Ventures Pinnacle Incorporated
# Dylan Byrne
# 15/05/2019
import boto3
import json
import os
import datetime

# saves a log and sends a sns with the content of the event
def lambda_handler(event, context):
    bucketName = os.environ['sourceBucket']
    snsARN = os.environ['snsTopicARN']

    content = format_dict_to_json(event)

    SaveLog(content, bucketName)
    send_sns(content, snsARN)

# changes a dictionary into json format or just a string if unable 
# in_dictionary = the dictionary to be formatted
# returns content = the formatted string from the dictionary
def format_dict_to_json(in_dictionary):
    try:
        content = json.dumps(in_dictionary, indent=4, sort_keys=False)
    except:
        content = str(in_dictionary)

    return content

# Saves a log file to a S3 bucket
# in_content = the content of the log file that will be made
# in_bucketName = the name of the bucket the log will be saved to
def SaveLog(in_content, in_bucketName):
    # Naming convention information
    time_identifier = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    tempFileName = '/tmp/avernus_step_log_temp.txt'
    fileName = 'avernus_step_log_{}.txt'.format(time_identifier)
    folderName = 'Avernus_State_Machine'
    
    with open(tempFileName, 'w+') as f:
        f.write(in_content)
        
    # Create an S3 client
    s3 = boto3.client('s3')
    # Add file to bucket
    s3.upload_file(tempFileName, in_bucketName, '{0}/{1}'.format(folderName,fileName))

# Sends an sns message to a topic ARN
# in_content = the content of the message to send
# in_snsARN = the ARN of the sns topic to send the message to
def send_sns(in_content, in_snsARN):
    sns = boto3.client('sns')
    sns.publish(TopicArn=in_snsARN, Message=in_content,)