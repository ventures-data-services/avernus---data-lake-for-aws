# Ventures Pinnacle Incorporated
# Dylan Byrne
# 23/07/2019
import os
import json
import boto3
import bucketLister
from datetime import datetime, timedelta

def lambda_handler(event, context):
    # Set up variables
    sourceBucket = get_log_prefix_pro(event)['Bucket']
    logBucket = os.environ["logBucket"]
    bmFilePrfx = "Bookmark"

    # Get and check the bookmark info
    bmFolderPrfx = get_log_prefix_pro(event)['Prefix']
    if bmFolderPrfx == "ERROR_UNKNOWN":
        print("[ERR] UNKNOWN CURRENT JOB, returning input as output")
        return event['judgeOut']
    bmFileKey = bmFolderPrfx + "/" + bmFilePrfx + ".txt"

    proDataFilePrefix = create_prefix_timestamped(bmFolderPrfx[:-8] + "ProDataFiles")
    proDataFolderPrefix = "ASM_Process_Data_Files"
    proDataFileKey = proDataFolderPrefix + "/" + proDataFilePrefix + ".txt"
    
    # 1. get all processed files, or create a blank file if none
    bmContents = get_json_log_contents(bmFileKey, logBucket)
    if bmContents == "GET_ERROR":
        # if here then create default bookmark file
        print("[LOG] Creating a default log file")
        save_log_pro(format_dict_to_json({"ProcessedFiles" : {}, "ProcessedTimes": {}, "FailedFiles" : {}}), logBucket, bmFilePrfx, bmFolderPrfx)
        bmContents = get_json_log_contents(bmFileKey, logBucket)

    # 2. get files in the bucket that are not processed based on if the files last modified time is newer than the last time
    #    the data was processed
    ignoreDT = ["unknown", "quarantine", "landing"] # default ignore types
    onlyDT = []
    if check_key(event, 'IgnoreDataTypes'):
        ignoreDT = ignoreDT + event['IgnoreDataTypes']
    if check_key(event, 'OnlyDataTypes'):
        onlyDT = event['OnlyDataTypes']

    result = get_nonpro_dict(sourceBucket, bmContents, ignoreDT, onlyDT)
    npFiles = result['npFiles']
    print(npFiles)

    # 3. save these files to txt and return the location of it
    save_log_pro(format_dict_to_json(npFiles), logBucket, proDataFilePrefix, proDataFolderPrefix)

    finalValue = {}
    for key in event['judgeOut']:
        if key != "GlueArgs" and key != "TypesEmpty" and key != "Workers":
            finalValue[key] = event['judgeOut'][key]

    finalValue["GlueArgs"] = {"--DATATYPES" : str(list(npFiles.keys())), "--PRODATAFILES" : str({"Bucket" : logBucket, "FileKey" : proDataFileKey})}

    if len(npFiles) == 0:
        finalValue["TypesEmpty"] = True
    else:
        finalValue["TypesEmpty"] = False
        finalValue["Workers"] = get_dpu(result['ftSizes'])

    return finalValue

# creates a formatted file prefix with a current timestamp
# in_prefix = the prefix to format
# returns the formatted prefix
def create_prefix_timestamped(in_prefix):
    currentTime = datetime.now().strftime("%Y-%m-%d-%H-%M")
    return in_prefix + '_' + currentTime

# Gets a python dictionary from a json file
# in_fileKey = the key of file to get the dict from
# in_bucketName = the name of bucket where the file is
# returns a dict of the file's contents
def get_json_log_contents(in_fileKey, in_bucketName):
    s3 = boto3.client('s3')
    try:
        response = s3.get_object(Bucket=in_bucketName, Key=in_fileKey)
        return json.loads(response['Body'].read().decode('utf-8'))
    except:
        return "GET_ERROR"

# Saves a log txt file
# in_content = string content of the log to save
# in_bucketName = the bucket to save the log to
# in_prefixFile = the prefix of the log file name
# in_prefixFolder = the prefix in the bucket to save to
def save_log_pro(in_content, in_bucketName, in_prefixFile, in_prefixFolder):
    # Naming convention information
    # lambda functions can only save to /tmp
    tempFileName = '/tmp/temp_log.json'
    fileName = in_prefixFile + ".txt"
    
    with open(tempFileName, 'w+') as f:
        f.write(in_content)
        
    s3 = boto3.client('s3')
    s3.upload_file(tempFileName, in_bucketName, '{0}/{1}'.format(in_prefixFolder,fileName))

# changes a dictionary into json format or just a string if unable 
# in_dictionary = the dictionary to be formatted
# returns content = the formatted string from the dictionary
def format_dict_to_json(in_dictionary):
    try:
        content = json.dumps(in_dictionary, indent=4, sort_keys=True)
    except:
        content = str(in_dictionary)

    return content

# checks to see if inputted key is contain in dict
# dict = the dictionary to check through
# key = the key to check for in the dictionary
# returns True if the key is contained in the dictionary, or
# returns False if the key is not contained in the dictionary
def check_key(dict, key):  
    if key in dict.keys():
        return True 
    else: 
        return False

# gets hardcoded info based on what the current ETL job is
# in_event = the lambda event handler that contains currentJob key
# returns dict {"Prefix : "Jobs_Bookmark_Prefix", "Bucket" : "name-of-source-bucket"}
def get_log_prefix_pro(in_event):
    if in_event['currentJob'] == os.environ["RawJob1"]:
        return {"Prefix" : "Raw_Bookmark", "Bucket" : os.environ['RawBucket']}
    elif in_event['currentJob'] == os.environ["TransformedJob1"]:
        return {"Prefix" : "Transformed_Bookmark", "Bucket" : os.environ['TransformedBucket']}
    else:
        return {"Prefix" : "ERROR_UNKNOWN", "Bucket" : "ERROR_UNKNOWN"}

# returns the data type of file key based on it's prefixes
# in_fileKey = the key to get the data type from
# returns the data type in lowercase
def get_file_dt(in_fileKey):
    fileParts = in_fileKey.split('/')
    dataType = fileParts[0]

    # special condition
    # CHANGE FOR PUBLIC RELEASE
    if dataType == "EightWire" or dataType == "eightwire" and fileParts[-1].split('.')[-1] == "csv":
        dataType = dataType + '/' + fileParts[-1].split('.')[0]
        if "_" in dataType:
            dataType = "eightwire/" + dataType.split("_")[-1]
    elif dataType == "EightWire" or dataType == "eightwire" and fileParts[-1].split('.')[-1] != "csv":
        dataType = dataType + '/' + fileParts[1]
        if "_" in dataType:
            dataType = "eightwire/" + dataType.split("_")[-1]

    return dataType.lower()

# convert the last modified timestamp into a comparable format
# in_fileObj = the obj dict returned from boto3
# returns datetime of last modified in correct format
def conv_file_time(in_fileObj):
    objTime = in_fileObj['LastModified'] + timedelta(hours=int(os.environ['timeDiff']))
    objTime = objTime.strftime("%Y-%m-%d-%H-%M")
    objTime = datetime.strptime(objTime, '%Y-%m-%d-%H-%M')
    return objTime

# create a lowercase version of a str list
# in_list = the list of strings to be converted to lower case
# returns a string list where each item is in lower case
def lower_list(in_list):
    newList = []
    for item in in_list:
        newList.append(item.lower())
    return newList

# get a dict of the last datetimes that each data type was processed
# in_bmProcessedTimes = the ProcessedTimes key of the bookmark file
# returns a dict {"dataType1" : datetime, "dataType2" : datetime}
def get_last_pro_dict(in_bmProcessedTimes):
    timeDict = {}
    for dType in in_bmProcessedTimes:
        timeDict[dType] = datetime.strptime(in_bmProcessedTimes[dType], '%Y-%m-%d-%H-%M')
    return timeDict

# get a dict of all files to process by data type
# in_bucketName = the bucket to find the data obj in
# in_bmContents = the contents of the bm file
# in_ignoreDT = a list of datatypes to ignore, default []
# in_onlyDT = a list of datatypes to keep, ignoring all others, default []
# returns dict {"dataType1" : ["file1.csv", "file2.csv"]}
def get_nonpro_dict(in_bucketName, in_bmContents, in_ignoreDT=[], in_onlyDT=[]):
    # set up variables
    nonProFiles = {}
    ftSizes = {}
    proTimes = get_last_pro_dict(in_bmContents['ProcessedTimes'])
    lowerIgnoreDT = lower_list(in_ignoreDT)
    lowerOnlyDT = lower_list(in_onlyDT)

    for obj in bucketLister.get_matching_s3_objects(in_bucketName):
        if obj['Size'] != 0:
            # get obj data type
            dt = get_file_dt(obj['Key'])
            # check if the data type is to be processed
            if dt not in lowerIgnoreDT and (dt in lowerOnlyDT or len(lowerOnlyDT) == 0):
                # get obj last mod time
                objTime = conv_file_time(obj)

                # get last processed time for the data type
                if check_key(proTimes, dt.lower()):
                    bmTime = proTimes[dt.lower()]
                else:
                    bmTime = datetime.strptime("1995-09-01-02-03", '%Y-%m-%d-%H-%M')
                    proTimes[dt.lower()] = bmTime

                # check if the obj should be processed
                if objTime > bmTime:
                    # add to the process list
                    if check_key(nonProFiles, dt) == False:
                        nonProFiles[dt] = []
                    nonProFiles[dt].append(obj['Key'])

                    # add size of obj for dpu calculation
                    ft = get_file_ft(obj['Key'])
                    if check_key(ftSizes, ft):
                        ftSizes[ft] = ftSizes[ft] + obj['Size']
                    else:
                        ftSizes[ft] = obj['Size']

    return {"npFiles" : nonProFiles, "ftSizes" : ftSizes}

# in_sizeKB = the total size of the files in KB
# in_KBperDPU = the number of KB you want per dpu
# returns the number of dpus
def get_dpu_kb(in_sizeKB, in_KBperDPU):
    result = in_sizeKB / in_KBperDPU
    return result

# get the number of DPUs to use for glue job workers
# in_fileTypeSizes = {"FileType1" : kb, "FileType2": kb}
# where file type is the suffix (eg csv)
# and kb is the size in kilobytes
# returns int number of DPUs
def get_dpu(in_fileTypeSizes):
    # Set up variables
    # KB rates per dpu per file type, defaults to default
    # TODO set up rates as input parameters
    rates = {"parquet" : 500000000, "csv" : 1000000000, "default" : 1000000000}
    minDPUs = 2
    # dpu = data processing unit
    dpus = 0

    # for each file type get the DPU rate
    for ft in in_fileTypeSizes:
        if check_key(in_fileTypeSizes, ft):
            dpus = dpus + get_dpu_kb(in_fileTypeSizes[ft], rates[ft])
        else:
            dpus = dpus + get_dpu_kb(in_fileTypeSizes[ft], rates['default'])

    # floor dpus
    if dpus < minDPUs:
        dpus = minDPUs

    # dpus will be double, return as int
    return int(dpus)

# gets the file type of a file key (eg csv)
def get_file_ft(in_fileKey):
    return in_fileKey.split('.')[-1].lower()

##########
# LEGACY #
##########
# Get a list of all non-processed files for a data type
# in_dType = the data type to check
# in_bucketName = the name of bucket to check (usually raw or transf)
# in_bmContents = the contents of the bookmark to check
# returns a list of all unprocessed file keys
# def get_np_list(in_dType, in_bucketName, in_bmContents):
#     npList = []
#     # if the data type doesn't exist in the log
#     if check_key(in_bmContents, in_dType.lower()) == False:
#         for obj in bucketLister.get_matching_s3_keys(in_bucketName, in_dType):
#             npList.append(obj)
#         return npList
#     else: 
#         for obj in bucketLister.get_matching_s3_keys(in_bucketName, in_dType):
#             if obj not in in_bmContents[in_dType.lower()]:
#                 npList.append(obj)
#         return npList

# 2. get files in the bucket that are not processed based on what datatypes to process
# npFiles = {}
# datatypes = ast.literal_eval(event['judgeOut']['GlueArgs']['--DATATYPES'])
# print("Data Type: " + str(datatypes))
# for dType in datatypes:
#     npFiles[dType.lower()] = get_np_list(dType, sourceBucket, bmContents['ProcessedFiles'])