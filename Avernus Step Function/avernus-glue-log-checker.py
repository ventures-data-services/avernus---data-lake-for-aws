# Ventures Pinnacle Incorporated
# Dylan Byrne
# 30/07/2019
import os
import json
import boto3
import bucketLister
from datetime import datetime, timedelta

def lambda_handler(event, context):
    # set up variables
    bucketName = os.environ['logBucket']
    snsARN = os.environ['snsARN']
    args = get_job_args_glog(event)

    prefix = args['Prefix']
    if prefix == "ERROR_UNKNOWN":
        send_sns(format_dict_to_json({"Message" : "No known log prefix for job", "JobName:" : event['currentJob']}), snsARN)
        return event['judgeOut']
    typesEmpty = False

    try:
        # get the contents of the glue log
        fileKey = get_last_modified_key(bucketName, prefix)
        fileContents = get_json_log_contents(fileKey, bucketName)

        glogFiles = get_glog_status(fileContents)

        # set up successfully processed data info
        goodData = list(glogFiles['SuccessFiles'])
        goodDataFiles = glogFiles['SuccessFiles']

        # set up failed processed data info
        failData = list(glogFiles['FailedFiles'])
        failDataFiles = glogFiles['FailedFiles']

        # check and config sns
        if len(goodData) == 0 and len(failData) > 0:
            typesEmpty = True
            send_sns(format_dict_to_json({"Message" : "NO NON-FAILED DATATYPES LEFT, and some data has failed to process, have been quarantined", 
                                            "FailedData:" : failDataFiles}), snsARN)
        elif len(goodData) == 0 and len(failData) == 0:
            typesEmpty = True
            send_sns(format_dict_to_json({"Message" : "NO NON-FAILED DATATYPES LEFT"}), snsARN)
        elif len(goodData) > 0 and len(failData) > 0:
            send_sns(format_dict_to_json({"Message" : "Some data has failed to process, have been quarantined", "FailedData:" : failDataFiles}), snsARN)

        # Update Bookmark file
        update_bookmark(goodDataFiles, failDataFiles, bucketName, args['Bookmark'])

        # quarantine failed files
        quarantine_files(failDataFiles, args['Bucket'])

        # config return value
        proDataFilesInfo = event['judgeOut']['GlueArgs']['--PRODATAFILES']
        if len(failData) > 0:
            if check_key(event['judgeOut'], "FAILDATA"):
                return {"GlueArgs" : {"--DATATYPES" : str(goodData), "--PRODATAFILES" : proDataFilesInfo}, "FAILDATA" : (event['judgeOut']['FAILDATA'].update(failDataFiles)), "TypesEmpty" : typesEmpty}
            else:
                return {"GlueArgs" : {"--DATATYPES" : str(goodData), "--PRODATAFILES" : proDataFilesInfo}, "FAILDATA" : failDataFiles, "TypesEmpty" : typesEmpty}
        else:
            if check_key(event['judgeOut'], "FAILDATA"):
                return {"GlueArgs" : {"--DATATYPES" : str(goodData), "--PRODATAFILES" : proDataFilesInfo}, "FAILDATA" : event['judgeOut']['FAILDATA'], "TypesEmpty" : typesEmpty}
            else:
                return {"GlueArgs" : {"--DATATYPES" : str(goodData), "--PRODATAFILES" : proDataFilesInfo}, "TypesEmpty" : typesEmpty}

    except Exception as e:
        # send an SNS if an error is encountered, return ERROR message
        send_sns({"Message" : "Error in Glue Log Checker", "Error" : e}, snsARN)
        return "ERROR"

# Gets a python dictionary from a json file
# in_fileKey = the key of file to get the dict from
# in_bucketName = the name of bucket where the file is
# returns a dict of the file's contents
def get_json_log_contents(in_fileKey, in_bucketName):
    s3 = boto3.client('s3')
    response = s3.get_object(Bucket=in_bucketName, Key=in_fileKey)
    return json.loads(response['Body'].read().decode('utf-8'))

# get the key of the last modified obj in a prefix of a bucket
# in_bucketName = the name of the bucket to check
# in_prefix = the name of the prefix to check in the bucket
# returns the key of the last modified file
def get_last_modified_key(in_bucketName, in_prefix):
    sortedArray = sorted(
        bucketLister.get_matching_s3_objects(in_bucketName, in_prefix),
        key=lambda x: x['LastModified'], reverse=True
    )
    return sortedArray[0]['Key']

# Sends an sns message to a topic ARN
# in_content = the content of the message to send
# in_snsARN = the ARN of the sns topic to send the message to
def send_sns(in_content, in_snsARN):
    sns = boto3.client('sns')
    sns.publish(TopicArn=in_snsARN, Message=in_content,)

# checks to see if inputted key is contain in dict
# dict = the dictionary to check through
# key = the key to check for in the dictionary
# returns True if the key is contained in the dictionary, or
# returns False if the key is not contained in the dictionary
def check_key(dict, key):  
    if key in dict.keys():
        return True 
    else: 
        return False

# gets the correct log prefix based an the job name, currently hardcoded in
# in_event = the lambda event to get the current job from
# returns the log prefix and bookmark prefix if it is a known job or ERROR_UNKNOWN if it is not
def get_job_args_glog(in_event):
    if in_event['currentJob'] == os.environ["RawJob1"]:
        return {"Prefix" : "R2T", "Bookmark" : "Raw_Bookmark", "Bucket" : os.environ["RawBucket"]}
    elif in_event['currentJob'] == os.environ["TransformedJob1"]:
        return {"Prefix" : "T2N", "Bookmark" : "Transformed_Bookmark", "Bucket" : os.environ["TransformedBucket"]}
    else:
        return {"Prefix" : "ERROR_UNKNOWN", "Bookmark" : "ERROR_UNKNOWN", "Bucket" : "ERROR_UNKNOWN"}

# changes a dictionary into json format or just a string if unable 
# in_dictionary = the dictionary to be formatted
# returns content = the formatted string from the dictionary
def format_dict_to_json(in_dictionary):
    try:
        content = json.dumps(in_dictionary, indent=4, sort_keys=True)
    except:
        content = str(in_dictionary)

    return content

# Save a log to bucket in a prefix
# in_content = the content of the log to save (string)
# in_bucketName = the name of the s3 bucket to save the log
# in_prefixFolder = the prefix in the bucket to save the log in
def save_log_pro(in_content, in_bucketName, in_prefixFolder):
    # Naming convention information
    tempFileName = '/tmp/temp_log.txt'
    fileName = "Bookmark.txt"
    
    with open(tempFileName, 'w+') as f:
        f.write(in_content)
        
    s3 = boto3.client('s3')
    s3.upload_file(tempFileName, in_bucketName, '{0}/{1}'.format(in_prefixFolder,fileName))

# convert the data processed section in a glue log into a more usable format
# in_glog = the glue log as a dictionary
# returns {"dataType1" : {"Successful" : ["file1.csv", file2.csv], "Failed" : ["file3.csv", "file4.csv"]}}
def conv_glue_log(in_glog):
    datatypeProcesses = {}
    
    for data in in_glog['Data_Processed']:
        DataType = data['DataType']
        succesfulFiles = []
        failedFiles = []

        # get and convert all file names
        if 'Succesful' in data['FilesProcessed']:
            for listOfFiles in data['FilesProcessed']['Succesful']:
                for fileName in listOfFiles:
                    succesfulFiles += ['/'.join(fileName.split('/')[3:])]

        if 'Failed' in data['FilesProcessed']:
            for listOfFiles in data['FilesProcessed']['Failed']:
                for fileName in listOfFiles:
                    convFileName = '/'.join(fileName.split('/')[3:])
                    # some errors output a different file naming convention
                    if len(convFileName) != 0:
                        failedFiles += [convFileName]
                    else:
                        failedFiles += [fileName]

        datatypeProcesses[DataType] = {'Successful':succesfulFiles, 'Failed':failedFiles}
    
    return datatypeProcesses

# gets a dict of all successful and failed t process files from the glue log
# in_glog = the glue log to process
# returns {"SuccessFiles" : {"dataType1" : ["file1.csv"]}, "FailedFiles" : {"dataType1" : ["file2.csv"]}}
def get_glog_status(in_glog):
    succFiles = {}
    failFiles = {}
    convLog = conv_glue_log(in_glog)

    for dt in convLog:
        if len(convLog[dt]['Successful']) != 0:
            succFiles[dt] = convLog[dt]['Successful']
        if len(convLog[dt]['Failed']) != 0:
            failFiles[dt] = convLog[dt]['Failed']

    return {"SuccessFiles" : succFiles, "FailedFiles" : failFiles}

# Updates the bookmark log file
# in_proData = the file keys that have been processed correctly
# in_failData = the file keys that have failed to process
# in_logBucket = the name of the log bucket
# in_bmPrefix = the prefix of where the bookmark log is
def update_bookmark(in_proData, in_failData, in_logBucket, in_bmPrefix):
    # Get the previous bookmark
    bmLog = get_json_log_contents(in_bmPrefix + "/Bookmark.txt", in_logBucket)

    # Include the current timestamp
    bmLog['LastProcessedTime'] = (datetime.utcnow() + timedelta(hours=int(os.environ['timeDiff']))).strftime("%Y-%m-%d-%H-%M")

    # Update processed data
    for dType in in_proData:
        if check_key(bmLog['ProcessedFiles'], dType):
            bmLog['ProcessedFiles'][dType] = list(dict.fromkeys(in_proData[dType] + bmLog['ProcessedFiles'][dType]))
        else:
            bmLog['ProcessedFiles'][dType] = list(dict.fromkeys(in_proData[dType]))
        # keep track of the process time for each dtype
        bmLog['ProcessedTimes'][dType] = bmLog['LastProcessedTime']

    # Update failed data
    for dType in in_failData:
        if check_key(bmLog['FailedFiles'], dType):
            if check_key(bmLog['FailedFiles'][dType], bmLog['LastProcessedTime']):
                bmLog['FailedFiles'][dType][bmLog['LastProcessedTime']] = list(dict.fromkeys(in_failData[dType] + bmLog['FailedFiles'][dType][bmLog['LastProcessedTime']]))
            else:
                bmLog['FailedFiles'][dType][bmLog['LastProcessedTime']] = list(dict.fromkeys(in_failData[dType]))
        else:
            bmLog['FailedFiles'][dType] = {}
            bmLog['FailedFiles'][dType][bmLog['LastProcessedTime']] = list(dict.fromkeys(in_failData[dType]))

    # Save the log
    save_log_pro(format_dict_to_json(bmLog), in_logBucket, in_bmPrefix)

# Moves files to quarantine prefix on same bucket
# in_fileKeys = list of file keys to quarantine
# in_bucketName = the name of bucket where the keys are
def quarantine_files(in_fileKeys, in_bucketName):
    qPrefix = "quarantine/"
    s3Res = boto3.resource('s3')
    print("[QFS] Bucket: " + in_bucketName)

    for dType in in_fileKeys:
        for qFile in in_fileKeys[dType]:
            try:
                # copy file to quarantine prefix, delete the orginal
                print("[QFS] Quarantining file: " + in_bucketName + '/' + qFile)
                s3Res.Object(in_bucketName, qPrefix + qFile).copy_from(CopySource=(in_bucketName + '/' + qFile))
                s3Res.Object(in_bucketName, qFile).delete()
                print("[QFS] Done")
            except Exception as e:
                print("[ERR][QFS] " + qFile)
                print("[ERR][QFS] " + str(e))
    print("[QFS] Quarantining complete")

##########
# LEGACY #
##########
# get files from the event (in process) for an inputted datatype
# in_dTypeList = the list of data types to get files for
# in_event = the lambda event that contains the judgeOut output
# returns a dict {"dataType" : ["file.txt", "file2.txt"]}
# def get_dtypes_files(in_dTypeList, in_event):
#     dTypeDict = {}
#     proDataFiles = ast.literal_eval(in_event['judgeOut']['GlueArgs']['--PRODATAFILES'])
#     proFiles = get_json_log_contents(proDataFiles['FileKey'], proDataFiles['Bucket'])
#     for dType in in_dTypeList:
#         if check_key(proFiles, dType):
#             dTypeDict[dType] = proFiles[dType]
#     return dTypeDict