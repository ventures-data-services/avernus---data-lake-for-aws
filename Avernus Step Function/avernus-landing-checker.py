# Ventures Pinnacle Incorporated
# Dylan Byrne
# 09/05/2019
import os
import boto3
import bucketLister

# generate output based on input and the check_landing function
# returns dictionary of output of the following:
# status = status of the landing folder, either empty or populated
# waitCount = the number of times loop has waited
# lastFile = the last file the landing checker has seen
# lastJudged = keep the same input, or output of check_landing if none
def lambda_handler(event, context):
    newWaitCount = 0
    currentLastFile = ""
    result = check_landing()
    lastJudged = result

    # if there is a checker output already
    if check_key(event, "checkerOut"):
        newWaitCount = (int(event['checkerOut']['waitCount']) + 1)
        currentLastFile = event['checkerOut']['lastFile']
        lastJudged = event['checkerOut']['lastJudged']

    if result == "empty":
        return {"status" : result, "waitCount" : newWaitCount, "lastFile" : currentLastFile, "lastJudged" : lastJudged}
    else:
        return {"status" : "populated", "waitCount" : newWaitCount, "lastFile" : result, "lastJudged" : lastJudged}

# Checks if there is any files within the Landing folder of a bucket
# returns obj, the first file discovered when checking, or
# returns empty, if no files are discovered
def check_landing():
    bucketName = os.environ['sourceBucket']
    landingPrefix = "Landing"

    for obj in bucketLister.get_matching_s3_keys(bucketName, landingPrefix):
        if obj == "Landing/":
            continue
        else:
            return obj

    return "empty"

# checks to see if inputted key is contain in dict
# dict = the dictionary to check through
# key = the key to check for in the dictionary
# returns True if the key is contained in the dictionary, or
# returns False if the key is not contained in the dictionary
def check_key(dict, key):  
    if key in dict.keys():
        return True 
    else: 
        return False