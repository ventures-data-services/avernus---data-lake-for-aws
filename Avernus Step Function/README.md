# Avernus Step Function
##### Ventures, Dylan Byrne, 30/07/2019 

## Contents 
- [Description](#description)
- [Code Access](#code-access)
- [Input](#input)
  - [Data Type Definition](#data-type-definition)
- [Avernus Data Processing State Machine](#avernus-data-processing-state-machine)
  - [State Descriptions](#state-descriptions)
    - [Sorting Section](#sorting-section)
    - [Glue Job Section](#glue-job-section)
    - [End States](#end-states)
  - [State Machine Output](#state-machine-output)
- [Avernus Landing Judge](#avernus-landing-judge)
  - [Landing Judge Description](#landing-judge-description)
  - [Current Supported Data Types](#current-supported-data-types)
  - [Data Types Dictionary](#data-types-dictionary)
  - [Judge Output](#judge-output)
  - [Adding New Data Types](#adding-new-data-types)
  - [Removing Data Types](#removing-data-types)
- [Avernus Bookmark Find Unprocessed](#avernus-bookmark-find-unprocessed)
  - [Bookmark Find Unprocessed Description](#bookmark-find-unprocessed-description)
  - [Bookmark Find Unprocessed Output](#bookmark-find-unprocessed-output)
- [Avernus Glue Log Checker](#avernus-glue-log-checker)
  - [Glue Log Checker Description](#glue-log-checker-description)
  - [Supported Log Prefixes](#supported-log-prefixes)
  - [Unknown Prefix Handling](#unknown-prefix-handling)
  - [SNS Events](#sns-events)
  - [Output](#output)

## Description
Resources used in the Avernus Step Function, which takes the uploaded data, sorts it, and processes it through to the Enriched data stage. The code contained here are used by the lambda functions in the state machine, all of it is written in Python 3.7.

## Code Access
It is expected by the CloudFormation template that all code files will be archived together (eg AvernusStepFunctionPackage.zip) in a S3 bucket, the location and key of which you provide at stack creation as parameters. Most functions are assumed to be archived together with at least the *bucketLister.py* file.

## Input
You can provide JSON input to the state machine, but no input is required. The following input keys can be used:

| **Input Key**       | **Description**                                              | **Valid Input**                   |
| ------------------- | ------------------------------------------------------------ | --------------------------------- |
| **sortOnly**        | Whether the state machine continues after the sorting section to the glue jobs (false to continue) | `true` or `false`                 |
| **OnlyDataTypes**   | What data types the state machine should process, it will ignore all others | list of strings, `["dt1", "dt2"]` |
| **IgnoreDataTypes** | What data types the state machine will ignore, has by default landing, unknown, and quarantine added to this list in the code | list of strings`["dt1", "dt2"]`   |

If the same data type is in both *OnlyDataTypes* and *IgnoreDataTypes* it **WILL** be ignored.

An example JSON input would be:

```json
{
    "sortOnly" : false,
    "OnlyDataTypes" : ["dataType1", "dataType2", "dataType3"],
    "IgnoreDataTypes" : ["dataType4", "dataType5"]
}
```
If no input is provided the state machine will set *sortOnly* to `false`, and will ignore *OnlyDataTypes* and *IgnoreDataTypes*, processing all data types not bookmarked except for the default ignore types (landing, unknown, and quarantine).

#### Data Type Definition

A data type by default is assumed be the lowered first prefix before the data file, separated by the '/' character. For example for the file *Visits/vis-2019.csv*, the data type is assumed to be *visits*. 

The prefix, therefore the data type, can be set for files in that are sorted from the *Landing/* prefix in the raw bucket, in the [*avernus-landing-judge.py*](#avernus-landing-judge) file.

Custom data types can be constructed through modifying the *get_file_dt* function in the [*avernus-bookmark-find-unpro.py*](#avernus-bookmark-find-unprocessed) file, to have special conditions or some other form of custom code. You will also have to make some accommodations for this in the Glue Job scripts as well.

## Avernus Data Processing State Machine
<img src="../images/DaPromFull.png" alt="StateMachineDiagram" width="600" height="850" />

### State Descriptions
#### Sorting Section
| **State Name** | **Type** | **Description** |
| ------ | -------- | ---- |
| **LandingJudge** | Task: Lambda | Starts the *avernus-landing-judge* lambda function, which sorts files contained within the */Landing* prefix of the raw bucket, placing any unknown file types into an */Unknown* prefix in the bucket, and sending an SNS with a list of these files. More info in the [Avernus Landing Judge](#avernus-landing-judge) section of this README. Outputs to `"$.judgeOut"` |
| **WaitLanding** | Wait | Waits for 2 minutes by default |
| **LandingChecker** | Task: Lambda | Starts the *avernus-landing-checker* function, which checks if the */Landing* folder is empty, and updates metrics used later. Outputs to `"$.checkerOut"` |
| **LandingEmpty?** | Choice | Moves to **SortOnlyChecker** if the Landing is empty, otherwise will move back to **WaitLanding** if the number of iterations through **LandingChecker** is less than 5, or will go to **LastJudgedChecker** if the iterations is above 5. If none of the other conditions are met it will move to **SetEndTypeFail** |
| **LastJudgedChecker** | Task: Lambda | Starts the *avernus-last-judged-checker*, which checks to see if the *avernus-landing-judge* is moving files out of the Landing folder. Outputs to `"$.checkerOut.lastJudged"` |
| **JudgeMovingFiles?** | Choice | If  `"$.checkerOut.lastJudged"`  is `"Fail"` it will move to  **SetEndTypeFail**, otherwise it will move to **ResetWaitCount** |
| **ResetWaitCount** | Pass | Sets the `"$.checkerOut.waitCount"` variable to 0 |
| **SortOnlyChecker** | Task: Lambda | Starts the *avernus-sort-only-checker* function, which checks if `"$.sortOnly"` exists, if it does it keeps it the same value, otherwise it sets it to `false`|
| **SortOnly?** | Choice | Moves to **SetEndTypeSuccess** if `"$.sortOnly"` is `true`,  or moves to **SetEndTypeFail** if or `"$.judgeOut.TypesEmpty"` is `true`, otherwise it moves to **SetR2TJob** |

#### Glue Job Section
| **State Name** | **Type** | **Description** |
| ------ | -------- | ---- |
| **SetR2TJob** | Pass | Sets the `"$.currentJob"` variable to the name of the first raw data job created by the Cloudformation stack |
| **Raw2ProFinder** | Task: Lambda | Starts the [*avernus-bookmark-find-unpro*](#avernus-bookmark-find-unprocessed) function. Finds which files have not been processed via the raw bookmark, and updates `"$.judgeOut.GlueArgs"` with these file names. Also calculates how many DPUs to assign to the R2T Glue Job, stored in the `$.judgeOut.Workers` variable |
| **RawProcessData?** | Choice | Checks `"$.judgeOut.TypesEmpty"` if there is data to be processed from raw to transformed, if there is not it moves to **SetT2NJobBasic**, otherwise it goes to **R2TJob** |
| **R2TJob** | Task: Glue Job | Starts the raw data glue job with arguments (`"--DATATYPES"`) from the *avernus-landing-judge* output, stored in `"$.judgeOut.GlueArgs"` |
| **WaitR2TJob** | Wait | Waits for 5 minutes by default |
| **R2TJobChecker** | Task: Lambda | Starts the *avernus-job-checker* lambda function which checks the status of the job whose name is in `"$.currentJob"` and returns what action should be taken based on this. Outputs to `"$.jobCheckerOutR2T"` |
| **R2TJobFinished?** | Choice | If the result in `"$.jobCheckerOutR2T"` is `"SUCCEED"` then it moves to **RawJobLogChecker**, or if it is `"FAIL"` it moves to **SetEndTypeFail**, or if it is `"WAIT"` it moves back to **WaitR2TJob** |
| **RawJobLogChecker** | Task: Lambda | Starts the [*avernus-glue-log-checker*](#avernus-glue-log-checker) function which checks the most recently created log from **R2TJob**, checking the job's output and updating glue arguments, the bookmark file, and failed data types, sending a SNS when appropriate. Outputs to `"$.judgeOut"` |
| **RawDataTypes?** | Choice | If `"$.judgeOut.TypesEmpty"` is true then it moves to **SetEndTypeFail**, otherwise it moves to **SetT2NJobBasic** |
| **SetT2NJobBasic** | Pass | Sets the `"$.currentJob"` variable to the name of the first transformed data job created by the CloudFormation stack |
| **Transformed2ProFinder** | Task: Lambda | Starts the [*avernus-bookmark-find-unpro*](#avernus-bookmark-find-unprocessed) function. Finds which files have not been processed via the transformed bookmark, and updates `"$.judgeOut.GlueArgs"` with these file names. Also calculates how many DPUs to assign to the R2T Glue Job, stored in the `$.judgeOut.Workers` variable |
| **TransformedProcessData?** | Choice | Checks `"$.judgeOut.TypesEmpty"` if there is data to be processed from raw to transformed, if there is not it moves to **SetEndTypeSuccess**, otherwise it goes to **T2NJobBasic** |
| **T2NJobBasic** | Task: Glue Job | Starts the transformed basic glue job with arguments (`"--DATATYPES"`) from the *avernus-landing-judge* output, stored in `"$.judgeOut.GlueArgs"` |
| **WaitT2NJobBasic** | Wait |Waits for 5 minutes by default |
| **T2NJobBasicChecker** | Task: Lambda | Starts the *avernus-job-checker* lambda function which checks the status of the job whose name is in `"$.currentJob"` and returns what action should be taken based on this. Outputs to `"$.jobCheckerOutT2NBasic"` |
| **T2NJobBasicFinished?** | Choice | If the result in `"$.jobCheckerOutT2NBasic"` is `"SUCCEED"` then it moves to **TransformedBasicJobLogChecker**, or if it is `"FAIL"` it moves to **SetEndTypeFail**, or if it is `"WAIT"` it moves back to **WaitT2NJobBasic** |
| **TransformedBasicJobLogChecker** | Task: Lambda | Starts the [*avernus-glue-log-checker*](#avernus-glue-log-checker) function which checks the most recently created log from **T2NJobBasic**, checking the job's output and updating glue arguments and failed data types, and sending a SNS when appropriate. Outputs to `"$.judgeOut"` |
| **TransformedBasicDataTypes?** | Choice | If `"$.judgeOut.TypesEmpty"` is true then it moves to **SetEndTypeFail**, otherwise it moves to **SetEndTypeSuccess** |

#### End States
| **State Name** | **Type** | **Description** |
| ------ | -------- | ---- |
| **SetEndTypeSuccess** | Pass | Sets the `"$.endType"` variable to `"Success"` |
| **SuccessNotify** | Task: Lambda | Starts the *avernus-state-log-sns* function which sends the final output as a SNS, and writes it as a log to the log bucket of the data lake |
| **SucceedEnd** | Succeed | Stops the Step Function with a success |
| **SetEndTypeFail** | Pass | Sets the `"$.endType"` variable to `"Fail"` |
| **FailNotify** | Task: Lambda | Starts the *avernus-state-log-sns* function which sends the final output as a SNS, and writes it as a log to the log bucket of the data lake |
| **FailEnd** | Fail | Stops the Step Function with a failure |

### State Machine Output
The state machine upload logs with the *Avernus_State_Machine/* prefix to the data lake log bucket, and sends a SNS to the step function topic. A log for a successful run would look like the following:
```json
{
    "CalledFrom": "avernus-ferryman-log-checker",
    "OnlyDataTypes": [
        "dt1",
        "dt2",
        "dt3"
    ],
    "judgeOut": {
        "GlueArgs": {
            "--DATATYPES": "['dt1', 'dt2']",
            "--PRODATAFILES": "{'Bucket': 'team-datalake-logs-s3', 'FileKey': 'ASM_Process_Data_Files/Transformed_ProDataFiles_2019-12-30-01-01.txt'}"
        },
        "TypesEmpty": false
    },
    "checkerOut": {
        "status": "empty",
        "waitCount": 0,
        "lastFile": "",
        "lastJudged": "empty"
    },
    "sortOnly": false,
    "currentJob": "team-datalakeJob-transformed-ETL",
    "R2Terror": {
        "Error": "States.Timeout",
        "Cause": ""
    },
    "jobCheckerOutR2T": "SUCCEEDED",
    "T2Nerror": {
        "Error": "States.Timeout",
        "Cause": ""
    },
    "jobCheckerOutT2NBasic": "SUCCEEDED",
    "endType": "Success"
}
```
## Avernus Landing Judge
### Landing Judge Description
The Avernus Landing Judge sorts all files in the Landing/ prefix on the Raw Data Bucket in the data lake. If an object in the landing does not meet its sort criteria it then saves that file with the Unknown/ prefix.

### Current Supported Data Types
| **Data Name** | **File Prefix** | **S3 Bucket Prefix** |
| ------ | -------- | ---- |
| **PHO_Enrolment_Weekly** | PHO_Enrolment, but also has the 'S' character before the timestamp, indicating it's a snapshot file | *PHO_Enrolment_Weekly/* |
| **PHO_Enrolment_Monthly** | PHO_Enrolment, but also has the 'M' character before the timestamp, indicating it's a monthly file | *PHO_Enrolment_Monthly/* |
| **PHO_Enrolment_Archive** | PHO_Enrolment, but also has the 'A' character before the timestamp, indicating it's a file to archive | *PHO_Enrolment_Archive/* |
| **PHO_Enrolment** | PHO_Enrolment, but no file should be finally in this type, they'll either be a Weekly, Monthly, Archive, or Unknown file | *PHO_Enrolment/* |
| **PHO_** | PHO_ | *PHO_Collection/* |
| **PHOS_PERSON_EXTRACT_PERIOD** | PHOS_PERSON_EXTRACT_PERIOD | *PHO_Financial_Extract/* |
| **NNPAC** | NNPAC | *NNPAC/* |
| **NMDS** | NMDS | *NMDS/* |
| **Unknown** | All file prefixes that do not match any defined prefix (the others in this column) | *Unknown/* |

### Data Types Dictionary
| **Key** | **Value** | **Example** |
| ------ | -------- | ---- |
| **Uploaded** | A Boolean if a data type has been sorted, used as a parameter for starting Glue ETL jobs, should always begin as False | `False` |
| **Path** | the prefix for objects of this keyword should be sorted into, within the bucket. It is recommended you only use capital case letters, lower case letters, and numbers, separated by underscores for your prefix naming convention. This will also be the table name used in the Glue data catalogue, and the path without the '/' is used as the data token passed to the Glue job | `"My_Prefix/"` |
| **Data Source** | the value used for the Data Source tag that is applied to objects when sorting | `"MoH"` |
| **Purpose** | the value used for the Purpose tag that is applied to objects when sorting | `"NMDS"` |

### Judge Output
The landing judge outputs a dictionary in the following format:
```python
{ "GlueArgs" : {"--DATATYPES" : "[dataType1, dataType2]"}, "TypesEmpty" : False}
```
Where `"GlueArgs"` are the arguments passed to the Glue ETL Jobs, currently the data types discovered during sorting, and `"TypesEmpty"` is a Boolean which is `True` if data types is empty, and `False` if it is not.

### Adding New Data Types
1. Add the data file's prefix to the dataPrefix list A prefix that is contained within another prefix must be placed after that prefix in the dataPrefix list. For example, 'prefix' is contained within 'prefixNew' so in order for 'prefixNew' to be sorted separately it must be placed first in the dataPrefix list.
```python
# This will sort seperately
dataPrefix = ['prefixNew', 'prefix']

# This will not
dataPrefix = ['prefix', 'prefixNew']
```
2. Add the new data types information to the dataDictionary. The main key must be the same as the prefix used in the dataPrefix list. Use the information in the [Data Types Dictionary](#data-types-dictionary) to choose appropriate values for each key.
```python
dataDictionary = {
    "Unknown" : {"Uploaded" : False, "Path" : "Unknown/", "Data Source" : "Unknown", "Purpose" : "Unknown"},
    # OTHER DATA TYPES ...
    "PHOS_PERSON_EXTRACT_PERIOD" : {"Uploaded" : False, "Path" : "PHO_Financial_Extract/", "Data Source" : "MoH", "Purpose" : "PHO Financial Extract"},
    "prefixNew" : {"Uploaded" : False, "Path" : "Prefix_New/", "Data Source" : "My Source", "Purpose" : "Have some data"}
} 
```
3. If you need any additional classification for your data type it is recommended you create a function that does this, which is then called inside the `judge_landing_objects` function.

### Removing Data Types
To remove a data type simply remove its entry from the dataPrefix list and dataDictionary dictionary, and any extra classification functions associated with it. The only extra classification function currently in the code is `check_report_pho_enrol()` for *PHO_Enrolment* file types.

## Avernus Bookmark Find Unprocessed

### Bookmark Find Unprocessed Description

The Avernus Bookmark Find Unprocessed finds which files to send to the ETL jobs by checking the files in a S3 bucket against a bookmark file to see if they have yet to be processed. It does this by checking the last modified date for each object against the last processed date for it's data type. If the objects last modified date is newer, it is added to the list to be processed. This is to account for both new data files that added with unique names, and data files that overwrite pre-existing data files.

### Bookmark Find Unprocessed Output

It outputs a dictionary in the following format:

```python
{"GlueArgs" : {"--DATATYPES" : "[dataType1, dataType2]", "--PRODATAFILES" : "{'Bucket' : 'logBucket', 'FileKey' : 'ASM_Process_Data_Files/dataStage_ProDataFiles_2019-01-01-01-01.txt'}"}, "Workers" : 2, "TypesEmpty" : False}
```

The Avernus Find Unprocessed functions out puts file names to a process file in the log bucket under the 'ASM_Process_Data_Files' prefix. The naming convention for this file is 'dataStage_ProDataFiles_timestamp(YYYY-MM-DD-HH-MM).txt'. This file is written in JSON format in the following form:

```json
{
    "dataType1" : [
        "file1.csv",
        "file2.csv",
        "file3.csv"
    ],
    "dataType2" : [
        "file4.parquet",
        "file5.parquet",
        "file6.parquet"
    ]
}
```

It also calculates how many workers (DPUs/data processing units) to assign to the Glue job for the data it has discovered. This is done in the `get_dpus` method. It calculates this by file type, and assigning a KB rate to file types, per worker to assign. The file type is determined by the file extension of the object (eg txt, csv, psv). The minimum number of workers that Glue accepts is 2, so it is floored to this value, though you may increase it if you wish to increase the minimum number of workers. The rates and minimum workers can be adjusted in the `get_dpus` function, like the following for a new file type `newFileType`:

```python
def get_dpu(in_fileTypeSizes):
    # Set up variables
    # KB rates per dpu per file type, defaults to default if file type is unknown
    rates = {"parquet" : 500000000, "csv" : 1000000000, "default" : 1000000000, "newFileType" : 1500000000}
    # Change below to set the minimum number of workers, Glue will error if a job is assigned less than 2
    minDPUs = 2   
	# FUNCTION CODE ...
```

The value calculated is stored in `"Workers"` key of the output dictionary.

## Avernus Glue Log Checker

### Glue Log Checker Description
The Avernus Glue Log checker will check the last created log file from an inputted job. It then uses this job file to return which data types successfully and failed to process and sends an SNS if it discovers any issues.

### Supported Log Prefixes
The glue log checker uses the `get_job_args_glog()` function to get the prefix to check in the Log Bucket to find a jobs log file. It currently supports only the jobs included the two jobs created by the stack, any other job name passed to this function will return `"ERROR_UNKNOWN"` for both the Prefix and Bookmark keys.

| **Key** | **Description** | **Example** |
| ------ | -------- | ------ |
| Prefix | The prefix used for getting the log created by the Glue Job | "R2T" |
| Bookmark | The prefix to get and save the bookmark file to | "Raw_Bookmark" |

```python
{"Prefix" : "R2T", "Bookmark" : "Raw_Bookmark"}
```

### Unknown Prefix Handling

If an `"ERROR_UNKNOWN"` prefix is returned from the `get_job_args_glog()` function then it means that the job name the function received does not have supported values. In this case the Avernus Glue Log Checker sends an SNS about this, then returns the inputted `event['judgeOut']` as it cannot check this against a log.

### SNS Events
The following events cause an SNS to be sent, only one SNS should be sent per run:
- an unsupported prefix for a job is discovered
- new failed data types appear in the log
- there are no non-failed data types

### Job Log Checker Output
The Avernus Glue Log Checker outputs a dictionary in the following format:
```python
{
    "GlueArgs" : {
                    "--DATATYPE" : "['dataType1', 'dataType2']", 
                    "--PRODATAFILES" : 
                        "{'Bucket' : 'logBucket', 'FileKey' : 'ASM_Process_Data_Files/dataStage_ProDataFiles_2019-01-01-01-01.txt'}" 
                }, 		
    "FAILDATA" : ["dataType3", "dataType4"], 
	"TypesEmpty" : False
}
```
Where `"GlueArgs"` are the arguments passed to the Glue ETL Jobs, currently all  current data types and the location of the process file, `"FAILDATA"` is all failed data from the glue log file, and `"TypesEmpty"` is a Boolean which is True if data types is empty, and False if it is not. 

It also updates the bookmark file for the applicable data stage with all the names of the successfully processed files, and keeps tracked of the names of files that have failed to process. The bookmark file looks like the following:

```json
{
    "FailedFiles": {
        "dataType1" : { "2019-01-01-01-01" : [
            "file5.csv",
            "file6.csv"
        ]
    },
    "LastProcessedTime": "2019-01-01-01-01",
    "ProcessedFiles": {
        "dataType1" : [
            "file1.csv",
            "file2.csv"
        ],
        "dataType1" : {
            "file3.parquet",
            "file4.parquet"
        }
    }
    "ProcessedTimes": {
        "dataType1" : "2019-01-01-01-01",
        "dataType1" : "2019-01-01-01-01"
    }
}
```
