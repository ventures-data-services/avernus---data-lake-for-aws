# Ventures Pinnacle Incorporated
# Dylan Byrne
# 14/05/2019
import boto3

# returns the output of job_checker
def lambda_handler(event, context):
    return job_checker(event['currentJob'])

# checks the status of a Glue Job
# in_jobName = the name of the Glue Job to check
# returns SUCCEEDED if the state is SUCCEEDED, or
# returns FAILED if the state is FAILED, TIMEOUT, or STOPPED, or
# returns WAIT if it is any other state
def job_checker(in_jobName):
    client = boto3.client('glue')
    # get the status of the most recent job run
    currentJobStatus = client.get_job_runs(JobName=in_jobName)['JobRuns'][0]['JobRunState']

    if currentJobStatus == 'SUCCEEDED':
        return currentJobStatus
    elif currentJobStatus == 'FAILED' or currentJobStatus == 'TIMEOUT' or currentJobStatus == 'STOPPED':
        return 'FAILED'
    else:
        return 'WAIT'