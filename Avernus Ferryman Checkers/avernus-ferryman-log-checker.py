# Ventures Pinnacle Incorporated
# Dylan Byrne
# 28/06/2019
import boto3
import json
import os

def lambda_handler(event, context):
    # get environ variables
    snsARN = os.environ['snsTopicARN']
    stateARN = os.environ['stateARN']
    stateName = os.environ['stateName']
    # get file info from event
    fileKey = event['Records'][0]['s3']['object']['key']
    bucketName = event['Records'][0]['s3']['bucket']['name']

    logContents = get_json_log_contents(fileKey, bucketName)

    # if the ferryman moved files successfully run the state machine
    if logContents['Status'] == 'COMPLETED' and int(logContents['ProcessesRan']['CountFilesInTemp']) > 0:
        clientSF = boto3.client('stepfunctions')
        if check_stf_status(clientSF, stateARN) == True:
            response = clientSF.start_execution(
            stateMachineArn=stateARN,
            name=stateName,
            input= json.dumps({"CalledFrom" : "avernus-ferryman-log-checker"})
            )
            print(response)
        else:
            send_sns('Charon Log indicates an execution should run, but an execution is already running', logContents, snsARN)
    # else if the ferryman did not run successfully send error sns
    elif logContents['Status'] != 'COMPLETED':
        send_sns("Avernus Ferryman log status was not COMPLETED", logContents, snsARN)
    # if the log did not meet the above conditions then the ferryman completed successfully
    # but didn't move any files

    return logContents

# checks the status of state machine to see if it's running or not
# in_client = a boto3 step function client
# in_stateARN = the sns topic to send the message to
# returns True if the state machine is not currently running, False if it isn't
def check_stf_status(in_client, in_stateARN):
    response = in_client.list_executions(stateMachineArn=in_stateARN, statusFilter='RUNNING')
    if len(response['executions']) == 0:
        return True
    else:
        return False

# Gets a python dictionary from a json file
# in_fileKey = the key of file to get the dict from
# in_bucketName = the name of bucket where the file is
# returns a dict of the file's contents
def get_json_log_contents(in_fileKey, in_bucketName):
    s3 = boto3.client('s3')
    response = s3.get_object(Bucket=in_bucketName, Key=in_fileKey)
    return json.loads(response['Body'].read().decode('utf-8')) 

# sends an sns with the logs info in the case of not COMPLETED status
# in_content = the content of the log in dict form
# in_message = a message to add to the notification
# in_stateARN = the sns topic to send the message to
def send_sns(in_message, in_content, in_snsARN):
    sns = boto3.client('sns')
    notification = {"Message" : in_message, "LogContent" : in_content}
    notification = format_dict_to_json(notification)

    sns.publish(
        TopicArn=in_snsARN,
        Message=notification,
    )

# changes a dictionary into json format or just a string if unable 
# in_dictionary = the dictionary to be formatted
# returns content = the formatted string from the dictionary
def format_dict_to_json(in_dictionary):
    try:
        content = json.dumps(in_dictionary, indent=4, sort_keys=True)
    except:
        content = str(in_dictionary)

    return content