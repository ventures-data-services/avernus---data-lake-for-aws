# Avernus Ferryman Checkers
##### Ventures, Dylan Byrne, 25/06/2019

## Description
Resources used to check the Avernus Ferryman is running properly, and to trigger the data processing Step Function if it has uploaded new files to the raw data bucket.

## Code Use
It is expected by the CloudFormation template that all code files will be archived together (eg AvernusFerrymanChecker.zip) in a S3 bucket, the location and key of which you provide at stack creation.

## Lambda Functions
| | **Ferryman Running Checker** | **Ferryman Log Checker** |
| -------- | -------- | -------- |
| **Description** | Checks the last modified file in the Ferryman/ prefix in the log bucket, if the timestamp in that file's name happened before the [threshold](#threshold) time then it sends a SNS to the Ferryman topic | Checks the Charon_log file every time one is PUT into the Ferryman/ prefix in the log bucket. If the log indicated it moved files successfully then it runs the Avernus State Machine, if it completed successfully but moved no file then nothing happens, if it didn't complete successfully then a SNS is sent to the Ferryman Topic. |
| **Triggered by**| Schedule equal to the [threshold](#threshold) number of hours | PUT event in the log bucket, where the prefix equals Ferryman/ |
| **Post stack setup** | No | [Manually setup](#log-checker-extra-setup) the S3 PUT event for this lambda |

### Threshold
The threshold value is user inputted at the time of stack creation and must by a value that is 2 or greater. It represents the number of hours the Running Checker is scheduled to run (eg once every 7 hours), and what the threshold is for how old a Charon log file can be (eg if it is older than 7 hours send an warning SNS).

## Log Checker Extra Setup
After stack creation some extra setup is needed for the Ferryman Log Checker.
1. Go to the Ferryman Log Checker created by the stack
2. Under the 'Add Triggers' section in the designer add a S3 trigger
3. For bucket select the log bucket created by the stack
4. For event type it is recommended you choose PUT
5. For Prefix input Ferryman/
6. For Suffix you can leave blank or set to the file extension you know you will receive (eg .txt)
7. Make sure the 'Enable trigger' box is ticked, then click 'Add'

This should set up the Log Checker to run automatically every time an object is PUT into Ferryman/ directory of the log bucket.

