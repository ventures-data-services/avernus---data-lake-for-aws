# Ventures Pinnacle Incorporated
# Dylan Byrne
# 24/06/2019
import boto3
import bucketListerMod
from datetime import datetime, timedelta
import datetime
import os

def lambda_handler(event, context):
    # get environ variables
    # timeDifference is the difference between the file timestamp and utc time
    timeDifference = int(os.environ['timeDiff'])
    snsARN = os.environ['snsTopicARN']
    threshold = int(os.environ['threshold'])
    bucketName = os.environ['logBucket']
    prefix = os.environ['logPrefix']

    lastFile = get_last_modified_file(bucketName, prefix)
    response = check_timestamp(get_file_timestamp(lastFile), threshold, timeDifference)

    if response['inThreshold'] == False:
        send_check_fail_sns(lastFile, response, snsARN, bucketName)

    return {"lastFile" : lastFile, "checkTimestampResponse" : response}

# check to see if timestamp is within threshold
# in_timeStamp = the timestamp of the file to check
# in_checkPeriodHours = the number of hours to make the threshold
# in_timeDifference = the difference in hours between the log timestamp and utc
# returns a dictionary of response information with the "inThreshold" key containing the bool
# of whether the in_timeStamp is within the threshold
def check_timestamp(in_timeStamp, in_threshold, in_timeDifference):
    threshTime = (datetime.datetime.utcnow() + timedelta(hours=in_timeDifference - in_threshold)).strftime("%Y%m%d%H%M%S")[:-2]
    if int(in_timeStamp) > int(threshTime):
        return {"inThreshold" : True, "checkThresholdHours" : in_threshold, "threshTime" : threshTime, "fileTime" : in_timeStamp}
    else:
        return {"inThreshold" : False, "checkThresholdHours" : in_threshold, "threshTime" : threshTime, "fileTime" : in_timeStamp}

# get the last modified file key from a bucket
# in_bucketName = the the bucket name to check
# in_prefix = the prefix in the bucket to check to
# returns the key of the last modified file in the bucket dir
def get_last_modified_file(in_bucketName, in_prefix):
    sortedArray = sorted(
        bucketListerMod.get_matching_s3_keys(in_bucketName, in_prefix),
         key=lambda x: x['LastModified'], reverse=True
    )
    return sortedArray[0]['Key']

# Gets the filename's timestamp by trimming the filename
# in_fileName = the file name to be trimmed
# returns in_fileName which is the str of the filenames timestamp
def get_file_timestamp(in_fileName):
    in_fileName = in_fileName.split('/')[1].split('_')[2].split('.')[0]
    return in_fileName

# sends an sns with information
# in_lastFile = the key of the last log recieved
# in_thresholdCheck = the threshold hour rate
# in_snsARN = the sns topic to send the message to
def send_check_fail_sns(in_lastFile, in_thresholdCheck, in_snsARN, in_bucketName):
    sns = boto3.client('sns')
    notification = {"Message" : "Last file found in the Ferryman logs is before set threshold", "LogBucket" : in_bucketName, "LastFile" : in_lastFile, "ThresholdCheck" : in_thresholdCheck}
    notification = format_dict_to_json(notification)

    sns.publish(
        TopicArn=in_snsARN,
        Message=notification,
    )

# changes a dictionary into json format or just a string if unable 
# in_dictionary = the dictionary to be formatted
# returns content = the formatted string from the dictionary
def format_dict_to_json(in_dictionary):
    try:
        content = json.dumps(in_dictionary, indent=4, sort_keys=True)
    except:
        content = str(in_dictionary)

    return content